<?php

namespace App\Http\Controllers\Auth;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use App\User;
use App\VerifyUser;
use App\Profile;
use App\Role;
use Session;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

use App\Mail\VerifyMail;
use App\Mail\NewRegistrationMail;
use Illuminate\Support\Facades\Mail;



class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard/translator';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            //'password' => ['required', 'string', 'min:8', 'regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\X])(?=.*[!$#%]).*$/' , 'confirmed']
            'password' => ['required', 'string', 'min:8', 'regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\X])(?=.*[!@#$&_*]).*$/' , 'confirmed']
        ]);
    }
    // !@#$&*

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $ucode = 'ucodel'.time();
        $user = User::create([
            'role' => $data['role'] ?? 'Translator',
            'ucode' => $ucode,
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'email_verified_at' => now(),
        ]);

        $verifyUser = VerifyUser::create([
          'user_id' => $user->id,
          'token' => sha1(time())
      ]);
        Mail::to($user->email)->send(new VerifyMail($user));
        $profile = Profile::create([
            'user_id' => $user->id,
            'ucode' => $ucode
        ]);
        $user->roles()->attach(Role::where('name', $data['role'] ?? 'Translator')->first());
        $admin_email = $_SERVER['REMOTE_ADDR'] == "127.0.0.1" ? "admin@example.com" : "support@arabeasy.com";
        Mail::to($admin_email)->send(new NewRegistrationMail($user));
        return $user;
    }


    public function verify($token)
    {
      $verifyUser = VerifyUser::where('token', $token)->first();
      if(isset($verifyUser) ){
        $user = $verifyUser->user;
        if(!$user->verified) {
          $verifyUser->user->verified = 1;
          $verifyUser->user->email_verified_at = now();
          $verifyUser->user->save();
          $status = "Your e-mail is verified. You can now login.";
      } else {
          $status = "Your e-mail is already verified. You can now login.";
      }
  } else {
    return redirect('/login')->with('warning', "Sorry your email cannot be identified.");
}
return redirect('/login')->with('status', $status);
}

protected function registered(Request $request, $user)
{
  $this->guard()->logout();
  return redirect('/login')->with('status', 'We sent you an activation code. Check your email and click on the link to verify.');


}
}