<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use DB;
use App\Role;
use Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


    public function login(Request $request)
    {

        $this->validate($request, [
         'email' => ['required', 'string', 'email'],
         //'password' => ['required', 'string', 'min:8', 'regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\X])(?=.*[!$#%]).*$/']
         'password' => ['required', 'string', 'min:8']
     ]);



        $username = $request->email; 
        $password = $request->password;
        
        
        
        if ( Auth::attempt(['email' => $username, 'password' => $password])) {

            Auth::user()->profile->update([
                'introModelDisplayState' => 1
            ]);
            

            $role_id = DB::table('user_role')->where('user_id', Auth::id())->get(['role_id'])[0];
            $role = Role::find($role_id->role_id);
            Session::put('role', $role->name);
            // Auth::user()->roles()->attach(Role::where('name', $role->name)->first());
            switch ($role->name) {
                case 'Admin':
                $this->redirectTo = '/dashboard/admin';
                return redirect()->route('dashboard.admin.index');
                break;

                case 'Client':
                $this->redirectTo = '/dashboard/client';
                return redirect()->route('dashboard.client.myprofile.personalInformation');
                break;

                case 'Quality Analyst':
                $this->redirectTo = '/dashboard/quality-analyst';
                return redirect()->route('dashboard.quality-analyst.myprofile.personalInformation');
                break; 

                case 'Proof Reader':
                $this->redirectTo = '/dashboard/proof-reader';
                return redirect()->route('dashboard.proof-reader.myprofile.personalInformation');
                break; 

                case 'Operations':
                $this->redirectTo = '/dashboard/operations';
                return redirect()->route('dashboard.operations.myprofile.personalInformation');
                break;     

                default:
                $this->redirectTo = '/dashboard/translator/my-profile/personal-information';
                return redirect()->route('dashboard.translator.myprofile.personalInformation');
                break;
            }
        }else{
            return redirect()->route('login')->with(['warning' => 'Incorrect Email or Password!']);
        }
        return redirect()->route('login')->with(['warning' => 'Please, check your credentials']);
    }

    public function authenticated(Request $request, $user)
    {
      if (!$user->verified) {
        auth()->logout();
        return back()->with('warning', 'You need to confirm your account. We have sent you an activation code, please check your email.');
    }
    return redirect()->intended($this->redirectPath());
}
}