<?php

namespace App\Http\Controllers;
use App\User;
use App\Role;
use App\QueryMessage;
use Auth;


use App\Mail\GetHelpMail;
use Illuminate\Support\Facades\Mail;

use Illuminate\Http\Request;

class CommonController extends Controller
{
    // protected $role = 'translator';
    public function __construct()
    {
        // switch (Auth::user()->role) {
            //     case 'Admin':
                //         $this->role = 'admin';
                //     break;
        
                //     case 'Translator':
                    //         $this->role  = 'translator';
                    //     break;
        
                    //     case 'Quality Analyst':
                        //         $this->role = 'quality-analyst';
                        //     break;
        
                        //     case 'Proof Reader':
                            //         $this->role = 'proof-reader';
                            //     break;
        
                            //     case 'Operations':
                                //         $this->role = 'operations';             
                                //     break;
        
                                //     case 'Client':
                                    //         $this->role = 'client';                       
                                    //     break;
        
                                    //     default:
                                    //     $this->role = 'translator';   
                                    //     break;
                                    // }
        
        $this->middleware('auth');
    }
    
    
    
    
    
    public function getHelp()
    {
        return view('dashboard.get-help');
    }
    
    public function getHelpProcess(Request $r)
    {
        
        $query = $r->get('query');
        
        $queryMessage = new QueryMessage;
        $queryMessage->ucode = Auth::user()->ucode;
        $queryMessage->query = $query;
        $queryMessage->save();
        $admin_email = $_SERVER['REMOTE_ADDR'] == "127.0.0.1" ? "admin@example.com" : "support@arabeasy.com";
        Mail::to($admin_email)->send(new GetHelpMail(Auth::user(), $query));
        return response()->json(['message' => true], 200);
    }
    
    public function manageProfile()
    {
        return view('dashboard.manage-profile');
    }
    
    public function manageTest()
    {
        return view('dashboard.manage-test');
    }
    
    public function listRoles()
    {
        $users = User::all();
        return view('dashboard.admin.assign-roles', ['users' => $users]);
    }
    
    public function getGenerateArticle()
    {
        return response('Article generated!', 200);
    }
    
    public function postAdminAssignRoles(Request $request)
    {
        $user = User::where('email', $request['email'])->first();
        $user->roles()->detach();
        if ($request['role_user']) {
            $user->roles()->attach(Role::where('name', 'User')->first());
        }
        if ($request['role_author']) {
            $user->roles()->attach(Role::where('name', 'Author')->first());
        }
        if ($request['role_admin']) {
            $user->roles()->attach(Role::where('name', 'Admin')->first());
        }
        return redirect()->back();
    }
    
    public function permissionError(){
        return view('errors.permission');
    }
    
    public function displayImage($filename)
    
    {
        $path = storage_public('bank_proofs/' . $filename);
        if (!File::exists($path)) {
            
            abort(404);
            
        }
        
        
        
        $file = File::get($path);
        
        $type = File::mimeType($path);
        
        
        
        $response = Response::make($file, 200);
        
        $response->header("Content-Type", $type);
        
        
        
        return $response;
        
    }
}