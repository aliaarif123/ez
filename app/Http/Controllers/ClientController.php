<?php

namespace App\Http\Controllers;
use App\User;
use App\Role;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    public function index()
    {
        return view('dashboard.client.index');
    }
    
    public function manageProfile()
    {
        return view('dashboard.client.manage-profile');
    }  
}