<?php

namespace App\Http\Controllers;
use App\Assignment;
use App\Test;
use Auth;
use Session;
use App\User;
use App\Profile;
use DB;
use Cache;
use App\LanguagePair;
Use Exception;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Mail\AcceptContractMail;
use Illuminate\Support\Facades\Mail;


class TranslatorController extends Controller
{
    public function index()
    {
        $assignments = [];
        
        $data = [
            'assignments' =>  $assignments,
        ];
        return view('dashboard.translator.index', $data);
    }
    
    
    public function educationFields(){
        return view('dashboard.translator.partials.educations.fields');
    }
    
    public function manageProfile($ucode = null)
    {
        if($ucode){
            $profile =  Profile::where('ucode', $ucode)->first();
            $languages = LanguagePair::where('flag', 'language')->where('ucode', $ucode)->get();
        }else{
            $profile =  Profile::where('ucode', Auth::user()->ucode)->with(['user'])->first();           
            $languages = LanguagePair::where('flag', 'language')->where('ucode', Auth::user()->ucode)->get(); 
        }
        
        // $countries = DB::table('countries')->orderBy('name', 'asc')->whereIn('id', [1, 2, 3, 9, 13, 15, 16, 17, 18, 19, 22, 23, 32, 34, 37, 38, 42, 44, 48, 53, 59, 60, 64, 66, 79, 80, 87, 93, 94, 101, 102, 103, 104, 105, 108, 111, 112,  117, 118, 121, 124, 132, 133, 134, 139, 148, 149, 157, 159, 160, 165, 166, 178, 191, 192, 195, 201, 204, 208, 213, 215, 218, 222, 223, 224, 227, 229, 234, 243])->get();

        // $countriesPhoneCodes = DB::table('countries')->orderBy('phonecode', 'asc')->whereIn('id', [1, 2, 3, 9, 13, 15, 16, 17, 18, 19, 22, 23, 32, 34, 37, 38, 42, 44, 48, 53, 59, 60, 64, 66, 79, 80, 87, 93, 94, 101, 102, 103, 104, 105, 108, 111, 112,  117, 118, 121, 124, 132, 133, 134, 139, 148, 149, 157, 159, 160, 165, 166, 178, 191, 192, 195, 201, 204, 208, 213, 215, 218, 222, 223, 224, 227, 229, 234, 243])->distinct('phonecode')->get('phonecode');



        $countries = DB::table('countries')->orderBy('name', 'asc')->get();

        $countriesPhoneCodes = DB::table('countries')->distinct('phonecode')->get('phonecode');


        return view('dashboard.translator.manage-profile', compact(['profile', 'countries', 'countriesPhoneCodes', 'languages']));
    } 
    
    public function checkAndUpdateFinalStatus($request, $profile)
    {
        $role = Auth::user()->role;
        //if($role == 'Translator'){
        $translator = Profile::where('ucode', $profile->ucode)->where('finalStatus', 'Profile Incomplete')->first();

        if(
            $role == 'Translator' && 
            $translator && 
            $profile->personal_information_flag == 1 &&
            $profile->skills_language_proficiency_flag == 1 &&
            $profile->skills_subject_matter_expertise_flag == 1 &&
            $profile->skills_software_and_tools_flag == 1  

        ){
           $updateProfile = Profile::where('ucode', Auth::user()->ucode)->update([
            'finalStatus' => 'Test Pending'
        ]);
           if($updateProfile){
               Session::put('finalStatus', Auth::user()->profile->finalStatus);
               return true;
           }else{
            return false;
        }
    }else{
        return false;
    }
}


public function updateProfile(Request $request)
{


    $profile = Profile::where('ucode', $request->ucode)->first();
    if($request->form_type == 'pm-banking-information'){
        //$this->checkAndUpdateFinalStatus($request, $profile);
        return $this->updatePmBankingInformation($request, $profile);
    }elseif($request->form_type == 'pm-alternate-payment'){
        //$this->checkAndUpdateFinalStatus($request, $profile);
        return $this->updatePmAlternatePayment($request, $profile);
    }elseif($request->form_type == 'pd-work-schedule'){
        //$this->checkAndUpdateFinalStatus($request, $profile);
        return $this->updatePdWorkSchedule($request, $profile);
    }elseif($request->form_type == 'pd-work-history'){
        //$this->checkAndUpdateFinalStatus($request, $profile);
        return $this->updatePdWorkHistory($request, $profile);
    }elseif($request->form_type == 'pd-identification-information'){
        //$this->checkAndUpdateFinalStatus($request, $profile);
        return $this->updatePdIdentificationInformation($request, $profile);
    }elseif($request->form_type == 'skills-software-and-tools'){
        //$this->checkAndUpdateFinalStatus($request, $profile);
        return $this->updateSkillsSoftwareAndTools($request, $profile);
    }elseif($request->form_type == 'skills-subject-matter-expertise'){
        //$this->checkAndUpdateFinalStatus($request, $profile);
        return $this->updateSkillsSubjectMatterExpertise($request, $profile);
    }elseif($request->form_type == 'skills-language-proficiency'){
        //$this->checkAndUpdateFinalStatus($request, $profile);
        return $this->updateSkillsLanguageProficiency($request, $profile);
    }else{
        //$this->checkAndUpdateFinalStatus($request, $profile);
        return $this->updatePersonalInformation($request, $profile);
    }           
}


public function updatePmBankingInformation($request, $profile){
    $proof_of_bank_path = 'bank_proofs/no-thumbnail.jpeg';
    if($request->has('proof_of_bank')){
        $request->validate([
            'proof_of_bank' => 'required|image|mimes:jpeg,jpg|mimes:application/pdf|max:400',
        ]);

        $extension = ".".$request->proof_of_bank->getClientOriginalExtension();
        $name = basename($request->proof_of_bank->getClientOriginalName(), $extension).time();
        $name = $request->ucode.'-address-'.$name.$extension;
        $proof_of_bank_path = $request->proof_of_bank->storeAs('bank_proofs', $name, 'public');        
    }else{
        $proof_of_bank_path = $profile->proof_of_bank;
    }
    $profile_save = Profile::where('ucode', $profile->ucode)->update([
        'bank_name' => $request->bank_name ?? null,
        'beneficiary_name' => $request->beneficiary_name ?? null,
        'beneficiary_account_number' => $request->beneficiary_account_number ?? null,
        'ifsc_code' => $request->ifsc_code ?? null,
        'bank_branch_address' => $request->bank_branch_address ?? null,
        'proof_of_bank_type' => $request->proof_of_bank_type,
        'proof_of_bank' => $proof_of_bank_path,
        'paypal_email' => $request->paypal_email ?? null,
        'payments_info_flag' => 1,
        'saveBtnStatus' => 0
    ]);
    if($profile_save){
        return response()->json(['message' => true, 'filled' => true], 200);
    }else{
        return response()->json(['message' => false, 'filled' => false], 302);
    }            
} 


public function updatePmAlternatePayment($request, $profile){
    $profile_save = Profile::where('ucode', $profile->ucode)->update([
        'paypal_email' => $request->paypal_email ?? null,
        'payments_info_flag' => 1,
        'saveBtnStatus' => 0
    ]);
    if($profile_save){
        return response()->json(['message' => true, 'filled' => true], 200);
    }else{
        return response()->json(['message' => false, 'filled' => false], 302);
    }  
}


public function updatePdWorkSchedule($request, $profile){
    $arrOfSunday = [];
    $arrOfMonday = [];
    $arrOfTuesday = [];
    $arrOfWednesday = [];
    $arrOfThursday = [];
    $arrOfFriday = [];
    $arrOfSaturday = [];

    $arrOfSunday['sunday_check_1'] = isset($request->sunday_check_1) ?  1 :  0;
    $arrOfSunday['sunday_check_2'] = isset($request->sunday_check_2) ?  1 :  0;
    $arrOfSunday['sunday_check_3'] = isset($request->sunday_check_3) ?  1 :  0;
    $arrOfSunday['sunday_check_4'] = isset($request->sunday_check_4) ?  1 :  0;
    $arrOfSunday['sunday_check_5'] = isset($request->sunday_check_5) ?  1 :  0;
    $arrOfSunday['sunday_check_6'] = isset($request->sunday_check_6) ?  1 :  0;

    $arrOfMonday['monday_check_1'] = isset($request->monday_check_1) ?  1 :  0;
    $arrOfMonday['monday_check_2'] = isset($request->monday_check_2) ?  1 :  0;
    $arrOfMonday['monday_check_3'] = isset($request->monday_check_3) ?  1 :  0;
    $arrOfMonday['monday_check_4'] = isset($request->monday_check_4) ?  1 :  0;
    $arrOfMonday['monday_check_5'] = isset($request->monday_check_5) ?  1 :  0;
    $arrOfMonday['monday_check_6'] = isset($request->monday_check_6) ?  1 :  0;

    $arrOfTuesday['tuesday_check_1'] = isset($request->tuesday_check_1) ?  1 :  0;
    $arrOfTuesday['tuesday_check_2'] = isset($request->tuesday_check_2) ?  1 :  0;
    $arrOfTuesday['tuesday_check_3'] = isset($request->tuesday_check_3) ?  1 :  0;
    $arrOfTuesday['tuesday_check_4'] = isset($request->tuesday_check_4) ?  1 :  0;
    $arrOfTuesday['tuesday_check_5'] = isset($request->tuesday_check_5) ?  1 :  0;
    $arrOfTuesday['tuesday_check_6'] = isset($request->tuesday_check_6) ?  1 :  0;

    $arrOfWednesday['wednesday_check_1'] = isset($request->wednesday_check_1) ?  1 :  0;
    $arrOfWednesday['wednesday_check_2'] = isset($request->wednesday_check_2) ?  1 :  0;
    $arrOfWednesday['wednesday_check_3'] = isset($request->wednesday_check_3) ?  1 :  0;
    $arrOfWednesday['wednesday_check_4'] = isset($request->wednesday_check_4) ?  1 :  0;
    $arrOfWednesday['wednesday_check_5'] = isset($request->wednesday_check_5) ?  1 :  0;
    $arrOfWednesday['wednesday_check_6'] = isset($request->wednesday_check_6) ?  1 :  0;
    $arrOfThursday['thursday_check_1'] = isset($request->thursday_check_1) ?  1 :  0;
    $arrOfThursday['thursday_check_2'] = isset($request->thursday_check_2) ?  1 :  0;
    $arrOfThursday['thursday_check_3'] = isset($request->thursday_check_3) ?  1 :  0;
    $arrOfThursday['thursday_check_4'] = isset($request->thursday_check_4) ?  1 :  0;
    $arrOfThursday['thursday_check_5'] = isset($request->thursday_check_5) ?  1 :  0;
    $arrOfThursday['thursday_check_6'] = isset($request->thursday_check_6) ?  1 :  0;

    $arrOfFriday['friday_check_1'] = isset($request->friday_check_1) ?  1 :  0;
    $arrOfFriday['friday_check_2'] = isset($request->friday_check_2) ?  1 :  0;
    $arrOfFriday['friday_check_3'] = isset($request->friday_check_3) ?  1 :  0;
    $arrOfFriday['friday_check_4'] = isset($request->friday_check_4) ?  1 :  0;
    $arrOfFriday['friday_check_5'] = isset($request->friday_check_5) ?  1 :  0;
    $arrOfFriday['friday_check_6'] = isset($request->friday_check_6) ?  1 :  0;

    $arrOfSaturday['saturday_check_1'] = isset($request->saturday_check_1) ?  1 :  0;
    $arrOfSaturday['saturday_check_2'] = isset($request->saturday_check_2) ?  1 :  0;
    $arrOfSaturday['saturday_check_3'] = isset($request->saturday_check_3) ?  1 :  0;
    $arrOfSaturday['saturday_check_4'] = isset($request->saturday_check_4) ?  1 :  0;
    $arrOfSaturday['saturday_check_5'] = isset($request->saturday_check_5) ?  1 :  0;
    $arrOfSaturday['saturday_check_6'] = isset($request->saturday_check_6) ?  1 :  0;

    $arryToJsonOfSunday = json_encode($arrOfSunday);
    $arryToJsonOfMonday = json_encode($arrOfMonday);
    $arryToJsonOfTuesday = json_encode($arrOfTuesday);
    $arryToJsonOfWednesday = json_encode($arrOfWednesday);
    $arryToJsonOfThursday = json_encode($arrOfThursday);
    $arryToJsonOfFriday = json_encode($arrOfFriday);
    $arryToJsonOfSaturday = json_encode($arrOfSaturday);

    $profile_save = Profile::where('ucode', $profile->ucode)->update([
        'sunday' => $arryToJsonOfSunday,
        'monday' => $arryToJsonOfMonday,
        'tuesday' => $arryToJsonOfTuesday,
        'wednesday' => $arryToJsonOfWednesday,
        'thursday' => $arryToJsonOfThursday,
        'friday' => $arryToJsonOfFriday,
        'saturday' => $arryToJsonOfSaturday,
        'pd_work_schedule_flag' => 1,
        'saveBtnStatus' => 0
    ]);
    if($profile_save){
        return response()->json(['message' => true, 'filled' => true], 200);
    }else{
        return response()->json(['message' => false, 'filled' => false], 302);
    }

}

public function updatePdWorkHistory($request, $profile){
    /* Type of Experence */ 
    if(isset($request->translation)){ $translation = 1; }else{ $translation = 0; }
    if(isset($request->proofreading)){ $proofreading = 1; }else{ $proofreading = 0; }
    if(isset($request->quality_assurance)){ $quality_assurance = 1; }else{ $quality_assurance = 0; }

    $proof_of_experience_path = 'address_proofs/no-thumbnail.jpeg';
    if($request->has('proof_of_experience')){
        if(file_exists('/public/storage/'.$profile->proof_of_experience)){
            File::delete('/public/storage/'.$profile->proof_of_experience);
        }
        $request->validate([
            'proof_of_experience' => 'required|image|mimes:jpeg,jpg|mimes:application/pdf|max:400',
        ]);
        $extension = ".".$request->proof_of_experience->getClientOriginalExtension();
        $name = basename($request->proof_of_experience->getClientOriginalName(), $extension).time();
        $name = $request->ucode.'-address-'.$name.$extension;
        $proof_of_experience_path = $request->proof_of_experience->storeAs('proof_of_experiences', $name, 'public');        
    }

    $profile_save = Profile::where('ucode', $profile->ucode)->update([
        'work_experience' => $request->work_experience,
        'translation' => $translation,
        'proofreading' => $proofreading,
        'quality_assurance' => $quality_assurance,
        'proof_of_experience' => $proof_of_experience_path,
        'pd_work_history_flag' => 1,
        'saveBtnStatus' => 0

    ]);

    if($profile_save){
        return response()->json(['message' => true, 'filled' => true], 200);
    }else{
        return response()->json(['message' => false, 'filled' => false], 302);
    }

}


public function updatePdIdentificationInformation($request, $profile){

    $photo_id_path = 'photo_ids/no-thumbnail.jpeg';
    if($request->has('photo_id')){
        $request->validate([
            'photo_id' => 'required|image|mimes:jpeg,jpg,application/pdf|max:400',
        ]);
        $extension = ".".$request->photo_id->getClientOriginalExtension();
        $name = basename($request->photo_id->getClientOriginalName(), $extension).time();
        $name = $request->ucode.'-photo-'.$name.$extension;
        $photo_id_path = $request->photo_id->storeAs('photo_ids', $name, 'public');        
    }else{
        $photo_id_path = $profile->photo_id;
    }


    $address_proof_path = 'address_proofs/no-thumbnail.jpeg';
    if($request->has('address_proof')){
     $request->validate([
        'address_proof' => 'required|image|mimes:jpeg,jpg,application/pdf|max:400',
    ]);
     $extension = ".".$request->address_proof->getClientOriginalExtension();
     $name = basename($request->address_proof->getClientOriginalName(), $extension).time();
     $name = $request->ucode.'-address-'.$name.$extension;
     $address_proof_path = $request->address_proof->storeAs('address_proofs', $name, 'public');        
 }else{
    $address_proof_path = $profile->address_proof;
}



$profile_save = Profile::where('ucode', $profile->ucode)->update([
    'photo_id_type' => $request->photo_id_type,
    'photo_id' => $photo_id_path,
    'address_proof_type' => $request->address_proof_type,
    'address_proof' => $address_proof_path,
    'pd_identification_information_flag' => 1,
    'saveBtnStatus' => 0
]);
//$status = $this->checkAndUpdateFinalStatus($request, $profile);

if($profile_save){
    return response()->json(['message' => true, 'filled' => true], 200);
}else{
    return response()->json(['message' => false, 'filled' => false], 302);
}

}

public function evaluation($ucode = null){



    $ucode  ? $ucode : $ucode = Auth::user()->ucode; 
    $profile = Profile::where('ucode', $ucode)->first();
    
    $role = Auth::user()->role;
    $translator = Profile::where('ucode', $profile->ucode)->where('finalStatus', 'Profile Incomplete')->first();

    if(
        $role == 'Translator' && 
        $translator && 
        $profile->personal_information_flag == 1 && 
        $profile->skills_language_proficiency_flag == 1 &&
        $profile->skills_subject_matter_expertise_flag == 1 &&
        $profile->skills_software_and_tools_flag == 1
    )
    {

        $updateProfile = Profile::where('ucode', Auth::user()->ucode)
        ->update([
            'finalStatus' => 'Test Pending'
        ]);

        if($updateProfile){
         Session::put('finalStatus', Auth::user()->profile->finalStatus);
     }
 }




 $myLanguagePairs = LanguagePair::where('flag', 'language-pair')->where('ucode', $ucode)->pluck('name');
 $myFinalTests = [];   
 $err = ''; 
 $counter = 0;
                                    //dd($myLanguagePairs);
 foreach($myLanguagePairs as $singlePair){
    $row_exists = DB::table('test_attempt')->where('ucode', $ucode)->where('lang_pair', $singlePair)->pluck('test_id');
                                        //echo '<pre>';
                                        //echo count($row_exists) >= 1 ? count($row_exists) : '0' ;
    $explodedArr = explode('-', $singlePair);
    $test = Test::where('source_language', $explodedArr[0])->where('target_language', $explodedArr[1])->first();
                                        //echo '<pre>';
                                        //echo $test;

    $myTest = Test::inRandomOrder()
    ->where('source_language', $explodedArr[0])
    ->where('target_language', $explodedArr[1])
    ->take(2)->get();
    if($test){
        switch(count($row_exists)){
            case 1:
            try{
                $myTest1 =  Test::whereIn('id', $row_exists)
                ->where('source_language', $explodedArr[0])
                ->where('target_language', $explodedArr[1])
                ->take(1)->get();
            }catch(Exception $e){ /* dd($e->getMessage()); */ }
            if($myTest1){ $myFinalTests[$singlePair][] = $myTest1[0];}
            try{
                $myTest2 =  Test::inRandomOrder()
                ->where('id', '!=', $myTest1[0]['id'])
                ->where('source_language', $explodedArr[0])
                ->where('target_language', $explodedArr[1])
                ->take(1)->get();
            }catch(Exception $e){ /* dd($e->getMessage()); */ }
            if($myTest2){ $myFinalTests[$singlePair][] = $myTest2[0];}
            break;
            case 2:
            try{
                $myTest1 =  Test::whereIn('id', $row_exists)
                ->where('source_language', $explodedArr[0])
                ->where('target_language', $explodedArr[1])
                ->take(2)->get();
            }catch(Exception $e){ /* dd($e->getMessage()); */ }
            if($myTest1){
                $myFinalTests[$singlePair][] = $myTest1[0];
                $myFinalTests[$singlePair][] = $myTest1[1];
            }
            break;
            default:
            if($myTest){
                $myFinalTests[$singlePair][] = $myTest[0];
                $myFinalTests[$singlePair][] = $myTest[1];
            } 

        }
    }
    $counter++;
}
$myAllPossibleTests = $myFinalTests;


if(
    $role == 'Translator' && 
    $translator && 
    $profile->personal_information_flag == 1 && 
    $profile->skills_language_proficiency_flag == 1 &&
    $profile->skills_subject_matter_expertise_flag == 1 &&
    $profile->skills_software_and_tools_flag == 1
)
{

    $updateProfile = Profile::where('ucode', Auth::user()->ucode)
    ->update([
        'finalStatus' => 'Test Pending'
    ]);

    if($updateProfile){
     Session::put('finalStatus', 'Test Pending');
     $status = Auth::user()->profile->finalStatus;
 }
}




return view('dashboard.translator.my-profile.evaluation', compact(['myAllPossibleTests', 'ucode', 'status']));    
}

public function publicProfile(){
    return view('dashboard.translator.my-profile.public-profile');
}



public function showUserTestSingle($lang_pair = null, $test_id = null, $ucode = null){

    $test_id = base64_decode($test_id);
    $test =  Test::find($test_id);
                                    //dd($test->test_duration);
    if(!$ucode){ $ucode = Auth::user()->ucode; }
    else{ $ucode = $ucode; }    
    $testAttempt =  DB::table('test_attempt')->where('ucode', $ucode)->where('test_id', $test_id)->first();
    if(!$testAttempt){
        DB::table('test_attempt')->insert(
            [
                'ucode' => $ucode, 
                'test_id' => $test_id,
                'test_started_at' => now(),
                'status' => 0,
                'lang_pair' => $lang_pair,
                'total_time' => $test->test_duration*60,
                'attempt_flag' => 0,
                'touch' => 1,
            ]
        );
        
    }else{
        DB::table('test_attempt')
        ->where('ucode', $ucode)
        ->where('test_id', $test_id)
        ->update([
            'ucode' => $ucode, 
            'test_id' => $test_id,
            'status' => 1,
            'total_time' => $testAttempt->total_time - $test->test_duration,
            'attempt_flag' => $testAttempt->attempt_flag + 1,
            'attempt_status' => 'done',
            'touch' => $testAttempt->touch + 1,
            'submit' => 1,        
        ]);


    }
    $test = Test::where('status', 1)->where('id', $test_id)->first();


    $test_exist = DB::table('test_attempt')->where('test_id', $test->id)->where('ucode', $ucode)->where('lang_pair', $lang_pair)->first();

    if($test){
        $test_attempt =  DB::table('test_attempt')->where('test_ended_at', null)->where('id', $test_exist->id)->first(); 

        //dd(2);

        $this->changeStatus1($lang_pair);

        return view('dashboard.translator.take-test', compact(['test', 'ucode', 'test_exist', 'test_attempt']));                          
    }else{
        return redirect()->route('dashboard.translator.showTestResults');
    }
}


public function changeStatus1($lp){

    $role = Auth::user()->role;
    $translator = Profile::where('ucode', Auth::user()->ucode)->where('finalStatus', 'Test Pending')->first();

    $test_attempts = DB::table('test_attempt')
    ->where('ucode', Auth::user()->ucode)
    ->where('lang_pair', $lp)
    ->where('status', 1)
    ->where('attempt_status', 'done')
    ->count();

    if($role == 'Translator' && $translator &&  $test_attempts == 2){
        Profile::where('ucode', Auth::user()->ucode)->update([
            'finalStatus' => 'Under Evaluation'
        ]);
        Session::put('finalStatus', Auth::user()->profile->finalStatus);
    }

}

public function attemptTest(Request $request){

    DB::table('test_attempt')
    ->where('ucode', Auth::user()->ucode)
    ->where('test_id', $request->test_id)
    ->where('lang_pair', $request->lang_pair)
    ->update([
        'ucode' => Auth::user()->ucode, 
        'test_id' => $request->test_id,
        'test_ended_at' => now(),
        'answer' => $request->ans_description,
        'status' => 1,    
        'attempt_status' => 'done'                 
    ]);

    //dd(1);

    $this->changeStatus1($request->lang_pair);

    return redirect()->route('dashboard.translator.myprofile.evaluation');
}


public function updatePersonalInformation($request, $profile){
    $this->validate($request, [
        'first_name' => 'required|string',
        'last_name' => 'required|string',
        'gender' => 'required|string',
        'nationality' => 'required',
        'country' => 'required',
        'country_phonecode_mobile' => 'required',
        'mobile' => 'required|numeric',
        'country_phonecode_whatsapp' => 'required',
        'whatsapp_mobile' => 'required|numeric'                
    ]);
    $profile_save = Profile::where('ucode', $profile->ucode)->update([
        'first_name' => $request->first_name,
        'middle_name' => $request->middle_name,
        'last_name' => $request->last_name,
        'gender' => $request->gender,
        'nationality' => $request->nationality,
        'country' => $request->country,
        'country_phonecode_mobile' => $request->country_phonecode_mobile,
        'mobile' => $request->mobile,
        'country_phonecode_whatsapp' => $request->country_phonecode_whatsapp,
        'whatsapp_mobile' => $request->whatsapp_mobile,
        'personal_information_flag' => 1,
        'saveBtnStatus' => 0

    ]);
    $status = $this->checkAndUpdateFinalStatus($request, $profile);
    if($profile_save){
        return response()->json(['message' => true, 'filled' => true, 'status' => $status], 200);
    }else{
        return response()->json(['message' => false, 'filled' => false, 'status' => false], 302);
    }

}



public function updateSkillsLanguageProficiency($request, $profile){
                                                        //return $request->language_count;
    $languages = [];
    $finalArr = [];
    if($request->record_flag == 'insert_record'){
        for($i = 0; $i < $request->language_count; $i++){
            $tempName = $_POST['lang_'.$i];
            $name = substr($tempName, 0, -2);
            $prof = substr($tempName, -1);
            $languageRowExist = DB::table('language_pairs')
            ->where('ucode', $request->ucode)
            ->where('user_id', $request->user_id)
            ->where('flag', 'language')
            ->where('name', $name)
            ->first();
            if(!$languageRowExist){
                DB::table('language_pairs')
                ->insert([
                    'ucode' => $request->ucode,
                    'user_id' => $request->user_id,
                    'flag' => 'language',
                    'name' =>  $name,
                    'proficiency' => $prof,
                    'created_at' => now()
                ]);

            }
            array_push($languages, $name);    
        }


        $fianlArr = $this->sampling($languages, 2);
        $arr = [];
        foreach($fianlArr as $singlePair){
            $exp = explode('-', $singlePair);
            if($exp[0] != $exp[1]){
                $languagePairRowExist = DB::table('language_pairs')
                ->where('ucode', $request->ucode)
                ->where('user_id', $request->user_id)
                ->where('flag', 'language-pair')
                ->where('name', $singlePair)
                ->first();            
                if(!$languagePairRowExist){
                    DB::table('language_pairs')
                    ->insert([
                        'ucode' => $request->ucode ?? Auth::user()->ucode,
                        'user_id' => $request->user_id,
                        'flag' => 'language-pair',
                        'name' =>  $singlePair,
                        'created_at' => now()                    
                    ]);

                }
            }  
        } 

    }else{
        for($i = 0; $i < $request->language_count; $i++){
            $tempName = $_POST['lang_'.$i];
            $name = substr($tempName, 0, -2);
            $prof = substr($tempName, -1);
            $languageRowExist = DB::table('language_pairs')
            ->where('ucode', $request->ucode)
            ->where('user_id', $request->user_id)
            ->where('flag', 'language')
            ->where('name', $name)
            ->first();

            array_push($languages, $name);

            if(!$languageRowExist){
                DB::table('language_pairs')
                ->insert([
                    'ucode' => $request->ucode,
                    'user_id' => $request->user_id,
                    'flag' => 'language',
                    'name' =>  $name,
                    'proficiency' => $prof,
                    'created_at' => now()
                ]);

            }else{
                DB::table('language_pairs')
                ->where('ucode', $request->ucode)
                ->where('user_id', $request->user_id)
                ->where('flag', 'language')
                ->where('name', $name)
                ->update([
                    'proficiency' => $prof,
                    'updated_at' => now()
                ]);

            }
        }


        $fianlArr = $this->sampling($languages, 2);
        $arr = [];
        foreach($fianlArr as $singlePair){
            $exp = explode('-', $singlePair);
            if($exp[0] != $exp[1]){
                $languagePairRowExist = DB::table('language_pairs')
                ->where('ucode', $request->ucode)
                ->where('user_id', $request->user_id)
                ->where('flag', 'language-pair')
                ->where('name', $singlePair)
                ->first();
                if(!$languagePairRowExist){
                    DB::table('language_pairs')
                    ->insert([
                        'ucode' => $request->ucode ?? Auth::user()->ucode,
                        'user_id' => $request->user_id,
                        'flag' => 'language-pair',
                        'name' =>  $singlePair                                 
                    ]);

                }

            }  
        } 
    }
    $ucode  = $request->ucode ?? Auth::user()->ucode;
    $profile_save = Profile::where('ucode', $ucode)->update(['skills_language_proficiency_flag' => 1]);

    $status = $this->checkAndUpdateFinalStatus($request, $profile);

    return response()->json(['message' => true, 'filled' => true, 'status' => $status], 200);



}




public function updateSkillsSubjectMatterExpertise($request, $profile){
    $UserFinalExpertiseArr = [];
    $UserFinalExpertiseArr['business'] =  $request->business ?? 0;
    $UserFinalExpertiseArr['technology'] =  $request->technology ?? 0;
    $UserFinalExpertiseArr['finance'] =  $request->finance ?? 0;
    $UserFinalExpertiseArr['marketing'] =  $request->marketing ?? 0;
    $UserFinalExpertiseArr['PR_media'] =  $request->PR_media ?? 0;
    $UserFinalExpertiseArr['legal'] =  $request->legal ?? 0;
    $UserFinalExpertiseArr['educational'] =  $request->educational ?? 0;
    $UserFinalExpertiseArr['medical'] =  $request->medical ?? 0;
    $UserFinalExpertiseArr['literature'] =  $request->literature ?? 0;
    $UserFinalExpertiseArr['sports'] =  $request->sports ?? 0;
    $UserFinalExpertiseArr['app_software'] =  $request->app_software ?? 0;
    $UserFinalExpertiseArr['general_research'] =  $request->general_research ?? 0;
    $arryToJsonAllExpertise = json_encode($UserFinalExpertiseArr, true);
    $profile_save = Profile::where('ucode', $profile->ucode)->update([
        'expertise' => $UserFinalExpertiseArr,
        'skills_subject_matter_expertise_flag' => 1,
        'saveBtnStatus' => 0
    ]);

    $status = $this->checkAndUpdateFinalStatus($request, $profile);

    if($profile_save){
        return response()->json(['message' => true, 'filled' => true, 'status' => $status], 200);
    }else{
        return response()->json(['message' => false, 'filled' => false, 'status' => false], 302);
    }
}


public function updateSkillsSoftwareAndTools($request, $profile){
    /* MS Office Tools */ 
    if(isset($request->ms_office_tool_powerPoint)){ $ms_office_tool_powerPoint = 1; }else{ $ms_office_tool_powerPoint = 0; }
    if(isset($request->ms_office_tool_Word)){ $ms_office_tool_Word = 1; }else{ $ms_office_tool_Word = 0; }
    if(isset($request->ms_office_tool_Excel)){ $ms_office_tool_Excel = 1; }else{ $ms_office_tool_Excel = 0; }
    if(isset($request->ms_office_tool_Visio)){ $ms_office_tool_Visio = 1; }else{ $ms_office_tool_Visio = 0; }

    /* CAT Tools */ 
    if(isset($request->cat_tool_MemoQ)){ $cat_tool_MemoQ = 1; }else{ $cat_tool_MemoQ = 0; }
    if(isset($request->cat_tool_sds_trados)){ $cat_tool_sds_trados = 1; }else{ $cat_tool_sds_trados = 0; }
    if(isset($request->cat_tool_other)){ $cat_tool_other = 1; }else{ $cat_tool_other = 0; }

    $profile_save = Profile::where('ucode', $profile->ucode)->update([
        'ms_office_tool_powerPoint' => $ms_office_tool_powerPoint,
        'ms_office_tool_Word' => $ms_office_tool_Word,
        'ms_office_tool_Excel' => $ms_office_tool_Excel,
        'ms_office_tool_Visio' => $ms_office_tool_Visio,
        'cat_tool_MemoQ' => $cat_tool_MemoQ,
        'cat_tool_sds_trados' => $cat_tool_sds_trados,
        'cat_tool_other' => $cat_tool_other,
        'skills_software_and_tools_flag' => 1,
        'saveBtnStatus' => 0   
    ]);


    $status = $this->checkAndUpdateFinalStatus($request, $profile);

    if($profile_save){
        return response()->json(['message' => true, 'filled' => true, 'status' => $status], 200);
    }else{
        return response()->json(['message' => false, 'filled' => false, 'status' => false], 302);
    }
}





public function updateDocumentsInfo($request, $profile){
    $this->validate($request, [
        'photo_id_type' => 'required|string',
        'photo_id' => 'required|mimes:jpeg,jpg,JPG,JPEG',
        'address_proof_type' => 'required|string',
        'address_proof' => 'required|mimes:jpeg,jpg,JPG,JPEG'              
    ]);

    if($request->has('photo_id')){
        $photo_id_path = 'photo_ids/no-thumbnail.jpeg';
        if(file_exists('/public/storage/'.$profile->photo_id)){
            File::delete('/public/storage/'.$profile->photo_id);
        }
        $request->validate([
            'photo_id' => 'required|image|mimes:jpeg,jpg|mimes:application/pdf|max:400',
        ]);
        $extension = ".".$request->photo_id->getClientOriginalExtension();
        $name = basename($request->photo_id->getClientOriginalName(), $extension).time();
        $name = $request->ucode.'-photo-'.$name.$extension;
        $photo_id_path = $request->photo_id->storeAs('photo_ids', $name, 'public');
    }

    if($request->has('address_proof')){
        $address_proof_path = 'address_proofs/no-thumbnail.jpeg';
        if(file_exists('/public/storage/'.$profile->address_proof)){
            File::delete('/public/storage/'.$profile->address_proof);
        }
        $request->validate([
            'address_proof' => 'required|image|mimes:jpeg,jpg|mimes:application/pdf|max:400',
        ]);
        $extension = ".".$request->address_proof->getClientOriginalExtension();
        $name = basename($request->address_proof->getClientOriginalName(), $extension).time();
        $name = $request->ucode.'-address-'.$name.$extension;
        $address_proof_path = $request->address_proof->storeAs('address_proofs', $name, 'public');        
    }

    $profile_save = Profile::where('ucode', $profile->ucode)->update([
        'photo_id_type' => $request->photo_id_type,
        'photo_id' => $photo_id_path,
        'address_proof_type' => $request->address_proof_type,
        'address_proof' => $address_proof_path,
        'documents_info_flag' => 1,
        'saveBtnStatus' => 0
    ]);


    if($profile_save){
        return response()->json(['message' => true, 'filled' => true], 200);
    }else{
        return response()->json(['message' => false, 'filled' => false], 302);
    }

}

public function updateExperienceInfo($request, $profile){
    /* Type of Experence */ 
    if(isset($request->translation)){ $translation = 1; }else{ $translation = 0; }
    if(isset($request->proofreading)){ $proofreading = 1; }else{ $proofreading = 0; }
    if(isset($request->quality_assurance)){ $quality_assurance = 1; }else{ $quality_assurance = 0; }

    /* MS Office Tools */ 
    if(isset($request->ms_office_tool_powerPoint)){ $ms_office_tool_powerPoint = 1; }else{ $ms_office_tool_powerPoint = 0; }
    if(isset($request->ms_office_tool_Word)){ $ms_office_tool_Word = 1; }else{ $ms_office_tool_Word = 0; }
    if(isset($request->ms_office_tool_Excel)){ $ms_office_tool_Excel = 1; }else{ $ms_office_tool_Excel = 0; }
    if(isset($request->ms_office_tool_Visio)){ $ms_office_tool_Visio = 1; }else{ $ms_office_tool_Visio = 0; }

    /* CAT Tools */ 
    if(isset($request->cat_tool_MemoQ)){ $cat_tool_MemoQ = 1; }else{ $cat_tool_MemoQ = 0; }
    if(isset($request->cat_tool_sds_trados)){ $cat_tool_sds_trados = 1; }else{ $cat_tool_sds_trados = 0; }
    if(isset($request->cat_tool_other)){ $cat_tool_other = 1; }else{ $cat_tool_other = 0; }

    $proof_of_experience_path = 'address_proofs/no-thumbnail.jpeg';
    if($request->has('proof_of_experience')){
        if(file_exists('/public/storage/'.$profile->proof_of_experience)){
            File::delete('/public/storage/'.$profile->proof_of_experience);
        }
        $request->validate([
            'proof_of_experience' => 'required|image|mimes:jpeg,jpg|mimes:application/pdf|max:400',
        ]);
        $extension = ".".$request->proof_of_experience->getClientOriginalExtension();
        $name = basename($request->proof_of_experience->getClientOriginalName(), $extension).time();
        $name = $request->ucode.'-address-'.$name.$extension;
        $proof_of_experience_path = $request->proof_of_experience->storeAs('proof_of_experiences', $name, 'public');        
    }



    $UserFinalExpertiseArr = [];


                                                                                            //$UserFinalExpertiseArr['ucode']= $profile->ucode;
    $UserFinalExpertiseArr['business'] =  $request->business ?? 0;
    $UserFinalExpertiseArr['technology'] =  $request->technology ?? 0;
    $UserFinalExpertiseArr['finance'] =  $request->finance ?? 0;
    $UserFinalExpertiseArr['marketing'] =  $request->marketing ?? 0;
    $UserFinalExpertiseArr['PR_media'] =  $request->PR_media ?? 0;
    $UserFinalExpertiseArr['legal'] =  $request->legal ?? 0;
    $UserFinalExpertiseArr['educational'] =  $request->educational ?? 0;
    $UserFinalExpertiseArr['medical'] =  $request->medical ?? 0;
    $UserFinalExpertiseArr['literature'] =  $request->literature ?? 0;
    $UserFinalExpertiseArr['sports'] =  $request->sports ?? 0;
    $UserFinalExpertiseArr['app_software'] =  $request->app_software ?? 0;
    $UserFinalExpertiseArr['general_research'] =  $request->general_research ?? 0;
    $arryToJsonAllExpertise = json_encode($UserFinalExpertiseArr, true);
    $profile_save = Profile::where('ucode', $profile->ucode)->update([
        'work_experience' => $request->work_experience,
        'translation' => $translation,
        'proofreading' => $proofreading,
        'quality_assurance' => $quality_assurance,
        'ms_office_tool_powerPoint' => $ms_office_tool_powerPoint,
        'ms_office_tool_Word' => $ms_office_tool_Word,
        'ms_office_tool_Excel' => $ms_office_tool_Excel,
        'ms_office_tool_Visio' => $ms_office_tool_Visio,
        'cat_tool_MemoQ' => $cat_tool_MemoQ,
        'cat_tool_sds_trados' => $cat_tool_sds_trados,
        'cat_tool_other' => $cat_tool_other,
        'proof_of_experience' => $proof_of_experience_path,
        'expertise' => $UserFinalExpertiseArr,
        'experience_info_flag' => 1,
        'saveBtnStatus' => 0
    ]);

                                                                                                //return response()->json(['message' => $request->type_of_experience], 200);

    if($profile_save){
        return response()->json(['message' => true, 'filled' => true], 200);
    }else{
        return response()->json(['message' => false, 'filled' => false], 302);
    }

}


public function updateAvailabilityInfo($request, $profile){
    $arrOfSunday = [];
    $arrOfMonday = [];
    $arrOfTuesday = [];
    $arrOfWednesday = [];
    $arrOfThursday = [];
    $arrOfFriday = [];
    $arrOfSaturday = [];

    $arrOfSunday['sunday_check_1'] = isset($request->sunday_check_1) ?  1 :  0;
    $arrOfSunday['sunday_check_2'] = isset($request->sunday_check_2) ?  1 :  0;
    $arrOfSunday['sunday_check_3'] = isset($request->sunday_check_3) ?  1 :  0;
    $arrOfSunday['sunday_check_4'] = isset($request->sunday_check_4) ?  1 :  0;
    $arrOfSunday['sunday_check_5'] = isset($request->sunday_check_5) ?  1 :  0;
    $arrOfSunday['sunday_check_6'] = isset($request->sunday_check_6) ?  1 :  0;

    $arrOfMonday['monday_check_1'] = isset($request->monday_check_1) ?  1 :  0;
    $arrOfMonday['monday_check_2'] = isset($request->monday_check_2) ?  1 :  0;
    $arrOfMonday['monday_check_3'] = isset($request->monday_check_3) ?  1 :  0;
    $arrOfMonday['monday_check_4'] = isset($request->monday_check_4) ?  1 :  0;
    $arrOfMonday['monday_check_5'] = isset($request->monday_check_5) ?  1 :  0;
    $arrOfMonday['monday_check_6'] = isset($request->monday_check_6) ?  1 :  0;

    $arrOfTuesday['tuesday_check_1'] = isset($request->tuesday_check_1) ?  1 :  0;
    $arrOfTuesday['tuesday_check_2'] = isset($request->tuesday_check_2) ?  1 :  0;
    $arrOfTuesday['tuesday_check_3'] = isset($request->tuesday_check_3) ?  1 :  0;
    $arrOfTuesday['tuesday_check_4'] = isset($request->tuesday_check_4) ?  1 :  0;
    $arrOfTuesday['tuesday_check_5'] = isset($request->tuesday_check_5) ?  1 :  0;
    $arrOfTuesday['tuesday_check_6'] = isset($request->tuesday_check_6) ?  1 :  0;

    $arrOfWednesday['wednesday_check_1'] = isset($request->wednesday_check_1) ?  1 :  0;
    $arrOfWednesday['wednesday_check_2'] = isset($request->wednesday_check_2) ?  1 :  0;
    $arrOfWednesday['wednesday_check_3'] = isset($request->wednesday_check_3) ?  1 :  0;
    $arrOfWednesday['wednesday_check_4'] = isset($request->wednesday_check_4) ?  1 :  0;
    $arrOfWednesday['wednesday_check_5'] = isset($request->wednesday_check_5) ?  1 :  0;
    $arrOfWednesday['wednesday_check_6'] = isset($request->wednesday_check_6) ?  1 :  0;
    $arrOfThursday['thursday_check_1'] = isset($request->thursday_check_1) ?  1 :  0;
    $arrOfThursday['thursday_check_2'] = isset($request->thursday_check_2) ?  1 :  0;
    $arrOfThursday['thursday_check_3'] = isset($request->thursday_check_3) ?  1 :  0;
    $arrOfThursday['thursday_check_4'] = isset($request->thursday_check_4) ?  1 :  0;
    $arrOfThursday['thursday_check_5'] = isset($request->thursday_check_5) ?  1 :  0;
    $arrOfThursday['thursday_check_6'] = isset($request->thursday_check_6) ?  1 :  0;

    $arrOfFriday['friday_check_1'] = isset($request->friday_check_1) ?  1 :  0;
    $arrOfFriday['friday_check_2'] = isset($request->friday_check_2) ?  1 :  0;
    $arrOfFriday['friday_check_3'] = isset($request->friday_check_3) ?  1 :  0;
    $arrOfFriday['friday_check_4'] = isset($request->friday_check_4) ?  1 :  0;
    $arrOfFriday['friday_check_5'] = isset($request->friday_check_5) ?  1 :  0;
    $arrOfFriday['friday_check_6'] = isset($request->friday_check_6) ?  1 :  0;

    $arrOfSaturday['saturday_check_1'] = isset($request->saturday_check_1) ?  1 :  0;
    $arrOfSaturday['saturday_check_2'] = isset($request->saturday_check_2) ?  1 :  0;
    $arrOfSaturday['saturday_check_3'] = isset($request->saturday_check_3) ?  1 :  0;
    $arrOfSaturday['saturday_check_4'] = isset($request->saturday_check_4) ?  1 :  0;
    $arrOfSaturday['saturday_check_5'] = isset($request->saturday_check_5) ?  1 :  0;
    $arrOfSaturday['saturday_check_6'] = isset($request->saturday_check_6) ?  1 :  0;

    $arryToJsonOfSunday = json_encode($arrOfSunday);
    $arryToJsonOfMonday = json_encode($arrOfMonday);
    $arryToJsonOfTuesday = json_encode($arrOfTuesday);
    $arryToJsonOfWednesday = json_encode($arrOfWednesday);
    $arryToJsonOfThursday = json_encode($arrOfThursday);
    $arryToJsonOfFriday = json_encode($arrOfFriday);
    $arryToJsonOfSaturday = json_encode($arrOfSaturday);

    $profile_save = Profile::where('ucode', $profile->ucode)->update([
        'sunday' => $arryToJsonOfSunday,
        'monday' => $arryToJsonOfMonday,
        'tuesday' => $arryToJsonOfTuesday,
        'wednesday' => $arryToJsonOfWednesday,
        'thursday' => $arryToJsonOfThursday,
        'friday' => $arryToJsonOfFriday,
        'saturday' => $arryToJsonOfSaturday,
        'availability_info_flag' => 1,
        'saveBtnStatus' => 0
    ]);
    if($profile_save){

        return response()->json(['message' => true, 'filled' => true], 200);
    }else{
        return response()->json(['message' => false, 'filled' => false], 302);
    }

}




public function updatePaymentsInfo($request, $profile){
                                                                                                    //dd($request->bank_statement);

    $bank_statement_path = 'bank_statements/no-thumbnail.jpeg';
    if($request->has('bank_statement')){
       $request->validate([
        'bank_statement' => 'required|image|mimes:jpeg,jpg|mimes:application/pdf|max:400',
    ]);
       $extension = ".".$request->bank_statement->getClientOriginalExtension();
       $name = basename($request->bank_statement->getClientOriginalName(), $extension).time();
       $name = $request->ucode.'-address-'.$name.$extension;
       $bank_statement_path = $request->bank_statement->storeAs('bank_statements', $name, 'public');        
   }else{
    $bank_statement_path = $profile->bank_statement;
}

$cancelled_cheque_path = 'cancelled_cheques/no-thumbnail.jpeg';
if($request->has('cancelled_cheque')){
    $request->validate([
        'cancelled_cheque' => 'required|image|mimes:jpeg,jpg|mimes:application/pdf|max:400',
    ]);
    $extension = ".".$request->cancelled_cheque->getClientOriginalExtension();
    $name = basename($request->cancelled_cheque->getClientOriginalName(), $extension).time();
    $name = $request->ucode.'-address-'.$name.$extension;
    $cancelled_cheque_path = $request->cancelled_cheque->storeAs('cancelled_cheques', $name, 'public');        
}else{
    $cancelled_cheque_path = $profile->bank_statement;
}



$profile_save = Profile::where('ucode', $profile->ucode)->update([
    'bank_country' => $request->bank_country ?? null,
    'bank_name' => $request->bank_name ?? null,
    'bank_branch_address' => $request->bank_branch_address ?? null,
    'beneficiary_name' => $request->beneficiary_name ?? null,
    'beneficiary_account_number' => $request->beneficiary_account_number ?? null,
    'ifsc_code' => $request->ifsc_code ?? null,
    'bank_statement' => $bank_statement_path,
    'cancelled_cheque' => $cancelled_cheque_path,
    'paypal_email' => $request->paypal_email ?? null,
    'payments_info_flag' => 1,
    'saveBtnStatus' => 0
]);



if($profile_save){
    return response()->json(['message' => true, 'filled' => true], 200);
}else{
    return response()->json(['message' => false, 'filled' => false], 302);
}

}



public function contracts(Request $request)
{
    $getUserCodefromURL = $request->route('ucode') ?? Auth::user()->ucode ;

    $languagePairRowExist = DB::table('language_pairs')
    ->where('ucode', $getUserCodefromURL)
    ->where('flag', 'language-pair')
    ->get();

    $translatorDetails= DB::table('profiles')
    ->where('ucode', $getUserCodefromURL)
    ->get();

                                                                                                                //dd($languagePairRowExist);

    return view('dashboard.translator.my-account.contracts', ['languagePairRowExist' => $languagePairRowExist, 'translatorDetails'=> $translatorDetails]);

}

public function ModalWithLpDetails(Request $request)
{

    $userTestID = $request->id;
    $tempArr = explode("XXX", $userTestID);
    $userCode = $tempArr[0];
    $lP_id = $tempArr[1];
    $lP_name = $tempArr[2];

                                                                                                                //return $userTestID;



    $languagePairRowExist = DB::table('language_pairs')
    ->where('ucode', $userCode)
    ->where('id', $lP_id)
    ->where('name', $lP_name)
    ->get();

                                                                                                                //return $languagePairRowExist;

    $finalRating        = $languagePairRowExist[0]->final_rating ?? 0;

                                                                                                                //dd($finalRating);

    $contract_flag      = $languagePairRowExist[0]->contract_flag ?? 0;
    $contract_type      = $languagePairRowExist[0]->contract_type ?? 0;
    $proofreader        = $languagePairRowExist[0]->proofreader ?? 0;
    $fixed_rate         = $languagePairRowExist[0]->fixed_rate ?? 0;
    $quota_rate         = $languagePairRowExist[0]->quota_rate ?? 0;
    $translation_wc     = $languagePairRowExist[0]->translation_wc ?? 0;
    $proofreading_wc    = $languagePairRowExist[0]->proofreading_wc ?? 0;


    if($languagePairRowExist){

        return $finalRating.'X-X'.$contract_flag.'X-X'.$contract_type.'X-X'.$proofreader.'X-X'.$fixed_rate.'X-X'.$quota_rate.'X-X'.$translation_wc.'X-X'.$proofreading_wc.'X-X'.$userCode.'X-X'.$lP_id.'X-X'.$lP_name;
    }else{
        return "FAIL@@";
    }

}

public function saveModalDetails(Request $request)
{

                                                                                                                //echo '<pre>';
                                                                                                                //return $request->all();

    $userCode = $request->dyn_ucode;
    $lP_id = $request->dyn_lpid;
    $lP_name = $request->dyn_lpname;

    if(isset($request->proofreader)){
        $checkVal = 1;
    }else{
        $checkVal = 0;
    }

    $allGood = DB::table('language_pairs')
    ->where('ucode', $userCode)
    ->where('id', $lP_id)
    ->where('name', $lP_name)
    ->update([
        'contract_flag'     => $request->contract_flag,
        'contract_type'     => $request->contract_type,
        'final_rating'      => $request->edit_rating,
        'proofreader'       => $checkVal,
        'fixed_rate'        => $request->fixed_rate,
        'quota_rate'        => $request->quota_rate,
        'translation_wc'    => $request->translation_wc,
        'proofreading_wc'   => $request->proofreading_wc,
    ]);

    if($allGood){
        return 'OKK';
    }else{
        return "FAIL";
    }

}




public function acceptContract(Request $request)
{


    $languagePairRowExist = DB::table('language_pairs')
    ->where('ucode', $request->ucode)
    ->where('flag', 'language-pair')
    ->where('name', $request->name)
    ->where('id', $request->lp_id)
    ->update([
        'contract_accept' => 1
    ]);

    if($languagePairRowExist){
        $lp_data = LanguagePair::findOrFail($request->lp_id);
        $admin_email = $_SERVER['REMOTE_ADDR'] == "127.0.0.1" ? "admin@example.com" : "support@arabeasy.com";
        Mail::to($admin_email)->send(new AcceptContractMail(Auth::user(), $lp_data));
        Mail::to(Auth::user()->email)->send(new AcceptContractMail(Auth::user(), $lp_data));   



        $updateProfile = Profile::where('ucode', $request->ucode)->update([
            'finalStatus' => 'On-Pilot'
        ]);
        if($updateProfile){
            Session::put('finalStatus', 'On-Pilot');
        }




        return back()->with(['msg' => 'You have successfully accepted the contract']);
    }else{
        return back()->with(['msg' => 'Have some problem in accepting the contract']);
    }
}

public function personalInformation($ucode = null){
    $ucode = $ucode ? $ucode : Auth::user()->ucode;
    $profile =  Profile::where('ucode', $ucode)->first();

                                                                                                                        //$countries = DB::table('countries')->distinct()->orderBy('phonecode', 'asc')->get(['phonecode', 'name']);

                                                                                                                        //dd($profile);
    $countries = DB::table('countries')->orderBy('name', 'asc')->whereIn('id', [1, 2, 3, 9, 13, 15, 16, 17, 18, 19, 22, 23, 32, 34, 37, 38, 42, 48, 53, 59, 60, 64, 66, 79, 80, 87, 93, 94, 101, 102, 103, 104, 105, 108, 111, 112,  117, 118, 121, 124, 132, 133, 134, 139, 148, 149, 157, 159, 160, 165, 166, 178, 191, 192, 195, 201, 204, 208, 213, 215, 218, 222, 223, 224, 227, 229, 234, 243])->get();
    $countriesPhoneCodes = DB::table('countries')->orderBy('phonecode', 'asc')->whereIn('id', [1, 2, 3, 9, 13, 15, 16, 17, 18, 19, 22, 23, 32, 34, 37, 38, 42, 48, 53, 59, 60, 64, 66, 79, 80, 87, 93, 94, 101, 102, 103, 104, 105, 108, 111, 112,  117, 118, 121, 124, 132, 133, 134, 139, 148, 149, 157, 159, 160, 165, 166, 178, 191, 192, 195, 201, 204, 208, 213, 215, 218, 222, 223, 224, 227, 229, 234, 243])->distinct('phonecode')->get('phonecode');

                                                                                                                        //dd($countriesPhoneCodes);

    return view('dashboard.translator.my-profile.personal-information', compact(['profile', 'countries', 'countriesPhoneCodes']));
}


public function skills($ucode = null){
    $ucode = $ucode ? $ucode : Auth::user()->ucode;
    $profile =  Profile::where('ucode', $ucode)->first();
    $languages = LanguagePair::where('flag', 'language')->where('ucode', $ucode)->get();
    $countries = DB::table('countries')->get();
    return view('dashboard.translator.my-profile.skills', compact(['profile', 'countries', 'languages']));
}

public function professionalDetails($ucode = null){
    $ucode = $ucode ? $ucode : Auth::user()->ucode;
    $profile =  Profile::where('ucode', $ucode)->first();
    $countries = DB::table('countries')->get();
    return view('dashboard.translator.my-profile.professional-details', compact(['profile', 'countries']));
}

public function paymentMethod($ucode = null){
    $ucode = $ucode ? $ucode : Auth::user()->ucode;
    $profile =  Profile::where('ucode', $ucode)->first();
    $countries = DB::table('countries')->get();
    return view('dashboard.translator.my-profile.payment-method', compact(['profile', 'countries']));
}

public function myAccount($ucode = null){
    $ucode = $ucode ? $ucode : Auth::user()->ucode;
    $profile =  Profile::where('ucode', $ucode)->first();
    $countries = DB::table('countries')->get();
    return view('dashboard.translator.my-account', compact(['profile', 'countries']));
}

public function activeAssignments($ucode = null){
    $ucode = $ucode ? $ucode : Auth::user()->ucode;
    $profile =  Profile::where('ucode', $ucode)->first();
    $countries = DB::table('countries')->get();
    return view('dashboard.translator.my-assignments.active-assignments', compact(['profile', 'countries']));
}

public function liveFeed($ucode = null){
    $ucode = $ucode ? $ucode : Auth::user()->ucode;
    $profile =  Profile::where('ucode', $ucode)->first();
    $countries = DB::table('countries')->get();
    return view('dashboard.translator.my-assignments.live-feed', compact(['profile', 'countries']));
}




public function sampling($chars, $size, $combinations = array()) {
    if (empty($combinations)) { $combinations = $chars; }
    if ($size == 1) {     return $combinations; }
    $new_combinations = [];
    foreach ($combinations as $combination) {
        foreach ($chars as $char) { $new_combinations[] = $combination .'-'. $char; }
    }
    return $this->sampling($chars, $size - 1, $new_combinations);    
}

public function changeTestShowFlag($langPairName){

    $checkFlag = LanguagePair::where('ucode', Auth::user()->ucode)->where('name', $langPairName)->first();

    if($checkFlag->showTest == 0){
        $changeFlag = LanguagePair::where('ucode', Auth::user()->ucode)->where('name', $langPairName)->update([
            'showTest' => 1
        ]);
    }else{
       $changeFlag = LanguagePair::where('ucode', Auth::user()->ucode)->where('name', $langPairName)->update([
        'showTest' => 0
    ]);
   }
   if($changeFlag){
    return response()->json(['message' => true, 'flagChanged' => true], 200);
}else{
    return response()->json(['message' => false, 'flagChanged' => false], 302);
}  
}



}