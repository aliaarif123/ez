<?php

namespace App\Http\Controllers;
use App\User;
use App\Role;
use Illuminate\Http\Request;

class OuController extends Controller
{
    public function index()
    {
        return view('dashboard.ou.index');
    }
    
    public function manageProfile()
    {
        return view('dashboard.ou.manage-profile');
    }  
}