<?php

namespace App\Http\Controllers;
use App\User;
use App\Role;
use Illuminate\Http\Request;

class QaController extends Controller
{
    public function index()
    {
        return view('dashboard.qa.index');
    }
    
    public function manageProfile()
    {
        return view('dashboard.qa.manage-profile');
    }  
}