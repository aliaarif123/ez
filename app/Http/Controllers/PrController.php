<?php

namespace App\Http\Controllers;
use App\User;
use App\Role;
use Illuminate\Http\Request;

class PrController extends Controller
{
    public function index()
    {
        return view('dashboard.pr.index');
    }
    
    public function manageProfile()
    {
        return view('dashboard.pr.manage-profile');
    }  
}