<?php


namespace App\Http\Controllers;

use App\User;
use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{

    // public function __construct() {
    //     //$this->middleware('auth')->except(['getSignUpPage', 'getSignInPage', 'getLogout']);
    // }


    public function getSignUpPage()
    {
        return view('auth.register');
    }

    public function getSignInPage()
    {
        return view('auth.login');
    }

    public function postSignUp(Request $request)
    {
        $user = new User();
        $user->first_name = $request['first_name'];
        $user->last_name = $request['last_name'];
        $user->email = $request['email'];
        $user->password = $request['password'];
        $user->save();
        $user->roles()->attach(Role::where('name', 'Translator')->first());
        Auth::login($user);
        return redirect()->route('main');
    }

    public function postSignIn(Request $request)
    {


$username = $request->username; //the input field has name='username' in form

$password = $request->password; //the input field has name='username' in form

if(filter_var($username, FILTER_VALIDATE_EMAIL)) {
    //user sent their email 
    Auth::attempt(['email' => $username, 'password' => $password]);
} else {
    //they sent their username instead 
    Auth::attempt(['username' => $username, 'password' => $password]);
}

//was any of those correct ?
if ( Auth::check() ) {
    //send them where they are going 
    
    //dd('1');
    //dd(Auth::user()->with('roles'));
    return redirect()->intended('dashboard');
}else{

    //dd('2');
    return redirect()->intended('auth.login');
}

//Nope, something wrong during authentication 
return redirect()->back()->withErrors([
    'credentials' => 'Please, check your credentials'
]);




        /*if (Auth::attempt(['email' => $request['email'], 'password' => $request['password']])) {
            return redirect()->route('main');
        }
        return redirect()->back();*/
    }

    public function getLogout()
    {
        Auth::logout();
        return redirect()->route('login');
    }
}
