<?php

namespace App\Http\Controllers;
use App\User;
use App\Profile;
use App\Role;
use App\Test;
use DB;
use Session;
use Illuminate\Http\Request;
use App\Mail\MailActivationlink;
use App\Mail\ProfileActivationMail;
use Illuminate\Support\Facades\Mail;

class AdminController extends Controller
{
    public function index()
    {
        $users = User::where('status', true)->where('role','!=',"Translator")->get();
        //dd($users);
        return view('dashboard.admin.index', ['users' => $users, 'user_count' => count($users)]);

    }
    
    public function manageUsers()
    {
        $users = User::where('status', true)->where('role','!=',"Translator")->get();
        //dd($users);
        return view('dashboard.admin.manage-users', ['users' => $users, 'user_count' => count($users)]);
    }

    public function manageRoles()
    {
        $users = User::where('status', true)->where('role','!=',"Admin")->with('roles')->get();

        //dd($users[0]->roles[0]->name);

        //dd($users);

        // $roles = [];

        // foreach ($users as  $user) {
        //     foreach ($user->roles as  $value) {
        //         //echo '<pre>'.$value->name;
        //         array_push($roles, $value->name);

        //         echo in_array('Admin', $roles ) ?  1 : 0;

        //     }   
        // }


        return view('dashboard.admin.manage-roles', ['users' => $users, 'user_count' => count($users)]);
    }

    public function manageRolesAction(Request $request)
    {
        //return response()->json(['message' => $request->roles], 200);
        $user = User::where('ucode', $request->ucode)->first();
        if($request->roles){
            for($i = 1; $i <= 5; $i++){
                $this->updateRoles($i, $request->roles, $user);
            }
            return response()->json(['message' => 'Roles updated for '.$user->profile->first_name], 200);
        }else{
            $user_role = DB::table('user_role')->where('user_id', $user->id)->delete();
            return response()->json(['message' => 'All Roles deleted for '.$user->profile->first_name], 200);
        }

    }


    public function updateRoles($i, $roles, $user)
    {

        if(in_array($i, $roles)){

            $role = DB::table('user_role')->where('user_id', $user->id)->where('role_id', $i)->first();
            if(!$role){
                DB::table('user_role')->insert([
                    'user_id' => $user->id,
                    'role_id' => $i
                ]);
            }
        }else{
            DB::table('user_role')->where('user_id', $user->id)->where('role_id', $i)->delete();
        }

        return true;

    }


    public function manageTranslotors()
    {
        $users = User::where('status', true)->where('role','=',"Translator")->with('profile')->get();
        //dd($users);
        return view('dashboard.admin.manage-translotors', ['users' => $users, 'user_count' => count($users)]);
    }


    public function addUser()
    {
        return view('dashboard.admin.add-user');
    }

    public function deleteUser(Request $request)
    {
        $userId = $request->id;

        $userDel = User::where('ucode', $userId)->first();

        $trName = $userDel->profile->first_name;
        $trEmail = $userDel->email;

        $user = User::where('ucode',$userId)->update(['status'=>0]);


        if($user){
            $data = [
                'userId' => $userId,
                'trName' => $trName,
                'trEmail' => $trEmail,
            ];

            return $data;
        }else{
            return "FAIL@";
        }
        //return response()->json($request->id, 200);
    }


    public function viewUser(Request $request)
    {
        $getUserCodefromURL = $request->route('id');
        $userDetails = User::where('ucode', $getUserCodefromURL)->first();
        return view('dashboard.admin.view-user', ['userDetails' => $userDetails]);
        //echo '<pre>';
        //print_r($userDetails);

    }

    public function evalTests(Request $request)
    {
        $getUserCodefromURL = $request->route('id');

        $userTests = Test::leftJoin('test_attempt', function($join) { 
            $join->on('test_attempt.test_id', '=', 'tests.id'); 
        })->where('test_attempt.ucode', '=', $getUserCodefromURL)->get(['test_attempt.id','test_attempt.ucode','test_attempt.lang_pair','test_attempt.test_score', 'test_attempt.test_result', 'tests.title', 'tests.source_language', 'tests.target_language', 'tests.test_type']);
        return view('dashboard.admin.showgiven-tests', ['userTests' => $userTests]);

    }

    public function updateContract(Request $request)
    {
        $getUserCodefromURL = $request->route('id');

        $languagePairRowExist = DB::table('language_pairs')
        ->where('ucode', $getUserCodefromURL)
        ->where('flag', 'language-pair')
        ->get();

        $translatorDetails= DB::table('profiles')
        ->where('ucode', $getUserCodefromURL)
        ->get();

        return view('dashboard.admin.updateContract', ['languagePairRowExist' => $languagePairRowExist, 'translatorDetails'=> $translatorDetails]);

    }


    public function updateContractLP(Request $request)
    {
        $userLPAction = $request->id;
        $userLPAction_breakup = explode("-XX-", $userLPAction);
        $ucode  = $userLPAction_breakup[0];
        $lpID   = $userLPAction_breakup[1];
        $lpstatus = $userLPAction_breakup[2];

        //update final/aggerate scores in translator's profile - LP wise
        $resultflag = DB::table('language_pairs')
        ->where('ucode', $ucode)
        ->where('id', $lpID)
        ->update([
            'status' => $lpstatus,
        ]);

        // $user = User::where('ucode',$userId)->update(['status'=>0]);

        if($resultflag){
            return "OK@";
        }else{
            return "FAIL@";
        }
        //return response()->json($request->id, 200);
    }


    public function sendStatusReport(Request $request)
    {
        $getUserCodefromURL = $request->id;
        $languagePairRowExist = DB::table('language_pairs')
        ->where('ucode', $getUserCodefromURL)
        ->where('flag', 'language-pair')
        ->get();


        //update final/aggerate scores in translator's profile - LP wise
        DB::table('language_pairs')
        ->where('ucode', $getUserCodefromURL)
        ->where('flag', 'language-pair')
        ->where('status', 1)
        ->update([
            'contract_send' => 1,
        ]);

        DB::table('profiles')
        ->where('ucode', $getUserCodefromURL)
        ->update([
            'saveBtnStatus' => 0,
            'finalStatus' => 'Contract Pending'
        ]);

        Session::put('finalStatus', 'Contract Pending');

        //send email to translator
        //Mail::to('newactivation@example.com')->send(new ProfileActivationMail());
    }

    public function evaluateTest(Request $request)
    {
        $userTestID = $request->route('id');
        $tempArr = explode("@@", $userTestID);
        $userCode = $tempArr[0];
        $testAttemptID = $tempArr[1];

        $userTestAttmpDetails = Test::leftJoin('test_attempt', function($join) { 
            $join->on('test_attempt.test_id', '=', 'tests.id'); 
        })->where('test_attempt.id', '=', $testAttemptID)->get(['test_attempt.*', 'tests.title', 'tests.source_language', 'tests.target_language', 'tests.description', 'tests.test_type']);

        return view('dashboard.admin.evaluateTest', ['userTestAttmpDetails' => $userTestAttmpDetails]);

    }

    public function submitEval(Request $request)
    {
        //return $request->all();
        //die;

        $UserFinalScoresArr = array();
        $UserScoresRatingArr = array();

        $UserFinalScoresArr['ucode']= $request->user_ucode;
        $UserFinalScoresArr['scores']['semantics']= $scoreInSemantics = $request->semantics;
        $UserFinalScoresArr['scores']['terminology']= $scoreInTerminology = $request->terminology;
        $UserFinalScoresArr['scores']['syntax']= $scoreInSyntax = $request->syntax;
        $UserFinalScoresArr['scores']['stylistic_quality']= $scoreInQuality = $request->stylistic_quality;
        $UserFinalScoresArr['scores']['stylistic_beauty']= $scoreInBeauty = $request->stylistic_beauty;

        //calculate Score/500 and Rating/5 for a given test -  test wise
        $totalScorebyEval = $scoreInSemantics+$scoreInTerminology+$scoreInSyntax+$scoreInQuality+$scoreInBeauty;
        $totalRatingbyEval = $totalScorebyEval/100;

        $UserScoresRatingArr['ucode']= $request->user_ucode;
        $UserScoresRatingArr['total_score']= $totalScorebyEval;
        $UserScoresRatingArr['total_rating']= $totalRatingbyEval;

        $arryToJsonAllScore = json_encode($UserFinalScoresArr, true);
        $arryToJsonTotalScore = json_encode($UserScoresRatingArr, true);

        //update scores in test attempt table - test wise
        DB::table('test_attempt')
        ->where('ucode', $request->user_ucode)
        ->where('id', $request->user_attempt_id)
        ->update([
            'test_score' => $arryToJsonAllScore,
            'test_result' => $arryToJsonTotalScore,
        ]);

        //update final/aggerate scores in translator's profile - LP wise
        DB::table('language_pairs')
        ->where('ucode', $request->user_ucode)
        ->where('name', $request->user_lang_pair)
        ->where('flag', 'language-pair')
        ->update([
            'final_score' => $totalScorebyEval,
            'final_rating' => $totalRatingbyEval,
        ]);



        //return $request->all();
        return response()->json(['message' => true], 200);

    }

    public function ModalWithLpDetails(Request $request)
    {

        $userTestID = $request->id;
        $tempArr = explode("XXX", $userTestID);
        $userCode = $tempArr[0];
        $lP_id = $tempArr[1];
        $lP_name = $tempArr[2];

        $languagePairRowExist = DB::table('language_pairs')
        ->where('ucode', $userCode)
        ->where('id', $lP_id)
        ->where('name', $lP_name)
        ->get();

        $finalRating        = $languagePairRowExist[0]->final_rating ?? 0;
        $contract_flag      = $languagePairRowExist[0]->contract_flag ?? 0;
        $contract_type      = $languagePairRowExist[0]->contract_type ?? 0;
        $proofreader        = $languagePairRowExist[0]->proofreader ?? 0;
        $fixed_rate         = $languagePairRowExist[0]->fixed_rate ?? 0;
        $quota_rate         = $languagePairRowExist[0]->quota_rate ?? 0;
        $translation_wc     = $languagePairRowExist[0]->translation_wc ?? 0;
        $proofreading_wc    = $languagePairRowExist[0]->proofreading_wc ?? 0;


        if($languagePairRowExist){
            return $finalRating.'X-X'.$contract_flag.'X-X'.$contract_type.'X-X'.$proofreader.'X-X'.$fixed_rate.'X-X'.$quota_rate.'X-X'.$translation_wc.'X-X'.$proofreading_wc.'X-X'.$userCode.'X-X'.$lP_id.'X-X'.$lP_name;
        }else{
            return "FAIL@@";
        }

    }

    public function saveModalDetails(Request $request)
    {

        //echo '<pre>';
        //return $request->all();

        $userCode = $request->dyn_ucode;
        $lP_id = $request->dyn_lpid;
        $lP_name = $request->dyn_lpname;

        if(isset($request->proofreader)){
            $checkVal = 1;
        }else{
            $checkVal = 0;
        }

        //check if we have existing values
        $languagePairVals = DB::table('language_pairs')
        ->where('ucode', $userCode)
        ->where('id', $lP_id)
        ->where('name', $lP_name)
        ->get();
        $tempContractFlag = $languagePairVals[0]->contract_flag;
        $tempContractType = $languagePairVals[0]->contract_type;
        //----------------------------------


        if(!isset($request->contract_flag)){
            $finalContractFlag = $tempContractFlag;
        }else{
            $finalContractFlag = $request->contract_flag;
        }

        if(!isset($request->contract_type)){
            $finalContractType = $tempContractType;
        }else{
            $finalContractType = $request->contract_type;
        }

        $allGood = DB::table('language_pairs')
        ->where('ucode', $userCode)
        ->where('id', $lP_id)
        ->where('name', $lP_name)
        ->update([
            'contract_flag'     => $finalContractFlag,
            'contract_type'     => $finalContractType,
            'final_rating'      => $request->edit_rating,
            'proofreader'       => $checkVal,
            'fixed_rate'        => $request->fixed_rate,
            'quota_rate'        => $request->quota_rate,
            'translation_wc'    => $request->translation_wc,
            'proofreading_wc'   => $request->proofreading_wc,
        ]);

        if($allGood){
            return 'OKK';
        }else{
            return "FAIL";
        }

    }

    public function getAdminPage()
    {
        $users = User::all();
        return view('admin', ['users' => $users]);
    }

    public function getGenerateArticle()
    {
        return response('Article generated!', 200);
    }

    public function addNewUser(Request $request)
    {
        //$ranToken = $this->getToken();
        //$data = request()->validate([
          //  'email' => 'required|string|email|max:255|unique:users'
        //]);

        Mail::to($request->email)->send(new MailActivationlink());
        return back()->with('success', 'Activation Link Sent.');

    }


    public function manageTests()
    {
        $tests = Test::get();
        return view('dashboard.admin.manage-test', ['tests' => $tests, 'test_count' => count($tests)]);
    }

    public function addTest(Request $request)
    {
        return view('dashboard.admin.add-test');

    }


    public function editTest(Request $request)
    {
        $getTestId = $request->route('id');
        $testDetails = Test::where('id', $getTestId)->first();
        return view('dashboard.admin.edit-test', ['testDetails' => $testDetails]);
    }


    public function addNewTest(Request $request)
    {
        $data = request()->validate([
            'title' => 'required|string|max:255',
            'description' => 'required|string',
            'source_language' => 'required|string',
            'target_language' => 'required|string',
            'instructions' => 'required|string',
            'word_count' => 'required|string',
            'test_type' => 'required',
            'test_duration' => 'required'
        ]); 

        $test = new Test();
        $test->title = $request->title;
        $test->description = $request->description;
        $test->source_language = $request->source_language;
        $test->target_language = $request->target_language;
        $test->instructions = $request->instructions;
        $test->word_count = $request->word_count;
        $test->test_type = $request->test_type;
        $test->test_duration = $request->test_duration;
        $test->save();

        if($test->save()){
            return redirect()->route('dashboard.admin.manageTests')->with('success', 'Test Submitted.');
        }else{
            return back()->with('error', 'Error: Please try again');
        }

    }

    public function updateTest(Request $request)
    {
        $getTestid = $request->route('id');
        $data = request()->validate([
            'title' => 'required|string|max:255',
            'description' => 'required|string',
            'source_language' => 'required|string',
            'target_language' => 'required|string',
            'instructions' => 'required|string',
            'word_count' => 'required|string',
            'test_duration' => 'required'
        ]); 

        $updateTest = Test::find($getTestid)->update($data);

        if($updateTest){
            return redirect()->route('dashboard.admin.manageTests')->with('success', 'Test Updated.');
        }else{
            return back()->with('error', 'Error: Please try again');
        }
    }

    public function deleteTest(Request $request)
    {
        $testId = $request->id;
        $test = Test::where('id',$testId)->update(['status'=>0]);

        if($test){
            return $testId;
        }else{
            return "FAIL@";
        }
        //return response()->json($request->id, 200);
    }

    public function addNewClient(){
        return view('dashboard.admin.add-client');
    }

    public function manageClients(){
        return view('dashboard.admin.manage-client');
    }

    public function addNewClientPost(Request $request){
        //echo '<pre>';
        //return $request->all();
        $Client_name = $request->Client_name;
        $Client_Team = $request->Client_Team;
        $Client_Email = $request->Client_Email;
        $Client_Phone = $request->Client_Phone;
        $Requester_Name = $request->Requester_Name;
    }


    public function manageAssignments(){
        $getAllAssigments = DB::table('assignments')->orderBy('created_at', 'DESC')->get();
        return view('dashboard.admin.manage-assignments', ['getAllAssigments' => $getAllAssigments]);
    }

    public function addNewAssignment(){
        $getAllClients = DB::table('clients')->get();
        return view('dashboard.admin.add-assignment', ['getAllClients' => $getAllClients]);
    }

    public function addNewAssignmentPost(Request $request){
        dd($request->all());

        $Client_name = $request->client_name;
        $client_team = $request->client_team;
        $Requester_Name = $request->Requester_Name;
        $word_count = $request->word_count;
        $page_count = $request->page_count;
        $client_instructions = $request->client_instructions;

        $assignment_name = $request->assignment_name;
        $assignment_duration = $request->assignment_duration;
        $source_language = $request->source_language;
        $target_language = $request->target_language;

        $assignmentUniqueCode = $Client_name."l".time();

        $assigment_save = DB::table('assignments')
        ->insert([
            'acode' => $assignmentUniqueCode,
            'client_id' => $Client_name,
            'client_team_id' => $client_team,
            'client_requester_name' =>  $Requester_Name,
            'assignment_name' => $assignment_name,
            'source_lang' =>  $source_language,
            'target_lang' => $target_language,
            'word_count' =>  $word_count,
            'page_count' =>  $page_count,
            'comment' => $client_instructions,
            'duration' => $assignment_duration,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        if($assigment_save){
            return redirect()->route('dashboard.admin.manageAssignments')->with('success', 'assignment Created.');
        }else{
            return back()->with('error', 'Error: Please try again');
        }


    }

    public function viewAssigment(Request $request){
        $getAsgId = $request->route('id');
        $assigment_details = DB::table('assignments')
        ->where('acode', $getAsgId)->get();
        return view('dashboard.admin.view-assignment', ['assigment_details' => $assigment_details]);
    }




    public function getToken($length=8){    
        $token = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet.= "0123456789";

        for($i=0;$i<$length;$i++){
            $token .= $codeAlphabet[mt_rand(0,strlen($codeAlphabet)-1)];
        }
        return $token;
    }

    public function postAdminAssignRoles(Request $request)
    {   
        $user = User::where('email', $request['email'])->first();
        $user->roles()->detach();
        if ($request['role_user']) {
            $user->roles()->attach(Role::where('name', 'User')->first());
        }
        if ($request['role_author']) {
            $user->roles()->attach(Role::where('name', 'Author')->first());
        }
        if ($request['role_admin']) {
            $user->roles()->attach(Role::where('name', 'Admin')->first());
        }
        return back();
    }
}