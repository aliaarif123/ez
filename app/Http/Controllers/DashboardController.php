<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
 
class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
       
        switch (Auth::user()->role) {
            case 'Admin':
                return view('dashboard/admin/index');
            break;

            case 'Translator':
                return view('dashboard/translator/index');
            break;

            case 'Quality Analyst':
                return view('dashboard/quality-analyst/index');
            break;

            case 'Proof Reader':
                return view('dashboard/proof-reader/index');
            break;

            case 'Operations':
                return view('dashboard/operations/index');                
            break;

            case 'Client':
                return view('dashboard/client/index');                           
            break;
            
            default:
                return view('dashboard/translator/index');    
            break;
        }
        
    }
}
