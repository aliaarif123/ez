<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
    * Handle an incoming request.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \Closure  $next
    * @param  string|null  $guard
    * @return mixed
    */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            switch (Auth::user()->role) {
                case 'Admin':
                    return redirect('/dashboard/admin');
                break;
                
                case 'Translator':
                    return redirect('/dashboard/translator');
                break;
                
                case 'Quality Analyst':
                    return redirect('/dashboard/quality-analyst');
                break; 
                
                // case 'Proof Reader':
                    // return redirect('/dashboard/proof-reader');
                    // break; 
                    
                    case 'Operations':
                        return redirect('/dashboard/operations');
                    break;     
                    
                    default:
                    return redirect('/dashboard/client');
                break;
            }
            
            $dashboard = url('/dashboard').'/'.strtolower(Auth::user()->role);
            
            
            return redirect($dashboard);
            
        }
        
        return $next($request);
    }
}
