<?php

namespace App\Http\Middleware;

use Closure;
use Request;
class CheckRole
{
    /**
    * Handle an incoming request.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \Closure  $next
    * @return mixed
    */
    public function handle($request, Closure $next)
    {
        
        
        if($request->user() === null) {
            return redirect()->route('permission');
            // return response('1You have not sufficient permission to access this route', 401);
        }
        
        $actions = $request->route()->getAction();
        $roles = isset($actions['roles']) ? $actions['roles'] : null;
        
        if($request->user()->hasAnyRole($roles) || !$roles ) {
            return $next($request);    
        }
        return redirect()->route('permission');
        //return response('2You have not sufficient permission to access this route', 401);
        
        
    }
}
