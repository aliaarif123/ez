<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ProfileActivationMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //return $this->view('view.name');
        return $this->from('mail@example.com', 'Mailtrap')
            ->subject('ArabEasy Contract Verification')
            ->markdown('mails.profile_activation')
            ->with([
                'name' => 'User',
                'link' => 'http://18.204.157.159/'
            ]);
    }
}
