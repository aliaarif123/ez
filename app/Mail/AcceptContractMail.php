<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AcceptContractMail extends Mailable
{
    use Queueable, SerializesModels;


    public $user;
    public $lp_data;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $lp_data)
    {
        $this->user = $user;
        $this->lp_data = $lp_data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.accept-contract-mail')->with(['user' => $this->user, 'lp_data' => $this->lp_data]);
    }
}