<?php

use Illuminate\Database\Seeder;
use App\Role;


class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_admin = new Role;
        $role_admin->name = 'Admin';
        $role_admin->description = 'Its a Super Admin';
        $role_admin->save();

        $role_translator = new Role;
        $role_translator->name = 'Translator';
        $role_translator->description = 'Its a Translator';
        $role_translator->save();

        $role_operations = new Role;
        $role_operations->name = 'Operations';
        $role_operations->description = 'Its an Operation User';
        $role_operations->save();

        // $role_proof_reader = new Role;
        // $role_proof_reader->name = 'Proof Reader';
        // $role_proof_reader->description = 'Its a Proof Reader';
        // $role_proof_reader->save();

        $role_quality_analyst = new Role;
        $role_quality_analyst->name = 'Quality Analyst';
        $role_quality_analyst->description = 'Its a Quality Analyst';
        $role_quality_analyst->save();


        $role_client = new Role;
        $role_client->name = 'Client';
        $role_client->description = 'Its a normal client';
        $role_client->save();
    }
}
