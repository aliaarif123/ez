<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$role_admin = Role::where('name', 'Admin')->first();
    	$admin = new User;
    	$admin->ucode = 'ucodel'.mt_rand(1234567891, 9999999999);
    	$admin->role = 'Admin';
    	$admin->email = 'admin@example.com';
    	$admin->password = bcrypt('password');
    	$admin->email_verified_at = now();
    	$admin->save();
    	$admin->roles()->attach($role_admin);
    	$admin->profile()->create([
    		'user_id' => $admin->id,
    		'ucode' => $admin->ucode,
    		//'first_name' => $admin->role,
            //'last_name' => 'Dummy Last Name',   		
        ]);

    	$role_translator = Role::where('name', 'Translator')->first();
    	$translator = new User;
    	$translator->ucode = 'ucodel'.mt_rand(1234567891, 9999999999);
    	$translator->role = 'Translator';
    	$translator->email = 'translator@example.com';
    	$translator->password = bcrypt('password');
    	$translator->email_verified_at = now();
    	$translator->save();
    	$translator->roles()->attach($role_translator);
    	$translator->profile()->create([
    		'user_id' => $translator->id,
    		'ucode' => $translator->ucode,
    		//'first_name' => $translator->role,
            //'last_name' => 'Dummy Last Name',	
			// 'lang_pairs' => [
			// 	'ar_en' => true,
			// 	'en_ar' => true
			// ],
			// 'tests_ratings' => [
			// 	'testID' => [
			// 		'score' => mt_rand(300, 1000),
			// 		'rating' => mt_rand(1, 5),
			// 		'status' => true
			// 	],
			// 	'testID' => [
			// 		'score' => mt_rand(300, 1000),
			// 		'rating' => mt_rand(1, 5),
			// 		'status' => true
			// 	],

			// ],
			// 'assignment_ratings' => [
			// 	'assignmentID' => [
			// 		'score' => mt_rand(300, 1000),
			// 		'rating' => mt_rand(1, 5)
			// 	],
			// 	'assignmentID' => [
			// 		'score' => mt_rand(300, 1000),
			// 		'rating' => mt_rand(1, 5)
			// 	],

			// ],


        ]);

		// $translator->profile()->create([
		// 	'ucode' => $translator->ucode,			
		// ]);

		// $translator->lang_pairs()->create([
		// 	'user_id' => $translator->id,
		// 	'ucode' => $translator->ucode,
		// 	'name' => 'Arabic',		
		// ]);

		// $translator->lang_pairs()->create([
		// 	'user_id' => $translator->id,
		// 	'ucode' => $translator->ucode,
		// 	'name' => 'English',		
		// ]);

		// $translator->lang_pairs()->create([
		// 	'user_id' => $translator->id,
		// 	'ucode' => $translator->ucode,
		// 	'name' => 'ArabicEnglish',
		// 	'flag' => 'laguage-pair',				
		// ]);

		// $translator->lang_pairs()->create([
		// 	'user_id' => $translator->id,
		// 	'ucode' => $translator->ucode,
		// 	'name' => 'EnglishArabic',
		// 	'flag' => 'laguage-pair',				
		// ]);

    	

    	$role_operations = Role::where('name', 'Operations')->first();
    	$operations = new User;
    	$operations->ucode = 'ucodel'.mt_rand(1234567891, 9999999999);
    	$operations->role = 'Operations';
    	$operations->email = 'operations@example.com';
    	$operations->password = bcrypt('password');
    	$operations->email_verified_at = now();
    	$operations->save();
    	$operations->roles()->attach($role_operations);
    	$operations->profile()->create([
    		'user_id' => $operations->id,
    		'ucode' => $operations->ucode,
    		//'first_name' => $operations->role,
            //'last_name' => 'Dummy Last Name',   		
        ]);

    	$role_proof_reader = Role::where('name', 'Proof Reader')->first();
    	$proof_reader = new User;
    	$proof_reader->ucode = 'ucodel'.mt_rand(1234567891, 9999999999);
    	$proof_reader->role = 'Proof Reader';
    	$proof_reader->email = 'proof_reader@example.com';
    	$proof_reader->password = bcrypt('password');
    	$proof_reader->email_verified_at = now();
    	$proof_reader->save();
    	$proof_reader->roles()->attach($role_proof_reader);
    	$proof_reader->profile()->create([
    		'user_id' => $proof_reader->id,
    		'ucode' => $proof_reader->ucode,
    		//'first_name' => $proof_reader->role,
            //'last_name' => 'Dummy Last Name',   		
        ]);
    	


    	$role_quality_analyst = Role::where('name', 'Quality Analyst')->first();
    	$quality_analyst = new User;
    	$quality_analyst->ucode = 'ucodel'.mt_rand(1234567891, 9999999999);
    	$quality_analyst->role = 'Quality Analyst';
    	$quality_analyst->email = 'quality_analyst@example.com';
    	$quality_analyst->password = bcrypt('password');
    	$quality_analyst->email_verified_at = now();
    	$quality_analyst->save();
    	$quality_analyst->roles()->attach($role_quality_analyst);
    	$quality_analyst->profile()->create([
    		'user_id' => $quality_analyst->id,
    		'ucode' => $quality_analyst->ucode,
    		//'first_name' => $quality_analyst->role,	
            //'last_name' => 'Dummy Last Name',   	
        ]);

    	$role_client = Role::where('name', 'Client')->first();
    	$client = new User;
    	$client->ucode = 'ucodel'.mt_rand(1234567891, 9999999999);
    	$client->role = 'Client';
    	$client->email = 'client@example.com';
    	$client->password = bcrypt('password');
    	$client->email_verified_at = now();
    	$client->save();
    	$client->roles()->attach($role_client);
    	$client->profile()->create([
    		'user_id' => $client->id,
    		'ucode' => $client->ucode,
    		//'first_name' => $client->role,
            //'last_name' => 'Dummy Last Name',   		
        ]);

    	
    	//for($i = 11; $i <= 20; $i++){
        //  $role_translator = Role::where('name', 'Translator')->first();
        //  $translator = new User;
        //  $translator->ucode = 'ucodel'.mt_rand(1234567891, 9999999999);
        //  $translator->role = 'Translator';
        //  $translator->email = 'translator'.$i.'@example.com';
        //  $translator->password = bcrypt('password');
        //  $translator->email_verified_at = now();
        //  $translator->save();
        //  $translator->roles()->attach($role_translator);
        //  $translator->profile()->create([
        //     'user_id' => $translator->id,
        //     'ucode' => $translator->ucode,
        //     'first_name' => $translator->role.' '.$i,
        //     'last_name' => 'Dummy Last Name',   		
        // ]);

			// $translator->lang_pairs()->create([
			// 	'user_id' => $translator->id,
			// 	'ucode' => $translator->ucode,
			// 	'name' => 'Arabic',		
			// ]);
			// $translator->lang_pairs()->create([
			// 	'user_id' => $translator->id,
			// 	'ucode' => $translator->ucode,
			// 	'name' => 'English',		
			// ]);

			// $translator->lang_pairs()->create([
			// 	'user_id' => $translator->id,
			// 	'ucode' => $translator->ucode,
			// 	'name' => 'ArabicEnglish',
			// 	'flag' => 'laguage-pair',				
			// ]);

			// $translator->lang_pairs()->create([
			// 	'user_id' => $translator->id,
			// 	'ucode' => $translator->ucode,
			// 	'name' => 'EnglishArabic',
			// 	'flag' => 'laguage-pair',				
			// ]);



      // }

    }
}
