<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssignmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assignments', function (Blueprint $table) {
            $table->bigIncrements('id');
            /* assignments basics start */
            $table->string('acode')->default('acodel'.time());
            $table->string('assignment_name')->nullable();
            $table->string('assignment_file_type')->nullable();
            $table->string('assignment_path')->nullable();
            /* assignments basics end */

            /* client start */
            $table->string('client_name')->nullable();
            $table->string('req_name')->nullable();
            $table->string('charge_code')->nullable();
            /* client end */

            /* assignments details start */
            $table->string('service_type')->nullable();
            $table->string('source_lang')->nullable();
            $table->string('target_lang')->nullable();
            /* assignments details end */

            /* assignments delevery start */
            $table->string('time_zone')->nullable();
            $table->string('deadline')->nullable();
            $table->text('client_instructions')->nullable();
            /* assignments delevery end */

            /* assignments delevery plan start */   
            $table->integer('word_count')->nullable();
            $table->integer('page_count')->nullable();
            $table->integer('word_count_adj')->nullable();
            $table->integer('page_count_adj')->nullable();
            /* assignments delevery plan end */

            /* assignments time slots start */   
            $table->datetime('translator_deadline')->nullable();
            $table->datetime('proof_reader_deadline')->nullable();
            $table->datetime('vga_deadline')->nullable();
            /* assignments time slots start */   

            $table->string('created_by')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assignments');
    }
}
