<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="author" content="ThemeSelect">
	@include('common.csrf')
	<?php
	//$title =  'Local Server';
	$remoteAddress = $_SERVER['HTTP_HOST'];

	if($remoteAddress == '54.224.245.146'){
		$title = 'Dev Server'; // Dev Server
	}else if($remoteAddress == '18.232.161.238'){
		$title =  'Test Server'; // Test Server
	}else{
		$title =  'Local Server'; // Local Vertual Server
	}
	?>
	<title>{{ $title }}</title>
	<link rel="apple-touch-icon" href="{{ asset('images/favicon/apple-touch-icon-152x152.png') }}">
	<link rel="shortcut icon" type="image/x-icon" href="{{ asset('images/favicon/favicon-32x32.png') }}">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="{{ asset('vendors/vendors.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('vendors/animate-css/animate.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/themes/vertical-menu-nav-dark-template/materialize.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/themes/vertical-menu-nav-dark-template/style.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/pages/dashboard-modern.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/custom/custom.css') }}">
	<style type="text/css">
	</style>
	@stack('css')

	@push('css')
	<style type="text/css">
		.modalA {
			.max-height: auto !important;
		}
	</style>
	@endpush
</head>
<body  class="vertical-layout page-header-light vertical-menu-collapsible vertical-menu-nav-dark 2-columns  "  data-open="click" data-menu="vertical-menu-nav-dark" data-col="2-columns">
	@include('common.header')
	@include('common.leftSideNav')
	<div id="main">
		<div class="row">
			<div class="breadcrumbs-inline pt-3 pb-1" id="breadcrumbs-wrapper">
				<div class="container">
					<div class="row">
						@include('common.breadcrumb')
						@if(Route::currentRouteName() == 'dashboard.translator.myassignments.liveFeed')
						<div class="col s2 m2 l2"><a class="btn btn-floating dropdown-settings waves-effect waves-light breadcrumbs-btn right" href="#!" data-target="dropdown1"><i class="material-icons">expand_more</i><i class="material-icons right">arrow_drop_down</i></a>
							<ul class="dropdown-content" id="dropdown1" tabindex="0">
								<li tabindex="0"><a class="grey-text text-darken-2" href="user-profile-page.html">Profile<span class="new badge red">2</span></a></li>
								<li tabindex="0"><a class="grey-text text-darken-2" href="app-contacts.html">Contacts</a></li>
								<li tabindex="0"><a class="grey-text text-darken-2" href="page-faq.html">FAQ</a></li>
								<li class="divider" tabindex="-1"></li>
								<li tabindex="0"><a class="grey-text text-darken-2" href="user-login.html">Logout</a></li>
							</ul>
						</div>
						@endif
					</div>
				</div>
			</div>
			<div class="content-wrapper-before blue-grey lighten-5"></div>
			<div class="col s12">
				@yield('content')
				@include('common.rightSideNav')


				<!-- Start Makeing Conditions for Popup inthe project -->
				<?php
				$role = Auth::user()->role;
				$status = Auth::user()->profile->finalStatus;
				$introModelDisplayState = Auth::user()->profile->introModelDisplayState
				?>
				<!-- @if($status == 'Profile Incomplete' && $introModelDisplayState == 1)
				@include('common.status.profile-incomelete')
				@elseif($status == 'Test Pending' && $introModelDisplayState == 1)
				@include('common.status.test-pending')
				@elseif($status == 'Pending Contract' && $introModelDisplayState == 1)
				@include('common.status.contract-pending')
				@elseif($status == 'Blacklisted' && $introModelDisplayState == 1)
				@include('common.status.blacklisted')
				@endif -->
				<!-- End Makeing Conditions for Popup inthe project -->
			</div>
		</div>
	</div>
	<!-- END: Page Main-->
	@include('common.footer')
	<script src="{{ asset('js/vendors.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('js/plugins.js') }}" type="text/javascript"></script>
	<!-- <script src="{{ asset('js/custom/custom-script.js') }}" type="text/javascript"></script> -->
	<script src="{{ asset('js/scripts/customizer.js') }}" type="text/javascript"></script>
	<script src="{{ asset('js/scripts/intro.js') }}" type="text/javascript"></script>
	<script src="{{ asset('js/axios.min.js') }}"></script>
	<script type="text/javascript">
		let remoteAddress = '{{ $_SERVER['HTTP_HOST']  }}';

		if(remoteAddress == '54.224.245.146'){
			window.bURL = 'http://54.224.245.146/'; // Dev Server
		}else if(remoteAddress == '18.232.161.238'){
			window.bURL = 'http://18.232.161.238/'; // Test Server
		}else{
			window.bURL = 'http://dev.project/'; // Local Server
		}
		//window.bURL = '{{ $_SERVER['REMOTE_ADDR'] == "127.0.0.1" ? "http://dev.project/" : $_SERVER['HTTP_HOST'] }}'; // Dev Server
		//window.bURL = '{{ $_SERVER['REMOTE_ADDR'] == "127.0.0.1" ? "http://dev.project/" : "http://54.82.152.136/" }}'; // Test Server

	</script>

	@stack('js')
	@push('js')
	<script type="text/javascript">
		// document.getElementById("intro-close").addEventListener("click", function(){
		// 	//$('#intro').remove();
		// 	alert(4);
		// 	axios.get(bURL+'intro-model-display-state')
		// 	.then(function (res) {
		// 		console.log(res)
		// 	})
		// });
		$(() => {
			$('#userFinalStatus').html(`Status : {{ Auth::user()->profile->finalStatus }}`);

			// $('#userFinalStatus').html(`Status : `+localStorage.getItem('status') ? localStorage.getItem('status') : 'Profile Incomplete');

			// if(localStorage.getItem('status')){
			// 	$('#userFinalStatus').html('Status : '+localStorage.getItem('status'));

			// }else{
			// 	localStorage.setItem('status', '{{ Auth::user()->profile->finalStatus }}');
			// }
			


			// window.status = `{{ Auth::user()->profile->finalStatus }}`;
			// sessionStorage.setItem('status', window.status);

			var token = document.head.querySelector('meta[name="csrf-token"]');

			$('#intro-close').on('click', (){
				axios.get('http://dev.project/intro-model-display-state')
				.then((res) => {
					console.log(res)
				})
			});
		});
	</script>
	@endpush
</body>
</html>