<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="ThemeSelect">
    @include('common.csrf')
    <?php
    //$title =  'Local Server';
    $remoteAddress = $_SERVER['HTTP_HOST'];

    if($remoteAddress == '54.224.245.146'){
        $title = 'Dev Server'; // Dev Server
    }else if($remoteAddress == '18.232.161.238'){
        $title =  'Test Server'; // Test Server
    }else{
        $title =  'Local Server'; // Local Vertual Server
    }
    ?>
    <title>{{ $title }}</title>
    <link rel="apple-touch-icon" href="{{ asset('images/favicon/apple-touch-icon-152x152.png') }}">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('images/favicon/favicon-32x32.png') }}">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- BEGIN: VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('vendors/vendors.min.css') }}">
    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/themes/vertical-menu-nav-dark-template/materialize.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/themes/vertical-menu-nav-dark-template/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/pages/page-500.css') }}">
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/custom/custom.css') }}">
    <!-- END: Custom CSS-->
    @stack('css')
</head>
<!-- END: Head-->
<body class="vertical-layout page-header-light vertical-menu-collapsible vertical-menu-nav-dark 1-column   blank-page blank-page" data-open="click" data-menu="vertical-menu-nav-dark" data-col="1-column">
    @yield('content')

    <!-- BEGIN VENDOR JS-->
    <script src="{{ asset('js/vendors.min.js') }}" type="text/javascript"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN THEME  JS-->
    <script src="{{ asset('js/plugins.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/custom/custom-script.js') }}" type="text/javascript"></script>
    <!-- END THEME  JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <!-- END PAGE LEVEL JS-->

    <script type="text/javascript">
        let remoteAddress = '{{ $_SERVER['HTTP_HOST']  }}';

        if(remoteAddress == '54.224.245.146'){
            window.bURL = 'http://54.224.245.146/'; // Dev Server
        }else if(remoteAddress == '18.232.161.238'){
            window.bURL = 'http://18.232.161.238/'; // Test Server
        }else{
            window.bURL = 'http://dev.project/'; // Local Server
        }
        //window.bURL = '{{ $_SERVER['REMOTE_ADDR'] == "127.0.0.1" ? "http://dev.project/" : $_SERVER['HTTP_HOST'] }}'; // Dev Server
        //window.bURL = '{{ $_SERVER['REMOTE_ADDR'] == "127.0.0.1" ? "http://dev.project/" : "http://54.82.152.136/" }}'; // Test Server

    </script>
    
    @stack('js')
</body>
</html>