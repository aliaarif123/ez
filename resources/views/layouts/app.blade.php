<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="author" content="ThemeSelect">
  @include('common.csrf')
  <?php
  //$title =  'Local Server';
  $remoteAddress = $_SERVER['HTTP_HOST'];

  if($remoteAddress == '54.224.245.146'){
    $title = 'Dev Server'; // Dev Server
  }else if($remoteAddress == '18.232.161.238'){
    $title =  'Test Server'; // Test Server
  }else{
    $title =  'Local Server'; // Local Vertual Server
  }
  ?>
  <title>{{ $title }}</title>
  <link rel="apple-touch-icon" href="{{ asset('images/favicon/apple-touch-icon-152x152.png') }}">
  <link rel="shortcut icon" type="image/x-icon" href="{{ asset('images/favicon/favicon-32x32.png') }}">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <!-- BEGIN: VENDOR CSS-->
  <link rel="stylesheet" type="text/css" href="{{ asset('vendors/vendors.min.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('vendors/animate-css/animate.css') }}">
  <!-- <link rel="stylesheet" type="text/css" href="{{ asset('vendors/chartist-js/chartist.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('vendors/chartist-js/chartist-plugin-tooltip.css') }}"> -->
    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/themes/vertical-menu-nav-dark-template/materialize.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/themes/vertical-menu-nav-dark-template/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/pages/dashboard-modern.css') }}">
    <!-- <link rel="stylesheet" type="text/css" href="{{ asset('css/pages/intro.css') }}"> -->
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/custom/custom.css') }}">
    <!-- END: Custom CSS-->
    <style type="text/css">

    </style>
    @stack('css')
  </head>
  <!-- END: Head-->
  <body class="vertical-layout page-header-light vertical-menu-collapsible vertical-menu-nav-dark 2-columns  " data-open="click" data-menu="vertical-menu-nav-dark" data-col="2-columns">


    @include('common.header')

    @include('common.leftSideNav')

    <!-- BEGIN: Page Main-->
    <div id="main">
      <div class="row">
        <div class="content-wrapper-before blue-grey lighten-5"></div>
        <div class="col s12">
          <div class="container">
           @yield('content')
         </div>
       </div>
     </div>
   </div>
   <!-- END: Page Main-->
   @include('common.footer')
   <script src="{{ asset('js/vendors.min.js') }}" type="text/javascript"></script>
   <!-- BEGIN THEME  JS-->
   <script src="{{ asset('js/plugins.js') }}" type="text/javascript"></script>
   <!-- <script src="{{ asset('js/custom/custom-script.js') }}" type="text/javascript"></script> -->
   <script src="{{ asset('js/scripts/customizer.js') }}" type="text/javascript"></script>
   <!-- END THEME  JS-->
   <!-- BEGIN PAGE LEVEL JS-->
   <!-- <script src="{{ asset('js/scripts/dashboard-modern.js') }}" type="text/javascript"></script> -->
   <script src="{{ asset('js/scripts/intro.js') }}" type="text/javascript"></script>
   <!-- END PAGE LEVEL JS-->
   <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
   <script type="text/javascript">
    //window.bURL = '{{ $_SERVER['REMOTE_ADDR'] == "127.0.0.1" ? "http://dev.project/" : "http://18.204.157.159/" }}';
  </script>

  <script type="text/javascript">
    let remoteAddress = '{{ $_SERVER['HTTP_HOST']  }}';

    if(remoteAddress == '54.224.245.146'){
      window.bURL = 'http://54.224.245.146/'; // Dev Server
    }else if(remoteAddress == '18.232.161.238'){
      window.bURL = 'http://18.232.161.238/'; // Test Server
    }else{
      window.bURL = 'http://dev.project/'; // Local Server
    }
    //window.bURL = '{{ $_SERVER['REMOTE_ADDR'] == "127.0.0.1" ? "http://dev.project/" : $_SERVER['HTTP_HOST'] }}'; // Dev Server
    //window.bURL = '{{ $_SERVER['REMOTE_ADDR'] == "127.0.0.1" ? "http://dev.project/" : "http://54.82.152.136/" }}'; // Test Server

  </script>
  @stack('js')

</body>
</html>