@extends('layouts.guest')
@section('content')

<form  action="{{ route('register') }}"  class="login-form" method="post">
  @csrf
  
  <div class="row">
    <div class="input-field col s12">
      <h5 class="ml-4">Register Panel</h5>
    </div>
  </div>
      <!-- <div class="row margin">
        <div class="input-field col s12">
          <i class="material-icons prefix pt-2">person_outline</i>

          <input id="username" type="text" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}" required autocomplete="username" autofocus>
            @error('username')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
          <label for="username" class="center-align">Username</label>
        </div>
      </div> -->
      <div class="row margin">
        <div class="input-field col s12">
          <i class="material-icons prefix pt-2">person_outline</i>

          <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
          @error('email')
          <span class="invalid-feedback red-text text-darken-2" role="alert">
            <strong>{{ $message }}</strong>
          </span>
          @enderror
          <label for="email" class="center-align">Email</label>
        </div>
      </div>
      <div class="row margin">
        <div class="input-field col s12">
          <i class="material-icons prefix pt-2">lock_outline</i>
          <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
          @error('password')
          <span class="invalid-feedback red-text text-darken-2" role="alert">
            <strong>{{ $message == 'The password format is invalid.' ? 'Password must include at least one uppercase, one lowercase, numeric and special symbol, with minimum length of 8.' : $message }}</strong>
          </span>
          @enderror
          <label for="password">Password</label>
        </div>
      </div>

      <div class="row margin">
        <div class="input-field col s12">
          <i class="material-icons prefix pt-2">lock_outline</i>
          <input id="password-confirm" type="password" class="form-control"  name="password_confirmation" required autocomplete="new-password">
          <label for="password-confirm">Confirm Password</label>
        </div>
      </div>

      
      <div class="row">
        <div class="input-field col s12">
          <button type="submit" class="btn waves-effect waves-light border-round gradient-45deg-purple-deep-orange col s12">Register</button>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s12 m12 l12">
          <p class="margin center-align medium-small">
            <a href="{{ route('login') }}">Already have an account ?</a>
          </p>
        </div>
      </div>
    </form>
    
    
    @endsection

    @push('css')

    @endpush

    @push('js')

    @endpush