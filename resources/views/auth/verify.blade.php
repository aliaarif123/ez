@extends('layouts.user')

@section('content')

<div class="row">
    <div class="col s12 m6 l6">
      <div class="card gradient-45deg-light-blue-cyan gradient-shadow">
        <div class="card-content white-text">
          <span class="card-title">Verify Your Email Address</span>
          <p>
            @if (session('resent'))
            <div class="alert alert-success" role="alert">
                {{ __('A fresh verification link has been sent to your email address.') }}
            </div>
            @endif

            {{ __('Before proceeding, please check your email for a verification link.') }}
            {{ __('If you did not receive the email') }},

        </p>
    </div>
    <div class="card-action">
      <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
        @csrf
        <button type="submit" class="waves-effect waves-light btn gradient-45deg-red-pink">{{ __('click here to request another') }}</button>.
    </form>


</div>
</div>
</div>
</div>




@endsection
