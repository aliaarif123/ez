@extends('layouts.guest')
@section('content')

<form  action="{{ route('login') }}"  class="login-form" method="post">
  @csrf

  <div class="row">
    <div class="input-field col s12">
      <h5 class="ml-4">Login Panel</h5>
    </div>
    <div class="input-field col s12">
      @if (session('status'))
      <div class="alert alert-success">
        {{ session('status') }}
      </div>
      @endif
      @if (session('warning'))
      <div class="alert alert-warning">
        {{ session('warning') }}
      </div>
      @endif
    </div>

  </div>
  <div class="row margin">
    <div class="input-field col s12">
      <i class="material-icons prefix pt-2">person_outline</i>

      <input id="email" type="text" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

      @error('email')
      <span class="invalid-feedback red-text text-darken-2" role="alert">
        <strong>{{ $message }}</strong>
      </span>
      @enderror
      <label for="email" class="center-align">Email</label>

    </div>
  </div>


  <div class="row margin">
    <div class="input-field col s12">
      <i class="material-icons prefix pt-2">lock_outline</i>

      <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

      @error('password')
      <span class="invalid-feedback red-text text-darken-2" role="alert">
        <strong>{{ $message }}</strong>
      </span>
      @enderror
      <label for="password">Password</label>
    </div>
  </div>
  <div class="row">
    <div class="col s12 m12 l12 ml-2 mt-1">
      <p>
        <label>

          <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

          <span>Remember Me</span>
        </label>
      </p>
    </div>
  </div>
  <div class="row">
    <div class="input-field col s12">
      <button type="submit" class="btn waves-effect waves-light border-round gradient-45deg-purple-deep-orange col s12">Login</button>
    </div>
  </div>
  <div class="row">
    <div class="input-field col s6 m6 l6">
      <p class="margin medium-small"><a href="{{ route('register') }}">Register Now!</a></p>
    </div>
    <div class="input-field col s6 m6 l6">
      <p class="margin right-align medium-small"><a href="{{ route('password.request') }}">Forgot password ?</a></p>
    </div>
  </div>
</form>


@endsection
