@extends('layouts.guest')
@section('content')

<form  action="{{ route('password.update') }}"  class="login-form" method="post">
  @csrf
  <input type="hidden" name="token" value="{{ $token }}">
  <div class="row">
    <div class="input-field col s12">
      <h5 class="ml-4">Reset Password Panel</h5>
    </div>
  </div>
  <div class="row margin">
    <div class="input-field col s12">
      <i class="material-icons prefix pt-2">person_outline</i>

      <input id="email" type="text" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
      @error('email')
      <span class="invalid-feedback red-text text-darken-2" role="alert">
        <strong>{{ $message }}</strong>
      </span>
      @enderror


      <label for="email" class="center-align">Email</label>
    </div>
  </div>
  <div class="row margin">
    <div class="input-field col s12">
      <i class="material-icons prefix pt-2">lock_outline</i>

      <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

      @error('password')
      <span class="invalid-feedback red-text text-darken-2" role="alert">
        <strong>{{ $message }}</strong>
      </span>
      @enderror
      <label for="password">{{ __('Password') }}</label>
    </div>
  </div>

  <div class="row margin">
    <div class="input-field col s12">
      <i class="material-icons prefix pt-2">lock_outline</i>

      <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
      <label for="password-confirm">{{ __('Confirm Password') }}</label>
    </div>
  </div>





  <div class="row">
    <div class="input-field col s12">
      <button type="submit" class="btn waves-effect waves-light border-round gradient-45deg-purple-deep-orange col s12">{{ __('Reset Password') }}</button>
    </div>
  </div>
  <div class="row">
    <div class="input-field col s6 m6 l6">
      <p class="margin medium-small"><a href="{{ route('register') }}">Register Now!</a></p>
    </div>
    <div class="input-field col s6 m6 l6">
      <p class="margin right-align medium-small"><a href="{{ route('password.request') }}">Forgot password ?</a></p>
    </div>
  </div>
</form>


@endsection