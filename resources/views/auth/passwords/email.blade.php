@extends('layouts.guest')
@section('content')
@if (session('status'))
<div class="alert alert-success" role="alert">
  {{ session('status') }}
</div>
@endif
<form  action="{{ route('password.email') }}"  class="login-form" method="post">
  @csrf
  <div class="row">
    <div class="input-field col s12">
      <h5 class="ml-4">Reset Password Panel</h5>
    </div>
  </div>
  <div class="row margin">
    <div class="input-field col s12">
      <i class="material-icons prefix pt-2">person_outline</i>
      <input id="email" type="text" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
      @error('email')
      <span class="invalid-feedback red-text text-darken-2" role="alert">
        <strong>{{ $message }}</strong>
      </span>
      @enderror
      <label for="email" class="center-align">Email</label>
    </div>
  </div>
  <div class="row">
    <div class="input-field col s12">
      <button type="submit" class="btn waves-effect waves-light border-round gradient-45deg-purple-deep-orange col s12">{{ __('Send Password Reset Link') }}</button>
    </div>
  </div>
  <div class="row">
    <div class="input-field col s6 m6 l6">
      <p class="margin medium-small"><a href="{{ route('register') }}">Register Now!</a></p>
    </div>
    <div class="input-field col s6 m6 l6">
      <p class="margin right-align medium-small"><a href="{{ route('login') }}">Have an account ?</a></p>
    </div>
  </div>
</form>
@endsection