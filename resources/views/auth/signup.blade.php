@extends('layouts.guest')
@section('content')

        <form action="{{ route('signup') }}" method="post">
            @csrf
         <div class="card-body pt-0">
                        <form class="form-horizontal" action="index.html" novalidate>

                            <fieldset class="form-group position-relative has-icon-left">
                                <input type="text" class="form-control" id="user-name" placeholder="Your Name"
                                    required>
                                <div class="form-control-position">
                                    <i class="la la-user"></i>
                                </div>
                            </fieldset>

                            <fieldset class="form-group position-relative has-icon-left">
                                <input type="text" class="form-control" id="user-email" placeholder="Your Email"
                                    required>
                                <div class="form-control-position">
                                    <i class="la la-user"></i>
                                </div>
                            </fieldset>

                            <fieldset class="form-group position-relative has-icon-left">
                                <input type="text" class="form-control" id="user-password" placeholder="Your Password"
                                    required>
                                <div class="form-control-position">
                                    <i class="la la-key"></i>
                                </div>
                            </fieldset>
                            <fieldset class="form-group position-relative has-icon-left">
                                <input type="password" class="form-control" id="user-mobile" placeholder="Enter mobile"
                                    required>
                                <div class="form-control-position">
                                    <i class="la la-key"></i>
                                </div>
                            </fieldset>
                            <button type="submit" class="btn btn-info btn-lg btn-block"><i class="ft-unlock"></i>Get Started</button>
                        </form>
                    </div>
                    <div class="card-body">
                        <p class="text-center"><a href="recover-password.html" class="card-link">Recover password</a></p>
                        <p class="text-center">New to Modern Admin? <a href="register-with-navbar.html" class="card-link">Create
                                Account</a></p>
                    </div>
      </form>
      
@endsection