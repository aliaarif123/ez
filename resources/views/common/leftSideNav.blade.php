<!-- BEGIN: SideNav-->
  <aside class="sidenav-main nav-expanded nav-lock nav-collapsible sidenav-light navbar-full sidenav-active-rounded">
    <div class="brand-sidebar">
    <!-- {{ url('/dashboard').'/'.strtolower(Auth::user()->role) }}  -->
      <h1 class="logo-wrapper"><a class="brand-logo darken-1" href="{{ url('/') }} ">
         <img src="{{ asset('images/logo/logo.png') }}" alt="logo" />
        <span class="logo-text hide-on-med-and-down">ArabEasy</span><br/>
        <!-- <small style="text-align:center">{{ Session::get('role') }}</small> -->
        </a>
      
        <a class="navbar-toggler" href="#"><i class="material-icons">radio_button_checked</i></a></h1>
    </div>
    <ul class="sidenav sidenav-collapsible leftside-navigation collapsible sidenav-fixed menu-shadow" id="slide-out" data-menu="menu-navigation" data-collapsible="menu-accordion">
<li class="navigation-header"><a class="navigation-header-text">YOUR DASHBOARD </a><i class="navigation-header-icon material-icons">more_horiz</i>
      </li>
   <!--    <li class="bold"><a class="collapsible-header waves-effect waves-cyan " href="#"><i class="material-icons">photo_filter</i><span class="menu-title" data-i18n="">Menu levels</span></a>
        <div class="collapsible-body">
          <ul class="collapsible collapsible-sub" data-collapsible="accordion">
            <li><a class="collapsible-body" href="#" data-i18n=""><i class="material-icons">radio_button_unchecked</i><span>Second level</span></a>
            </li>
            <li><a class="collapsible-body collapsible-header waves-effect waves-cyan" href="#" data-i18n=""><i class="material-icons">radio_button_unchecked</i><span>Second level child</span></a>
              <div class="collapsible-body">
                <ul class="collapsible" data-collapsible="accordion">
                  <li><a class="collapsible-body" href="#" data-i18n=""><i class="material-icons">radio_button_unchecked</i><span>Third level</span></a>
                  </li>
                </ul>
              </div>
            </li>
          </ul>
        </div>
      </li> -->


      @if(Session::get('role') == 'Admin')
        @include('common.li.admin')
      @elseif(Session::get('role') == 'Translator')
        @include('common.li.tr')
      @elseif(Session::get('role') == 'Operations')
        @include('common.li.ou')
      @elseif(Session::get('role') == 'Proof Reader')
        @include('common.li.pr')
      @elseif(Session::get('role') == 'Quality Analyst')
        @include('common.li.qa')
      @else
        @include('common.li.client')
      @endif





    </ul>
    <div class="navigation-background"></div>
    <a class="sidenav-trigger btn-sidenav-toggle btn-floating btn-medium waves-effect waves-light hide-on-large-only" href="javascript:;" data-target="slide-out"><i class="material-icons">menu</i></a>


  </aside>
  <!-- END: SideNav-->