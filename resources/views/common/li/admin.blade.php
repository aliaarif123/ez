@php 
//dd(Route::currentRouteName());
@endphp
<li class="bold">
	<a class="waves-effect waves-cyan collapsible-body {{ Route::currentRouteName() == 'dashboard.admin.index' ? 'active' : '' }}" href="{{ route('dashboard.admin.index') }}">
		<i class="material-icons">track_changes</i>
		<span class="menu-title" data-i18n="">Dashboard</span>
	</a>
</li>

<li class="bold">
	<a class="waves-effect waves-cyan {{ Route::currentRouteName() == 'dashboard.admin.manageAssignments' ? 'active' : '' }}" href="{{ route('dashboard.admin.manageAssignments') }}">
		<i class="material-icons">supervisor_account</i>
		<span class="menu-title" data-i18n="">Manage Assignments</span>
	</a>
</li>

<li class="bold">
	<a class="waves-effect waves-cyan {{ Route::currentRouteName() == 'dashboard.admin.manageUsers' ? 'active' : '' }}" href="{{ route('dashboard.admin.manageUsers') }}">
		<i class="material-icons">supervisor_account</i>
		<span class="menu-title" data-i18n="">Manage User</span>
	</a>
</li>

<li class="bold">
	<a class="waves-effect waves-cyan {{ Route::currentRouteName() == 'dashboard.admin.manageTranslotors' ? 'active' : '' }}" href="{{ route('dashboard.admin.manageTranslotors') }}">
		<i class="material-icons">supervisor_account</i>
		<span class="menu-title" data-i18n="">Manage Translotor</span>
	</a>
</li>

<li class="bold">
	<a class="waves-effect waves-cyan {{ Route::currentRouteName() == 'dashboard.admin.manageTests' ? 'active' : '' }}" href="{{ route('dashboard.admin.manageTests') }}">
		<i class="material-icons">import_contacts</i>
		<span class="menu-title" data-i18n="">Manage Test</span>
	</a>
</li>

<li class="bold">
	<a class="waves-effect waves-cyan {{ Route::currentRouteName() == 'dashboard.admin.manageRoles' ? 'active' : '' }}" href="{{ route('dashboard.admin.manageRoles') }}">
		<i class="material-icons">import_contacts</i>
		<span class="menu-title" data-i18n="">Manage Roles</span>
	</a>
</li>

<li class="bold">

	<a class="waves-effect waves-cyan {{ Route::currentRouteName() == 'dashboard.admin.manageClients' ? 'active' : '' }}" href="{{ route('dashboard.admin.manageClients') }}">
		<i class="material-icons">import_contacts</i>
		<span class="menu-title" data-i18n="">Manage Clients</span>
	</a>
</li>

<!--
<li class="bold">
	<a class="waves-effect waves-cyan " href="{{ route('dashboard.admin.evaluateTests') }}">
		<i class="material-icons">import_contacts</i>
		<span class="menu-title" data-i18n="">Evaluate Test</span>
	</a>
</li>

<li class="bold">
	<a class="waves-effect waves-cyan " href="{{ route('dashboard.admin.generalSettings') }}">
		<i class="material-icons">import_contacts</i>
		<span class="menu-title" data-i18n="">General Settings</span>
	</a>
</li>
-->      