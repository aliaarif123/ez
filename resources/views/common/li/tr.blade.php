<?php 

// $testStatusCount = DB::table('test_attempt')
// ->where('ucode', Auth::user()->ucode)->where('status', 1)->count();

// $lpCount = DB::table('language_pairs')
// ->where('ucode', Auth::user()->ucode)
// ->where('flag', 'language-pair')
// ->count();

// $lpC = $lpCount == 2 ? 4 : $lpCount * $lpCount - 1;

// if($lpC == $testStatusCount) {
// 	$customeDisabled = 'display:block';
// }else{
// 	$customeDisabled = 'display:none';
// }

// $testStatusCount = DB::table('language_pairs')
// ->where('ucode', Auth::user()->ucode)
// ->where('flag', 'language-pair')
// ->where('status', 1)
// ->where('contract_send', 1)
// ->count();

// if($testStatusCount) {
// 	$customeDisabled = 'display:block';
// }else{
// 	$customeDisabled = 'display:none';
// }

$customeDisabled = 'display:block'

?>

<li class="bold">
	<a class="waves-effect waves-cyan collapsible-body {{ Route::currentRouteName() == 'dashboard.translator.index' ? 'active' : '' }}" href="{{ route('dashboard.translator.index') }}">
		<i class="material-icons">track_changes</i>
		<span class="menu-title" data-i18n="">Dashboard</span>
	</a>
</li>

@php
$arr1 = [
'dashboard.translator.myassignments.activeAssignments',
'dashboard.translator.myassignments.liveFeed' 
];
@endphp
<li class="{{ in_array(Route::currentRouteName(), $arr1) ? 'bold open active' : '' }}">
	<a class="collapsible-header waves-effect waves-cyan " href="javascript:"><i class="material-icons">format_indent_increase</i>
		<span class="menu-title" data-i18n="">My Assignments</span>
	</a>
	<div class="collapsible-body">
		<ul class="collapsible collapsible-sub" data-collapsible="accordion">
			<li><a class="collapsible-body  {{ Route::currentRouteName() == 'dashboard.translator.myassignments.activeAssignments' ? 'active' : '' }}" href="{{ route('dashboard.translator.myassignments.activeAssignments') }}" data-i18n=""><i class="material-icons">radio_button_unchecked</i><span>Active Assignments</span></a>
			</li>
			<li>
				<a class="collapsible-body {{ Route::currentRouteName() == 'dashboard.translator.myassignments.liveFeed' ? 'active' : '' }}" href="{{ route('dashboard.translator.myassignments.liveFeed') }}" data-i18n=""><i class="material-icons">radio_button_unchecked</i><span>Live Feed</span>
				</a>
			</li>
		</ul>
	</div>
</li>
@php
$arr2 = [
'dashboard.translator.myprofile.personalInformation', 
'dashboard.translator.myprofile.skills',
'dashboard.translator.myprofile.evaluation', 
'dashboard.translator.myprofile.professionalDetails', 
'dashboard.translator.myprofile.paymentMethod',
'dashboard.translator.showUserTestSingle'
];
@endphp
<li class="{{ in_array(Route::currentRouteName(), $arr2) ? 'bold open active' : '' }}">
	<a  class="collapsible-header waves-effect waves-cyan " href="javascript:;"><i class="material-icons">format_indent_increase</i><span class="menu-title" data-i18n="">My Profile</span></a>
	<div class="collapsible-body">
		<ul class="collapsible collapsible-sub" data-collapsible="accordion">
			<li><a class="collapsible-body {{ Route::currentRouteName() == 'dashboard.translator.myprofile.personalInformation' ? 'active' : '' }}" href="{{ route('dashboard.translator.myprofile.personalInformation') }}" data-i18n=""><i class="material-icons">radio_button_unchecked</i><span>Personal Information</span></a>
			</li>	
			<li><a class="collapsible-body {{ Route::currentRouteName() == 'dashboard.translator.myprofile.skills' ? 'active' : '' }}" href="{{ route('dashboard.translator.myprofile.skills') }}" data-i18n=""><i class="material-icons">radio_button_unchecked</i><span>Skills</span></a>
			</li>	
			<li><a class="collapsible-body {{ Route::currentRouteName() == 'dashboard.translator.myprofile.evaluation'  || Route::currentRouteName() == 'dashboard.translator.showUserTestSingle' ? 'active' : '' }}" href="{{ route('dashboard.translator.myprofile.evaluation') }}" data-i18n=""><i class="material-icons">radio_button_unchecked</i><span>Evaluation</span></a>
			</li>

			<li><a class="collapsible-body {{ Route::currentRouteName() == 'dashboard.translator.myprofile.publicProfile' ? 'active' : '' }}" href="{{ route('dashboard.translator.myprofile.publicProfile') }}" data-i18n=""><i class="material-icons">radio_button_unchecked</i><span>Public Profile</span></a>
			</li>

			<!-- <li style="{{ $customeDisabled }}"><a class="collapsible-body {{ Route::currentRouteName() == 'dashboard.translator.myprofile.professionalDetails' ? 'active' : '' }}" href="{{ route('dashboard.translator.myprofile.professionalDetails') }}" data-i18n=""><i class="material-icons">radio_button_unchecked</i><span>Professional Details</span></a>
			</li>	
			<li style="{{ $customeDisabled }}"><a class="collapsible-body {{ Route::currentRouteName() == 'dashboard.translator.myprofile.paymentMethod' ? 'active' : '' }}" href="{{ route('dashboard.translator.myprofile.paymentMethod') }}" data-i18n=""><i class="material-icons">radio_button_unchecked</i><span>Payment Method</span></a>
			</li>			 -->
		</ul>
	</div>
</li>


@php
$arr3 = [
'dashboard.translator.my-account.contracts'
];
@endphp

<li style="{{ $customeDisabled }}" class="{{ in_array(Route::currentRouteName(), $arr3) ? 'bold open active' : '' }}">
	<a class="collapsible-header waves-effect waves-cyan " href="javascript:"><i class="material-icons">track_changes</i>
		<span class="menu-title" data-i18n="">My Account</span>
	</a>
	<div class="collapsible-body">
		<ul class="collapsible collapsible-sub" data-collapsible="accordion">
			<li><a class="collapsible-body {{ Route::currentRouteName() == 'dashboard.translator.my-account.contracts' ? 'active' : '' }}" href="{{ route('dashboard.translator.my-account.contracts') }}" data-i18n=""><i class="material-icons">radio_button_unchecked</i><span>My Contracts</span></a>
			</li>	
			
		</ul>
	</div>
</li>


<li class="bold" id="getHelp">
	<a class="waves-effect waves-cyan collapsible-body {{ Route::currentRouteName() == 'dashboard.translator.getHelp' ? 'active' : '' }}" href="{{ route('dashboard.translator.getHelp') }}">
		<i class="material-icons">track_changes</i>
		<span class="menu-title" data-i18n="">Get Help</span>
	</a>
</li>



