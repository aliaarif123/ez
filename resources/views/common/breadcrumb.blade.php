 <div class="col s10 m10 l10 breadcrumbs-left">
    @if(Request::segment(5) != null)
    @php 
    $test_id = base64_decode(Request::segment(5));
    $test = App\Test::where('status', 1)->where('id', $test_id)->first();
    @endphp
    <h5 class="breadcrumbs-title mt-0 mb-0 display-inline hide-on-small-and-down">
      {{ str_replace('-', ' ', ucwords($test->title)) }}
  </h5>                  
  @elseif(Request::segment(4) != null || gettype(Request::segment(5))  == 'integer')
  @php 
  $test_id = base64_decode(Request::segment(5));
  $test = App\Test::where('status', 1)->where('id', $test_id)->first();
  @endphp
  <h5 class="breadcrumbs-title mt-0 mb-0 display-inline hide-on-small-and-down">
      {{ str_replace('-', ' ', ucwords(Request::segment(4))) }}
  </h5>
  @elseif(Request::segment(3) != null || gettype(Request::segment(5))  == 'integer')
  @php 
  $test_id = base64_decode(Request::segment(5));
  $test = App\Test::where('status', 1)->where('id', $test_id)->first();
  @endphp
  <h5 class="breadcrumbs-title mt-0 mb-0 display-inline hide-on-small-and-down">
      {{ str_replace('-', ' ', ucwords(Request::segment(3))) }}
  </h5>
  @elseif(Request::segment(2) != null || gettype(Request::segment(5))  == 'integer')
  @php 
  $test_id = base64_decode(Request::segment(5));
  $test = App\Test::where('status', 1)->where('id', $test_id)->first();
  @endphp
  <!-- <h5 class="breadcrumbs-title mt-0 mb-0 display-inline hide-on-small-and-down">
      {{ str_replace('-', ' ', ucwords(Request::segment(2))) }}
    </h5> -->
  @elseif(Request::segment(1) != null || gettype(Request::segment(5))  == 'integer')
  @php 
  $test_id = base64_decode(Request::segment(5));
  $test = App\Test::where('status', 1)->where('id', $test_id)->first();
  @endphp
  <h5 class="breadcrumbs-title mt-0 mb-0 display-inline hide-on-small-and-down">
      {{ str_replace('-', ' ', ucwords(Request::segment(1))) }}
  </h5>
  @else
  @endif
  <ol class="breadcrumbs mb-0">
      <li class="breadcrumb-item">
        <a href="{{ route('dashboard').'/'.Request::segment(2) }}">Dashboard</a>
    </li>
    @if(Request::segment(1) != null && Request::segment(2) == null)
    <li class="breadcrumb-item">
        {{ str_replace('-', ' ', ucwords(Auth::user()->role)) }}
    </li>
    @elseif(Request::segment(2) != null && Request::segment(3) == null )
    <!-- <li class="breadcrumb-item active">
      {{ str_replace('-', ' ', ucwords(Request::segment(2))) }}              
    </li> -->
    @elseif(Request::segment(3) != null && Request::segment(4) == null )
   <!--  <li class="breadcrumb-item">
        <a href="{{ route('dashboard').'/'.Request::segment(2) }}">
          {{ str_replace('-', ' ', ucwords(Request::segment(2))) }}    
      </a>          
    </li> -->
  <li class="breadcrumb-item active">
    {{ str_replace('-', ' ', ucwords(Request::segment(3))) }}
</li>
@elseif(Request::segment(4) != null && Request::segment(5) == null )
<!-- <li class="breadcrumb-item">
    <a href="{{ route('dashboard').'/'.Request::segment(2) }}">
      {{ str_replace('-', ' ',   ucwords(Request::segment(2))) }}    
  </a>          
</li> -->
<li class="breadcrumb-item">
    {{ str_replace('-', ' ', ucwords(Request::segment(3))) }}    
</li>
<li class="breadcrumb-item active">
    {{ str_replace('-', ' ', ucwords(Request::segment(4))) }}
</li>
@elseif(Request::segment(5) != null)
@php 
$test_id = base64_decode(Request::segment(5));
$test = App\Test::where('status', 1)->where('id', $test_id)->first();
@endphp
<!-- <li class="breadcrumb-item">
    <a href="{{ route('dashboard').'/'.Request::segment(2) }}">
      {{ str_replace('-', ' ', ucwords(Request::segment(2))) }}    
  </a>          
</li> -->
<li class="breadcrumb-item">
    {{ str_replace('-', ' ', ucwords(Request::segment(3))) }}    
</li>
<li class="breadcrumb-item">
    {{ str_replace('-', ' ', ucwords(Request::segment(4))) }}    
</li>
<li class="breadcrumb-item active">
    {{ str_replace('-', ' ', ucwords($test->title)) }}
</li>
@endif
</ol>
</div>