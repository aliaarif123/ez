Type: Pending Contract
<div id="intro">
	<div id="img-modal" class="modal modal1 modalA black darken-1" style="overflow:hidden;">
		<div class="modal-content" style=" background-color: #111;">
			<p class="modal-header right modal-close" >
				<span class="right"><i class="material-icons right-align  white-text" id="intro-close" >clear</i></span>
			</p>
			<h4 align="center" class="mt-5 white-text" >Welcome to ArabEasy</h4>
			<h5 align="center" class="mt-5 white-text">Please complete the steps below</h5>
			<div class="carousel carousel-slider center intro-carousel" >
				<div class="carousel-fixed-item center middle-indicator" style="height: 110px;">
					<div class="left">
						<button class=" movePrevCarousel middle-indicator-text btn btn-flat white-text waves-effect waves-light btn-prev">
							<span class="white-text">Prev</span>
						</button>
					</div>
					<div class="right">
						<button class=" moveNextCarousel middle-indicator-text btn btn-flat white-text waves-effect waves-light btn-next">
							<span class="white-text">Next</span>
						</button>
					</div>
				</div>
				<div class="carousel-item slide-1">
					<h5 class="intro-step-title mt-5 center animated fadeInUp white-text">Go to My Profile and fill up your professional details and payment method</h5>
					<h5 class="intro-step-title mt-5 center animated fadeInUp white-text">
						<a href="{{ route('dashboard.translator.myprofile.personalInformation') }}" class="waves-effect waves-dark btn btn-lg btn-primary">Go to the Section</a>
					</h5>
				</div>
				<div class="carousel-item slide-2">
					<h5 class="intro-step-title mt-5 center animated fadeInUp white-text">Go to My Account > Contracts and approve the price per word offered on the basis of test rating</h5>
					<h5 class="intro-step-title mt-5 center animated fadeInUp white-text">
						<a  href="{{ route('dashboard.translator.myprofile.evaluation') }}" class="waves-effect waves-dark btn btn-lg btn-primary">Go to the Section</a>
					</h5>
				</div>
				<div class="carousel-item slide-3">
					<h5 class="intro-step-title mt-5 center animated fadeInUp white-text">Read and sign the contract and begin working on assignments.</h5>
				</div>
			</div>
		</div>
	</div>
</div>

