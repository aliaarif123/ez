<?php

// Session::put('finalStatus', Auth::user()->profile->finalStatus);

// $role = Auth::user()->role;

// if($role == 'Translator'){
// 	$status = 'Status : '.Session::get('finalStatus');
// }

//$status = Auth::user()->finalStatus;

//$status = 'Incomplete profile';
//if($user->role == 'Translator' || $user->role == 'Quality Analyst'){
/* Start (Incomplete profile) Status*/
	//$status = 'Incomplete profile';
/* End (Incomplete profile) Status*/

/* Start (Test pending) Status*/
	// if($user->profile->personal_information_flag == 1 &&
	// 	$user->profile->skills_language_proficiency_flag == 1 &&
	// 	$user->profile->skills_subject_matter_expertise_flag == 1 &&
	// 	$user->profile->skills_software_and_tools_flag == 1){
	// 	$status = 'Test pending';
	// }
/* End (Test pending) Status*/


	// $testStatusCount = DB::table('test_attempt')
	// ->where('ucode', Auth::user()->ucode)->where('status', 1)->count();
	// $lpCount = DB::table('language_pairs')
	// ->where('ucode', Auth::user()->ucode)
	// ->where('flag', 'language-pair')
	// ->count();
	// $lpC = $lpCount == 2 ? 4 : $lpCount * $lpCount - 1;
	// if($lpC == $testStatusCount) {
	// 	$status = 'Under evaluation';
	// }


	// $lps = DB::table('language_pairs')
	// ->where('ucode', Auth::user()->ucode)
	// ->where('flag', 'language-pair')
	// ->get();
	// $EvaluationStatus = false;
	// foreach ($lps as  $value) {
	// 	if(Session::get('StatusOf'.$value->name) == 1){
	// 		$EvaluationStatus = true;      
	// 	}
	// }
	// if($EvaluationStatus == true){
	// 	$status = 'Contract Pending/Improvement';      
	// }


	// $lps = DB::table('language_pairs')
	// ->where('ucode', Auth::user()->ucode)
	// ->where('flag', 'language-pair')
	// ->first();

	// if($lps->contract_send == 1 && $lps->contract_accept == 1){
	// 	$status = 'On-Pilot';      
	// }

	// if(Auth::user()->finalStatus){
	// 	$status = Auth::user()->finalStatus;      
	// }

//}


// $testStatusCount = DB::table('test_attempt')
// ->where('ucode', Auth::user()->ucode)->where('status', 1)->count();
// $lpCount = DB::table('language_pairs')
// ->where('ucode', Auth::user()->ucode)
// ->where('flag', 'language-pair')
// ->count();
// $lpC = $lpCount == 2 ? 4 : $lpCount * $lpCount - 1;
// if(Auth::user()->profile->personal_information_flag == 1){
// 	$status = 'Profile Completed';
// 	if($lpC == $testStatusCount) {
// 		$status = 'Under Evaluation';
// 		if(Session::has('status') && Session::get('status') == 'Active'){
// 			$status = 'Active';      
// 		}
// 	}
// }
?>
<!-- BEGIN: Header-->
<header class="page-topbar" id="header">
	<div class="navbar navbar-fixed"> 
		<nav class="navbar-main navbar-color nav-collapsible sideNav-lock navbar-dark gradient-45deg-purple-deep-orange gradient-shadow">
			<div class="nav-wrapper">
				@if(Request::segment(3) == 'test' && Request::segment(4) && Request::segment(5))
				<?php 
				$test_id2 = base64_decode(Request::segment(5));
				$test2 =  DB::table('test_attempt')->where('test_id', $test_id2)->where('ucode', Auth::user()->ucode)->first();
				if($test2){ ?>
					<ul class="navbar-list customeTimerOnHeader1">
						<li style="position:relative; left: 270px; text-decoration:blink">
							<i clsass="customeTimerOnHeader2">
								<span id="time"></span> <small>( Time left for submission )</small>
							</i>
						</li>
					</ul>
				<?php } ?>
				@endif
				<ul class="navbar-list right">
					<li id="userFinalStatus">Status: {{ Auth::user()->profile->finalStatus }}</li>
					<li><a class="waves-effect waves-block waves-light profile-button" href="javascript:;" data-target="profile-dropdown"><span class="material-icons">format_indent_increase</span></a></li>
				</ul>
				<ul class="dropdown-content" id="profile-dropdown">
					<li><a class="grey-text text-darken-1" href="javascript:;"><i class="material-icons">person_outline</i> Profile</a></li>
					<li>
						<a class="grey-text text-darken-1" href="{{ route('logout') }}" data-target="slide-out" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="material-icons">keyboard_tab</i>Logout</a>
						<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
							@csrf
						</form>
					</li>
				</ul>
			</div>
			<nav class="display-none search-sm">
				<div class="nav-wrapper">
<!--    <form>
<div class="input-field">
<input class="search-box-sm" type="search" required="">
<label class="label-icon" for="search"><i class="material-icons search-sm-icon">search</i></label><i class="material-icons search-sm-close">close</i>
</div>
</form> -->
</div>
</nav>
</nav>
</div>
</header>
<!-- END: Header -->

@push('js')
<script type="text/javascript">
	$(() => {


		//localStorage.getItem('status') ? $('#userFinalStatus').html(`Status : `+localStorage.getItem('status')) : 'Profile Incomplete';
	});
</script>
@endpush