@component('mail::message')
# Introduction

The body of your message.

@component('mail::button', ['url' => $link])
SignUp Here
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
