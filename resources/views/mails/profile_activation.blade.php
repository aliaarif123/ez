@component('mail::message')
Hello **{{$name}}**,  {{-- use double space for line break --}}
Thank you for choosing ArabEasy!

Click below to start working right now
@component('mail::button', ['url' => $link])
Setup Your Account
@endcomponent
Sincerely,  
ArabEasy team.
@endcomponent