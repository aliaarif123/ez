@extends('layouts.user')

@section('content')

<div class="row">
 <div class="col s12 m6 l4">
  <div class="card padding-4 animate fadeLeft">
   <div class="col s5 m5">
    <h5 class="mb-0">{{ $user_count}}</h5>
  </div>
  <div class="col s7 m7 right-align">
    <i class="material-icons background-round mt-5 mb-5 gradient-45deg-purple-amber gradient-shadow white-text">perm_identity</i>
    <p class="mb-0">Total Translators</p>
  </div>
</div>
</div>
<a href="{{ route('dashboard.admin.addUser') }}">
  <div class="col s12 m6 l4">
    <div class="card padding-4 animate fadeLeft">
      <div class="col s5 m5">
        <i class="material-icons pink-text">add</i>
      </div>
      <div class="col s7 m7 right-align">
        <i class="material-icons background-round mt-5 mb-5 gradient-45deg-purple-amber gradient-shadow white-text">perm_identity</i>
        <p class="mb-0">Invite New Translator</p>
      </div>
    </div>
  </div>
</a>

<div class="col s12 m12 l12">

  <span id="successMessage"></span>

  <div class="card subscriber-list-card animate fadeRight">
   <div class="card-content pb-1">
    <h4 class="card-title mb-0">Translators List <i class="material-icons float-right">more_vert</i></h4>
  </div>
  <table class="subscription-table responsive-table highlight">
    <thead>
     <tr>
      <th>Name</th>
      <th>Email</th>
      <th>Phone</th>
      <th>Evaluate Tests</th>
      <th>Contract</th>
      <th>Action</th>
    </tr>
  </thead>
  <tbody>
    @foreach ($users as $user)
    <tr id="myTableRow_{{ $user->ucode }}">
      <td>{{ $user->profile->first_name ?? '' }}</td>
      <td>{{ $user->email ?? '' }}</td>
      <td>{{ $user->profile->mobile ?? '' }}</td>
      <td><a href="{{ route('dashboard.admin.evalTests', [$user->ucode]) }}" title="Evaluate Tests"><i class="material-icons pink-text">rate_review</i></a></td>
      <td><a href="{{ route('dashboard.admin.updateContract', [$user->ucode]) }}" title="Update Contract"><i class="material-icons pink-text">rate_review</i></a></td>
      <td class="left-align">
        <a href="{{ route('dashboard.translator.manageProfile', [$user->ucode]) }}" title="Edit Details"><i class="material-icons pink-text">create</i></a>
        <a href="javascript:;" class="aaa" data-ucode='{{ $user->ucode }}' title="Delete"><i class="material-icons pink-text">clear</i></a>
      </td>
    </tr>
    @endforeach
  </tbody>
</table>
</div>
</div>
</div>
@endsection
@push('js')
<script type="text/javascript">

  $(() => {

    var token = document.head.querySelector('meta[name="csrf-token"]');
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;

  //console.log(token.content);

  $('.aaa').on('click', function(){
    if (confirm('Are you sure ?')) {
      var id = $(this).data('ucode');
      axios.post(bURL+'dashboard/admin/deleteUser', {
        'id': id
      })
      .then(function (response) {
        if(response.data==="FAIL@"){
          alert('fail');
        }else{
          $('#successMessage').html(`
            <div class="card-alert card green lighten-5" >
            <div class="card-content green-text">
            <p>SUCCESS : The Translator `+response.data.trName+` with email `+response.data.trEmail+` removed successfully.</p>
            </div>
            <button type="button" class="close green-text" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
            </button>
            </div>
            `)
          //console.log(response.data);
          $('#myTableRow_'+response.data.userId).remove();
          $('html, body').stop();
        }
            //console.log(response.data);
          })
      .catch(function (error) {
        console.log(error);
      });
    }
  });

});
</script>
@endpush