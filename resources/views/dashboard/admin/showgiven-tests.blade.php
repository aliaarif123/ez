@extends('layouts.user')

@section('content')

<div class="row">
  <div class="col s12 m12 l12">
    <div class="card subscriber-list-card animate fadeRight">
     <div class="card-content pb-1">
      <h4 class="card-title mb-0">Test Attempts<i class="material-icons float-right">more_vert</i></h4>
    </div>
    @if(Session::has('success'))
    <div class="badge green lighten-5 green-text text-accent-4">
      {{ Session::get('success') }}
    </div>
    @endif

    <table class="subscription-table responsive-table highlight">
      <thead>
       <tr>
        <th>Test</th>
        <th>Origin Language</th>
        <th>Translate Langauge</th>
        <th>Test Type</th>
        <th>Evaluate</th>
      </tr>
    </thead>
    <tbody>
      <?php
      $finalGivenTestArr = array();
      foreach ($userTests as $test){
        $finalGivenTestArr[$test->lang_pair][] = $test;
      }
      ?>


      <?php
      foreach ($finalGivenTestArr as $key_LP => $tests){
        ?>
        <th align="center" colspan="5">{{ $key_LP }}</th>
        <?php
        foreach ($tests as $test){
          ?>
          <tr id="myTableRow_{{ $test->id }}">
            <td>{{ $test->title }}</td>
            <td>{{ $test->source_language }}</td>
            <td>{{ $test->target_language }}</td>

            <td>
              @if ($test->test_type === "text")
              <span class="badge blue lighten-5 blue-text text-accent-4"> Text </span>
              @else
              <span class="badge blue lighten-5 blue-text text-accent-4"> File </span>
              @endif
            </td>
            <?php
            if($test->test_score){
              Session::put('StatusOf'.$test->source_language.'-'.$test->target_language, 1);
              ?>
              <td>
                <i class="material-icons pink-text">done</i></a>
              </td>
              <?php
            }else{
              Session::put('StatusOf'.$test->source_language.'-'.$test->target_language, 1);
              ?>
              <td>
                <?php
                $userTestID = $test->ucode."@@".$test->id;
                ?>
                <a href="{{ route('dashboard.admin.evaluateTest', [$userTestID]) }}" title="Evaluate"><i class="material-icons pink-text">rate_review</i></a>
              </td>
              <?php
            }
            ?>
          </tr>
          <?php
        }
      }
      ?>

    </tbody>
  </table>
</div>
</div>
</div>
@endsection
@push('js')
<script type="text/javascript">

  $(() => {

    var token = document.head.querySelector('meta[name="csrf-token"]');
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;

  //console.log(token.content);

  $('.deleteT').on('click', function(){

    if (confirm('Are you sure ?')) {
      var id = $(this).data('ucode');
      axios.post(bURL+'dashboard/admin/deleteTest', {
        'id': id
      })
      .then(function (response) {
        if(response.data==="FAIL@"){
          alert('fail');
        }else{
          $('#myTableRow_'+response.data).remove();
          $('html, body').stop();
        }
            //console.log(response.data);
          })
      .catch(function (error) {
        console.log(error);
      });
    }
  });

});
</script>
@endpush