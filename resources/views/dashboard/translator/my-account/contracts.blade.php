<?php
// echo '<pre>';
// print_r($languagePairRowExist);
$arrCount = count($languagePairRowExist);
//print_r($translatorDetails);
// die;
?>

<style type="text/css">

  .modal {
    height: 500px; //set a desired number
  }

  .link_button {
    -webkit-border-radius: 4px;
    -moz-border-radius: 4px;
    border-radius: 4px;
    border: solid 1px #20538D;
    text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.4);
    -webkit-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.4), 0 1px 1px rgba(0, 0, 0, 0.2);
    -moz-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.4), 0 1px 1px rgba(0, 0, 0, 0.2);
    box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.4), 0 1px 1px rgba(0, 0, 0, 0.2);
    background: #4479BA;
    color: #FFF;
    padding: 8px 12px;
    text-decoration: none;
    float: right;
    margin-right: 30px;
  }
</style>


@extends('layouts.user')

@section('content')

@php
$translatorUcode = Request::segment(5) ?? Auth::user()->ucode;
@endphp

<div class="row margin">
  @if(Session::has('msg'))
  <div class="card-alert card cyan lighten-5">
    <div class="card-content cyan-text">
      <p>Message : {{ Session::get('msg') }}</p>
    </div>
    <button type="button" class="close cyan-text" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">×</span>
    </button>
  </div>
  @endif
  <div class="input-field col s12">
    <table class="subscription-table responsive-table highlight">
      <thead>
       <tr>
        <th>Language Direction</th>
        <th>Current Score</th>
        <th>Current Rating</th>
        <th>Sign Contract</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($languagePairRowExist as $languagePairEach)
      <tr id="myTableRow_{{ $languagePairEach->id }}">
        <td>{{ $languagePairEach->name }}</td>
        <td>{{ $languagePairEach->final_score }} / 500</td>
        <td>{{ $languagePairEach->final_rating }} / 5</td>
        <?php

        $codeToupdateLP_details = $translatorUcode."XXX".$languagePairEach->id."XXX".$languagePairEach->name;
        ?>

        <?php if($languagePairEach->status==1 && $languagePairEach->contract_send==1){ ?>
          <?php 
          if($languagePairEach->contract_accept==1){ 
            Session::put('status', 'Active');
            ?>

            <td><i class="material-icons pink-text">done</i></td>
            <?php 
          }else{ 
            
            ?>
            <td><a href="javascript:;" class="openModalWithContactDetails" data-ucode='{{ $codeToupdateLP_details }}' title="Sign Contract">Sign Contract Here</a></td>
          <?php } ?>
        <?php }else{ ?>
          <td>N/A</td>
        <?php } ?>



      </tr>
      @endforeach
    </tbody>
  </table>
</div>
</div>
<!-- Modal Trigger -->
<!-- Modal Structure -->
<div id="modal1" class="modal">
  <div class="modal-content">
    <h5 align="center">Contract Detail</h5>
    <div class="row margin">
      <div class="col s12">
        <!--Invoice-->
        <div class="row">
          <div class="col s12 m12 l12">
            <div id="basic-tabs" class="card card card-default scrollspy">
              <div class="card-content pt-5 pr-5 pb-5 pl-5">
                <div id="invoice">
                  <div class="invoice-table">
                    <div class="row">
                      <div class="col s12 m12 l12">
                        {{ Form::open(array('url' => route('dashboard.translator.acceptContract'), 'method'=>'post', 'id' => 'acceptContract', 'name' => 'acceptContract')) }}
                        @csrf
                        <input type="hidden" name="name" id="name" >
                        <input type="hidden" name="ucode" id="ucode" >
                        <input type="hidden" name="lp_id" id="lp_id" >
                        <table class="highlight responsive-table">
                          <thead>
                                <!-- <tr>
                                  <th data-field="no">111</th>
                                  <th data-field="item">111</th>
                                </tr> -->
                              </thead>
                              <tbody>

                                <div id="lpWiseContract">
                                  Hi <span>{{ Auth::user()->profile->first_name }}</span>, We are happy to offer you the contract for below Language Direction, please read all mentioned details below.
                                </div>

                                <tr>
                                  <td>Language Direction: </td>
                                  <td><span id="language_direction"></span></td>
                                </tr>

                                <tr>
                                  <td>Contract Flag: </td>
                                  <td><span id="contract_type"></span></td>
                                </tr>

                                <tr>
                                  <td>Final Rating: </td>
                                  <td><span id="final_rating"></span></td>
                                </tr>

                                <tr>
                                  <td>Fixed Rate: </td>
                                  <td><span id="fixed_rate"></span></td>
                                </tr>

                                <tr>
                                  <td>Fixed Quota: </td>
                                  <td><span id="fixed_quota"></span></td>
                                </tr>

                                <tr>
                                  <td>Works as Proofreader: </td>
                                  <td><span id="proofreader"></span></td>
                                </tr>

                                <tr>
                                  <td>Language Direction: </td>
                                  <td><span id="translation_wd"></span></td>
                                </tr>

                                <tr>
                                  <td>Proofreading/wd: </td>
                                  <td><span id="proofreading_wd"></span></td>
                                </tr>

                                <!-- <tr>
                                  <td>Language Pair Id: </td>
                                  <td><span id="language_pair_id"></span></td>
                                </tr> -->

                              </tbody>
                            </table>
                            <button type="submit" id="agree" onclick="return confirm('Are you sure?')" class="btn waves-effect waves-light border-round gradient-45deg-purple-deep-orange col s12">Agree</button>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>


    </div>
  </div>
</div>
@endsection
@push('js')
<script type="text/javascript">

  $(() => {

    var token = document.head.querySelector('meta[name="csrf-token"]');
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;

    $('.openModalWithContactDetails').on('click', function(){
      //alert(1);
      var id = $(this).data('ucode');
      axios.post(bURL+'dashboard/translator/ModalWithLpDetails', {
        'id': id
      })
      .then(function (response) {
        if(response.data==="FAIL@"){
          console.log(0);
          console.log(response.data);
        }else{
          $(".modal").modal('open', {
            dismissible: false,
          });
          console.log(1);
          console.log(response.data);
          var str = response.data;
          var array = str.split("X-X");

          //alert(array[0]);

          $("#final_rating").html(array[0]);
          $("#contract_flag").html(array[1]);
          $("#contract_type").html(array[2]);
          $("#fixed_quota").html(array[5]);
          $("#proofreader").html(array[3]);
          $("#fixed_rate").html(array[4]);
          $("#translation_wd").html(array[6]);
          $("#proofreading_wd").html(array[7]);
          $("#language_pair_id").html(array[9]);
          $("#language_direction").html(array[10]);
          $("#name").val(array[10]);
          $("#ucode").val(array[8]);
          $("#lp_id").val(array[9]);

        }
      })
      .catch(function (error) {
        console.log(error);
      });
    });


    // $('#agree').on('click', () => {
    //   alert(1);
    // });
  });

</script>
@endpush

