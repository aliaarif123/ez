@extends('layouts.user')

@section('content')

<div class="row">
 <div class="col s12 m6 l3">
  <div class="card padding-4 animate fadeLeft">
   <div class="col s5 m5">
    <h5 class="mb-0">{{ count($assignments) }}</h5>
  </div>
  <div class="col s7 m7 right-align">
    <i class="material-icons background-round mt-5 mb-5 gradient-45deg-purple-amber gradient-shadow white-text">assignment</i>
  </div>
  <p class="mb-0">Total Assignments</p>
</div>
</div>
<div class="col s12 m6 l3">
  <div class="card padding-4 animate fadeLeft">
   <div class="col s5 m5">
    <h5 class="mb-0">{{ count($assignments) }}</h5>
  </div>
  <div class="col s7 m7 right-align">
    <i class="material-icons background-round mt-5 mb-5 gradient-45deg-purple-amber gradient-shadow white-text">assignment</i>
  </div>
  <p class="mb-0">Total assignments</p>
</div>
</div>
<div class="col s12 m6 l3">
  <div class="card padding-4 animate fadeLeft">
   <div class="col s5 m5">
    <h5 class="mb-0">{{ count($assignments) }}</h5>
  </div>
  <div class="col s7 m7 right-align">
    <i class="material-icons background-round mt-5 mb-5 gradient-45deg-purple-amber gradient-shadow white-text">assignment</i>
  </div>
  <p class="mb-0">Total assignments</p>
</div>
</div>
<div class="col s12 m6 l3">
  <div class="card padding-4 animate fadeLeft">
   <div class="col s5 m5">
    <h5 class="mb-0">{{ count($assignments) }}</h5>
  </div>
  <div class="col s7 m7 right-align">
    <i class="material-icons background-round mt-5 mb-5 gradient-45deg-purple-amber gradient-shadow white-text">assignment</i>
  </div>
  <p class="mb-0">Total Assignments</p>
</div>
</div>
</div>
<div class="col s12 m12 l12">
  <div class="card subscriber-list-card animate fadeRight">
   <div class="card-content pb-1">
    <h4 class="card-title mb-0">Assignment List <i class="material-icons float-right">more_vert</i></h4>
  </div>
  <table class="subscription-table responsive-table highlight">
    <thead>
     <tr>
      <th>Assignment Name</th>
      <th>Assignment Date</th>
      <th>Action</th>
    </tr>
  </thead>
  <tbody>
    @forelse($assignments as $assignment)

    <tr>
      <td>{{ $assignment->assignment_name }}</td>
      <td>{{ $assignment->created_at->toDayDateTimeString() }}</td>
      <td>
        <a href="#"><i class="material-icons green-text">edit</i></a>
        <a href="#"><i class="material-icons pink-text">clear</i></a>
      </td>
    </tr>

    @empty
    <tr>
      <td colspan="6"><div align="center">You have no assignments at all</div></td>
    </tr>
    @endforelse


  </tbody>
</table>
</div>
</div>
</div>
@endsection
@push('css')

@endpush

@push('js')

@endpush
