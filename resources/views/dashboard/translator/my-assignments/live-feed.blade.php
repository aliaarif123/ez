@extends('layouts.user')

@section('content')

<div id="intro" style="opacity: 0.7; color:white">
	<div class="row">
		<div class="col s12">
			<div id="img-modal" class="modal modal1 black">
				<div class="modal-content">
					<p class="modal-header right modal-close">
						<span class="right"><i class="material-icons right-align">clear</i></span>
					</p>
					<h4 align="center" class="txtWhite">Welcome to ArabEasy!</h4>
					<h6 align="center"  class="txtWhite">To get started you must complete the onboarding and evaluation process</h6>
					<div class="carousel carousel-slider center intro-carousel">
						<div class="carousel-fixed-item center middle-indicator">
							<div class="left">
								<button class="movePrevCarousel middle-indicator-text btn btn-flat purple-text waves-effect waves-light btn-prev">
									<i class="material-icons txtWhite">navigate_before</i> <span class="hide-on-small-only txtWhite">Prev</span>
								</button>
							</div>

							<div class="right">
								<button class=" moveNextCarousel middle-indicator-text btn btn-flat purple-text waves-effect waves-light btn-next">
									<span class="hide-on-small-only txtWhite">Next</span> <i class="material-icons txtWhite">navigate_next</i>
								</button>
							</div>
						</div>
						<div class="carousel-item slide-1">
							<h5 class="intro-step-title mt-10 center animated fadeInUp txtWhite">Go to My Profile and fill out sections</h5>
							<p class="intro-step-text mt-10 animated fadeInUp">
								<h4 align="center" class="txtWhite">Personal Information And Skills</h4>
							</p>
						</div>
						<div class="carousel-item slide-2">
							<h5 class="intro-step-title mt-10 center animated fadeInUp txtWhite">Submit test for language direction of choice in</h5>
							<p class="intro-step-text mt-10 animated fadeInUp">
								<h4 align="center" class="txtWhite">Evaluation section</h4>
							</p>
						</div>
						<div class="carousel-item slide-3">
							<h5 class="intro-step-title mt-10 center animated fadeInUp txtWhite">Pass evaluation and start working on assignments after filling</h5>
							<p class="intro-step-text mt-10 animated fadeInUp">
								<h4 align="center" class="txtWhite">Professional Details</h4>

								<button onclick="window.location.href=bURL+'dashboard/translator/my-profile/personal-information'" class="movePrevCarousel middle-indicator-text btn btn-flat purple-text waves-effect waves-light btn-prev">
									<span class="hide-on-small-only txtWhite">Go to My Profile Section</span>
								</button>

								
							</p>
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<template>
	<div class="row" id="liveFeed">


		<div class="col s12 m8 l8">
			<div class="card subscriber-list-card animate fadeRight">
				<table class="subscription-table responsive-table highlight">
					<thead>
						<tr>
							<th>File Type</th>
							<th>Content Type</th>
							<th>Language Pair</th>
							<th>Word Count</th>
							<th>Delivery By</th>
							<th>Status</th>
							<th>Submit Bid</th>
						</tr>
					</thead>
					<tbody>

						<tr>
							<td>File Type</td>
							<td>Content Type</td>
							<td>Language Pair</td>
							<td>Word Count</td>
							<td>Delivery By</td>
							<td>Status</td>
							<td>Submit Bid</td>
						</tr>


					</tbody>
				</table>
			</div>
		</div>

		<div class="col s12 m4 l4">
			<div class="card subscriber-list-card animate fadeRight">

				<table class="subscription-table responsive-table highlight">
					<thead>
						<tr>
							<th>Active Assignments</th>
						</tr>
					</thead>
					<tbody>

						<tr>
							<td>File Type</td>
						</tr>

					</tbody>
				</table>
			</div>
		</div>


	</div>
</template>
@endsection

@push('css')
<style type="text/css">
	.modal {
		max-height: 90%;
		color:white;

	}
	.txtWhite{
		color: #FFFFFF;
	}
</style>
@endpush

@push('js')
<!-- <script src="{{ asset('js/app.js') }}" type="text/javascript"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.0.5/vue.min.js" integrity="sha256-GOrA4t6mqWceQXkNDAuxlkJf2U1MF0O/8p1d/VPiqHw=" crossorigin="anonymous"></script>



<script>
	const app = new Vue({
		el: '#liveFeed',
		data :{

			modalCounter:1,

		},
		created() {

			localStorage.setItem('modalCounter', this.modalCounter);

			//alert('Vue js lib is working!');

		},          
		methods:{

		},
		mounted:function(){
			var element = document.getElementById('intro');
			if(localStorage.getItem('modalCounter') > 1){
				element.parentNode.removeChild(element);
			} 
		},
	});
</script>
@endpush
