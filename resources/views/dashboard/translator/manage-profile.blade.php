@extends('layouts.user')
@section('content')
@php 
$ucode = Request::segment(4) ?? Auth::user()->ucode;
$user_id = Request::segment(5) ?? Auth::id();
@endphp
<div id="popout" class="row">
  <div class="col s12">
    <!-- <h4 class="header">Skills</h4> -->
    <p>Manage the profile of {{ $profile->first_name }}</p>
  </div>
  <div class="col s12">
    <ul class="collapsible popout">
      <li class="active">
        <div class="collapsible-header" tabindex="0"><i class="material-icons">radio_button_checked</i>Personal Information</div>
        <div class="collapsible-body" style="display: block;">
          <span>
            {{ Form::open(array('url' => route('dashboard.translator.updateProfile'), 'files'=>'true', 'method'=>'post', 'enctype'=>'multipart/form-data', 'id' => 'personalInformation', 'name' => 'personalInformation')) }}
            @csrf
            <input type="hidden" name="form_type" value="personal_information">
            <input type="hidden" name="ucode" value="{{ $ucode }}">
            <input type="hidden" name="user_id" value="{{ $user_id }}">
            <!--  <div class="step-title waves-effect">Personal Information</div> -->
            <br/>
            <div class="step-content">
              <div class="row">
                <div class="input-field col m4 s12">
                  <label for="first_name">First Name<span class="red-text">*</span></label>
                  <input type="text" id=" first_name" name="first_name" value="{{ $profile->first_name }}">
                  <span class="error"><p id="first_name_error"></p></span>
                </div>
                <div class="input-field col m4 s12">
                  <label for="last_name">Last Name<span class="red-text">*</span></label>
                  <input type="text" id="last_name" name="last_name" value="{{ $profile->last_name }}">
                  <span class="error"><p id="last_name_error"></p></span>
                </div>
                <div class="input-field col m4 s12">
                  <label for="email">Email<span class="red-text">*</span></label>
                  <input type="email"  name="email" id="email" value="{{ Request::segment(4) ? $profile->user->email : Auth::user()->email }}"  disabled>
                  <span class="error"><p id="email_error"></p></span>
                </div>
              </div>
              <div class="row">
                <div class="input-field col m4 s12">
                  <select name="gender" id="gender">
                    <option value="Male" {{ $profile->gender == 'Male' ?? 'selected' }}>Male</option>
                    <option value="Female" {{ $profile->gender == 'Female' ?? 'selected' }}>Female</option>
                    <option value="Other" {{ $profile->gender == 'Other' ?? 'selected' }}>Other</option>
                  </select>
                  <label>Gender</label>
                  <span class="error"><p id="gender_error"></p></span>
                </div>
                <div class="input-field col m4 s12">
                  <label for="nationality">Nationality<span class="red-text">*</span></label>
                  <input type="text" id="nationality" name="nationality" value="{{ $profile->nationality }}">
                  <span class="error"><p id="nationality_error"></p></span>
                </div>
                <div class="input-field col m4 s12">
                  <select name="country" id="country">
                    <option value="{{ $profile->country ?? '' }}" readonly selected>{{ $profile->country ??  'Country' }}</option>
                    @foreach($countries as $country)
                    <option value="{{ $country->name ?? $country->name }}">{{ $country->name ?? $country->name }}</option>
                    @endforeach
                  </select>
                  <label>Country</label>
                  <span class="error"><p id="country_error"></p></span>
                </div>
                <div class="input-field col m4 s12">
                  <select name="country_phonecode_mobile" id="country_phonecode_mobile">
                    <option value="{{ $profile->country_phonecode_mobile ?? '' }}" readonly selected>{{ $profile->country_phonecode_mobile ??  'Phonecode Code' }}</option>
                    @foreach($countries as $country)
                    <option value="{{ $country->phonecode }}" {{ $profile->country_phonecode_mobile == $country->phonecode ?? selected   }} >{{ $country->phonecode }}</option>
                    @endforeach
                  </select>
                  <label>Phonecode Code</label>
                  <span class="error"><p id="country_phonecode_mobile_error"></p></span>
                </div>
                <div class="input-field col m4 s12">
                  <label for="mobile">Mobile: <span class="red-text">*</span></label>
                  <input type="text" id="mobile" value="{{ $profile->mobile ?? '' }}" name="mobile">
                  <span class="error"><p id="mobile_error"></p></span>
                </div>
                <div class="input-field col m4 s12">
                  <select name="country_phonecode_whatsapp" id="country_phonecode_whatsapp">
                    <option value="{{ $profile->country_phonecode_whatsapp ?? '' }}" readonly selected>{{ $profile->country_phonecode_whatsapp ?? 'Whatsapp Phonecode Code' }}</option>
                    @foreach($countries as $country)
                    <option value="{{ $country->phonecode }}" 
                      {{ $profile->country_phonecode_mobile == $country->phonecode ?? selected }}>
                      {{ $country->phonecode }}
                    </option>
                    @endforeach
                  </select>
                  <label>Whatsapp Phonecode Code</label>
                  <span class="error"><p id="country_phonecode_whatsapp_error"></p></span>
                </div>
                <div class="input-field col m4 s12">
                  <label for="whatsapp_mobile">WhatsApp Mobile No:</label>
                  <input type="text"  name="whatsapp_mobile" id="whatsapp_mobile" value="{{ $profile->whatsapp_mobile ?? '' }}">
                  <span class="error"><p id="whatsapp_mobile_error"></p></span>
                </div>
              </div>
              <!-- @include('common.wizbtns') -->
              <div class="step-actions">
                <div class="row" >
                  <div class="col m3 s12  mb-3  mt-1">
                    <button id="savePersonalInformationBtn" class="waves-effect waves-dark btn btn-lg btn-primary"
                    type="submit">Save</button>
                  </div>
                  <div class="col m9 s12  mb-2  mt-1" id="messagePersonalInformation"></div>
                </div>
              </div>
            </div>
          </form>
        </span>
      </div>
    </li>
    <li class="">
      <div class="collapsible-header" tabindex="0"><i class="material-icons">radio_button_checked</i>Language Proficiency</div>
      <div class="collapsible-body" style="">
        <span>
         {{ Form::open(array('url' => route('dashboard.translator.updateProfile'), 'files'=>'true', 'method'=>'post', 'enctype'=>'multipart/form-data', 'id' => 'skills-language-proficiency', 'name' => 'skills-language-proficiency')) }}
         @csrf
         <input type="hidden" name="form_type" value="skills-language-proficiency">
         <input type="hidden" name="ucode" value="{{ $ucode }}">
         <input type="hidden" name="user_id" value="{{ $user_id }}">
         <!--  <div class="step-title waves-effect">Personal Information</div> -->
         <br/>
         <div class="step-content">
          <div class="row">
            <table class="subscription-table responsive-table highlight">
              <thead>
                <tr>
                  <th class="custome-width" align="center"> Language Proficency</th>
                  <th class="custome-width" align="center">N/A</th>
                  <th class="custome-width" align="center">Elementory</th>
                  <th class="custome-width" align="center">Limited</th>
                  <th class="custome-width" align="center">Professsional Working</th>
                  <th class="custome-width" align="center">Full Professsional</th>
                  <th class="custome-width" align="center">Native/Bilingual</th>
                </tr>
              </thead>
              <tbody id="tableHeading">
                <div class="input-field col m12 s12">
                  <select id="languages" name="languages">
                    <option  value="" selected>---Choose One---</option>
                    <option id="Hindi" value="Hindi">Hindi</option>
                    <option id="Chinese" value="Chinese">Chinese</option>
                    <option id="Japanese" value="Japanese">Japanese</option>
                    <option id="Russian" value="Russian">Russian</option>
                    <option id="Spanish" value="Spanish">Spanish</option>
                    <option id="French" value="French">French</option>
                    <option id="German" value="German">German</option>
                  </select>
                  <label>Add More Language</label>
                  <span class="error"><p id="first_name_error"></p></span>
                </div>
                @if(App\LanguagePair::where('ucode', Request::segment(4) ?? Auth::user()->ucode)->first())
                <input type="hidden" id="language_count" name="language_count" value="{{ count($languages) ? count($languages) : 0  }}">
                <input type="hidden" name="record_flag" value="update_record">
                @php 
                $counter = 0;
                @endphp
                @foreach($languages as $language)
                <tr>
                  <td class="custom-width">{{ $language->name }}</td>
                  <td class="custom-width">
                    <p>
                      <label>
                        <input name="lang_{{ $counter }}" value="{{ $language->name }}_0" type="radio" {{ $language->proficiency == 0 ? 'checked' : '' }} >
                        <span></span>
                      </label>
                    </p>
                  </td>
                  <td class="custom-width">
                    <p>
                      <label>
                        <input name="lang_{{ $counter }}" value="{{ $language->name }}_1" type="radio" {{ $language->proficiency == 1 ? 'checked' : '' }} >
                        <span></span>
                      </label>
                    </p>
                  </td>
                  <td class="custom-width">
                    <p>
                      <label>
                        <input name="lang_{{ $counter }}" value="{{ $language->name }}_2" type="radio" {{ $language->proficiency == 2 ? 'checked' : '' }} >
                        <span></span>
                      </label>
                    </p>
                  </td>
                  <td class="custom-width">
                    <p>
                      <label>
                        <input name="lang_{{ $counter }}" value="{{ $language->name }}_3" type="radio" {{ $language->proficiency == 3 ? 'checked' : '' }} >
                        <span></span>
                      </label>
                    </p>
                  </td>
                  <td class="custom-width">
                    <p>
                      <label>
                        <input name="lang_{{ $counter }}" value="{{ $language->name }}_4" type="radio" {{ $language->proficiency == 4 ? 'checked' : '' }} >
                        <span></span>
                      </label>
                    </p>
                  </td>
                  <td class="custom-width">
                    <p>
                      <label>
                        <input name="lang_{{ $counter }}" value="{{ $language->name }}_5" type="radio" {{ $language->proficiency == 5 ? 'checked' : '' }} >
                        <span></span>
                      </label>
                    </p>
                  </td>
                  <th><span  class="error"><p id="lang_{{ $counter }}_error"></p></span></th>
                </tr>
                @php $counter++; @endphp
                @endforeach
                @else
                <input type="hidden" name="record_flag" value="insert_record">
                <input type="hidden" id="language_count" name="language_count" value="2">
                <tr>
                  <td class="custom-width">Arabic</td>
                  <td class="custom-width">
                    <p>
                      <label>
                        <input name="lang_0" value="Arabic_0" type="radio" checked  >
                        <span></span>
                      </label>
                    </p>
                  </td>
                  <td class="custom-width">
                    <p>
                      <label>
                        <input name="lang_0" value="Arabic_1" type="radio"  >
                        <span></span>
                      </label>
                    </p>
                  </td>
                  <td class="custom-width">
                    <p>
                      <label>
                        <input name="lang_0" value="Arabic_2" type="radio"  >
                        <span></span>
                      </label>
                    </p>
                  </td>
                  <td class="custom-width">
                    <p>
                      <label>
                        <input name="lang_0" value="Arabic_3" type="radio"  >
                        <span></span>
                      </label>
                    </p>
                  </td>
                  <td class="custom-width">
                    <p>
                      <label>
                        <input name="lang_0" value="Arabic_4" type="radio"  >
                        <span></span>
                      </label>
                    </p>
                  </td>
                  <td class="custom-width">
                    <p>
                      <label>
                        <input name="lang_0" value="Arabic_5" type="radio"  >
                        <span></span>
                      </label>
                    </p>
                  </td>
                  <th><span  class="error"><p id="lang_0_error"></p></span></th>
                </tr>
                <tr data-language="English">
                  <td class="custom-width">English</td>
                  <td class="custom-width">
                    <p>
                      <label>
                        <input name="lang_1" value="English_0" type="radio" checked >
                        <span></span>
                      </label>
                    </p>
                  </td>
                  <td class="custom-width">
                    <p>
                      <label>
                        <input name="lang_1" value="English_1" type="radio"  >
                        <span></span>
                      </label>
                    </p>
                  </td>
                  <td class="custom-width">
                    <p>
                      <label>
                        <input name="lang_1" value="English_2" type="radio"  >
                        <span></span>
                      </label>
                    </p>
                  </td>
                  <td class="custom-width">
                    <p>
                      <label>
                        <input name="lang_1" value="English_3" type="radio"  >
                        <span></span>
                      </label>
                    </p>
                  </td>
                  <td class="custom-width">
                    <p>
                      <label>
                        <input name="lang_1" value="English_4" type="radio"  >
                        <span></span>
                      </label>
                    </p>
                  </td>
                  <td class="custom-width">
                    <p>
                      <label>
                        <input name="lang_1" value="English_5" type="radio"  >
                        <span></span>
                      </label>
                    </p>
                  </td>
                  <th><span  class="error"><p id="lang_1_error"></p></span></th>

                </tr>

                @endif



              </tbody>
            </table>


            <!-- @include('common.wizbtns') -->
            <div class="step-actions">
              <div class="row" >



                <div class="col m3 s12  mb-3  mt-1">
                  <button id="saveSkillsLanguageProficiencyBtn" class="waves-effect waves-dark btn btn-lg btn-primary"
                  type="submit">Save</button>
                </div>
                <div class="col m9 s12  mb-2  mt-1" id="messageSkillsLanguageProficiency">

                </div>
              </div>
            </div>
          </div>
        </form>
      </span>
    </div>

  </li>
  <li class="">
    <div class="collapsible-header" tabindex="0"><i class="material-icons">radio_button_checked</i>Subject Matter Expertise</div>
    <div class="collapsible-body" style="">
     <span>
      {{ Form::open(array('url' => route('dashboard.translator.updateProfile'), 'files'=>'true', 'method'=>'post', 'enctype'=>'multipart/form-data', 'id' => 'skills-subject-matter-expertise', 'name' => 'skills-subject-matter-expertise')) }}
      @csrf
      <input type="hidden" name="form_type" value="skills-subject-matter-expertise">
      <input type="hidden" name="ucode" value="{{ Request::segment(4) ?? Auth::user()->ucode }}">
      <div class="step-content">

        <div class="row">





          <table class="subscription-table responsive-table highlight mb-5">
            <thead>
              <tr>
                <th class="custome-width" align="center"><strong>Expertise</strong></th>
                <th class="custome-width" align="center">1</th>
                <th class="custome-width" align="center">2</th>
                <th class="custome-width" align="center">3</th>
                <th class="custome-width" align="center">4</th>
                <th class="custome-width" align="center">5</th>
              </tr>
            </thead>

            <tbody id="tableHeading">
              @php
              $expertises = $profile->expertise ? json_decode($profile->expertise, true) : [];
              @endphp

              @forelse($expertises as $key => $val)

              <tr>
                <td class="custom-width">{{ ucwords(str_replace('_', ' / ', $key)) }}</td>
                <td class="custom-width">
                  <p>
                    <label>
                      <input name="{{ $key }}" value="1" type="radio" <?php if($val == 1){ echo 'checked'; }  ?> >
                      <span></span>
                    </label>
                  </p>
                </td>
                <td class="custom-width">
                  <p>
                    <label>
                      <input name="{{ $key }}" value="2" type="radio" <?php if($val  == 2){ echo 'checked'; }  ?> >
                      <span></span>
                    </label>
                  </p>
                </td>
                <td class="custom-width">
                  <p>
                    <label>
                      <input name="{{ $key }}" value="3" type="radio" <?php if($val == 3){ echo 'checked'; }  ?> >
                      <span></span>
                    </label>
                  </p>
                </td>
                <td class="custom-width">
                  <p>
                    <label>
                      <input name="{{ $key }}" value="4" type="radio" <?php if($val == 4){ echo 'checked'; }  ?> >
                      <span></span>
                    </label>
                  </p>
                </td>
                <td class="custom-width">
                  <p>
                    <label>
                      <input name="{{ $key }}" value="5" type="radio" <?php if($val == 5){ echo 'checked'; }  ?> >
                      <span></span>
                    </label>
                  </p>
                </td>
              </tr>
              @empty
              <tr>
                <td class="custom-width">Business</td>
                <td class="custom-width">
                  <p>
                    <label>
                      <input name="business" value="1" type="radio">
                      <span></span>
                    </label>
                  </p>
                </td>
                <td class="custom-width">
                  <p>
                    <label>
                      <input name="business" value="2" type="radio"  >
                      <span></span>
                    </label>
                  </p>
                </td>
                <td class="custom-width">
                  <p>
                    <label>
                      <input name="business" value="3" type="radio">
                      <span></span>
                    </label>
                  </p>
                </td>
                <td class="custom-width">
                  <p>
                    <label>
                      <input name="business" value="4" type="radio" >
                      <span></span>
                    </label>
                  </p>
                </td>
                <td class="custom-width">
                  <p>
                    <label>
                      <input name="business" value="5" type="radio" >
                      <span></span>
                    </label>
                  </p>
                </td>
              </tr>

              <tr>
                <td class="custom-width">Technology</td>
                <td class="custom-width">
                  <p>
                    <label>
                      <input name="technology" value="1" type="radio"  >
                      <span></span>
                    </label>
                  </p>
                </td>
                <td class="custom-width">
                  <p>
                    <label>
                      <input name="technology" value="2" type="radio"  >
                      <span></span>
                    </label>
                  </p>
                </td>
                <td class="custom-width">
                  <p>
                    <label>
                      <input name="technology" value="3" type="radio">
                      <span></span>
                    </label>
                  </p>
                </td>
                <td class="custom-width">
                  <p>
                    <label>
                      <input name="technology" value="4" type="radio" >
                      <span></span>
                    </label>
                  </p>
                </td>
                <td class="custom-width">
                  <p>
                    <label>
                      <input name="technology" value="5" type="radio" >
                      <span></span>
                    </label>
                  </p>
                </td>
              </tr>

              <tr>
                <td class="custom-width">Finance</td>
                <td class="custom-width">
                  <p>
                    <label>
                      <input name="finance" value="1" type="radio" >
                      <span></span>
                    </label>
                  </p>
                </td>
                <td class="custom-width">
                  <p>
                    <label>
                      <input name="finance" value="2" type="radio"  >
                      <span></span>
                    </label>
                  </p>
                </td>
                <td class="custom-width">
                  <p>
                    <label>
                      <input name="finance" value="3" type="radio">
                      <span></span>
                    </label>
                  </p>
                </td>
                <td class="custom-width">
                  <p>
                    <label>
                      <input name="finance" value="4" type="radio" >
                      <span></span>
                    </label>
                  </p>
                </td>
                <td class="custom-width">
                  <p>
                    <label>
                      <input name="finance" value="5" type="radio" >
                      <span></span>
                    </label>
                  </p>
                </td>
              </tr>

              <tr>
                <td class="custom-width">Medical</td>
                <td class="custom-width">
                  <p>
                    <label>
                      <input name="medical" value="1" type="radio" >
                      <span></span>
                    </label>
                  </p>
                </td>
                <td class="custom-width">
                  <p>
                    <label>
                      <input name="medical" value="2" type="radio"  >
                      <span></span>
                    </label>
                  </p>
                </td>
                <td class="custom-width">
                  <p>
                    <label>
                      <input name="medical" value="3" type="radio">
                      <span></span>
                    </label>
                  </p>
                </td>
                <td class="custom-width">
                  <p>
                    <label>
                      <input name="medical" value="4" type="radio" >
                      <span></span>
                    </label>
                  </p>
                </td>
                <td class="custom-width">
                  <p>
                    <label>
                      <input name="medical" value="5" type="radio" >
                      <span></span>
                    </label>
                  </p>
                </td>
              </tr>

              <tr>
                <td class="custom-width">PR / Media</td>
                <td class="custom-width">
                  <p>
                    <label>
                      <input name="PR_media" value="1" type="radio" >
                      <span></span>
                    </label>
                  </p>
                </td>
                <td class="custom-width">
                  <p>
                    <label>
                      <input name="PR_media" value="2" type="radio"  >
                      <span></span>
                    </label>
                  </p>
                </td>
                <td class="custom-width">
                  <p>
                    <label>
                      <input name="PR_media" value="3" type="radio">
                      <span></span>
                    </label>
                  </p>
                </td>
                <td class="custom-width">
                  <p>
                    <label>
                      <input name="PR_media" value="4" type="radio" >
                      <span></span>
                    </label>
                  </p>
                </td>
                <td class="custom-width">
                  <p>
                    <label>
                      <input name="PR_media" value="5" type="radio" >
                      <span></span>
                    </label>
                  </p>
                </td>
              </tr>

              <tr>
                <td class="custom-width">Marketing</td>
                <td class="custom-width">
                  <p>
                    <label>
                      <input name="marketing" value="1" type="radio" >
                      <span></span>
                    </label>
                  </p>
                </td>
                <td class="custom-width">
                  <p>
                    <label>
                      <input name="marketing" value="2" type="radio"  >
                      <span></span>
                    </label>
                  </p>
                </td>
                <td class="custom-width">
                  <p>
                    <label>
                      <input name="marketing" value="3" type="radio">
                      <span></span>
                    </label>
                  </p>
                </td>
                <td class="custom-width">
                  <p>
                    <label>
                      <input name="marketing" value="4" type="radio" >
                      <span></span>
                    </label>
                  </p>
                </td>
                <td class="custom-width">
                  <p>
                    <label>
                      <input name="marketing" value="5" type="radio" >
                      <span></span>
                    </label>
                  </p>
                </td>
              </tr>

              <tr>
                <td class="custom-width">Literature</td>
                <td class="custom-width">
                  <p>
                    <label>
                      <input name="literature" value="1" type="radio" >
                      <span></span>
                    </label>
                  </p>
                </td>
                <td class="custom-width">
                  <p>
                    <label>
                      <input name="literature" value="2" type="radio"  >
                      <span></span>
                    </label>
                  </p>
                </td>
                <td class="custom-width">
                  <p>
                    <label>
                      <input name="literature" value="3" type="radio">
                      <span></span>
                    </label>
                  </p>
                </td>
                <td class="custom-width">
                  <p>
                    <label>
                      <input name="literature" value="4" type="radio" >
                      <span></span>
                    </label>
                  </p>
                </td>
                <td class="custom-width">
                  <p>
                    <label>
                      <input name="literature" value="5" type="radio" >
                      <span></span>
                    </label>
                  </p>
                </td>
              </tr>

              <tr>
                <td class="custom-width">Educational</td>
                <td class="custom-width">
                  <p>
                    <label>
                      <input name="educational" value="1" type="radio" >
                      <span></span>
                    </label>
                  </p>
                </td>
                <td class="custom-width">
                  <p>
                    <label>
                      <input name="educational" value="2" type="radio"  >
                      <span></span>
                    </label>
                  </p>
                </td>
                <td class="custom-width">
                  <p>
                    <label>
                      <input name="educational" value="3" type="radio">
                      <span></span>
                    </label>
                  </p>
                </td>
                <td class="custom-width">
                  <p>
                    <label>
                      <input name="educational" value="4" type="radio" >
                      <span></span>
                    </label>
                  </p>
                </td>
                <td class="custom-width">
                  <p>
                    <label>
                      <input name="educational" value="5" type="radio" >
                      <span></span>
                    </label>
                  </p>
                </td>
              </tr>

              <tr>
                <td class="custom-width">App / Software</td>
                <td class="custom-width">
                  <p>
                    <label>
                      <input name="app_software" value="1" type="radio" >
                      <span></span>
                    </label>
                  </p>
                </td>
                <td class="custom-width">
                  <p>
                    <label>
                      <input name="app_software" value="2" type="radio"  >
                      <span></span>
                    </label>
                  </p>
                </td>
                <td class="custom-width">
                  <p>
                    <label>
                      <input name="app_software" value="3" type="radio">
                      <span></span>
                    </label>
                  </p>
                </td>
                <td class="custom-width">
                  <p>
                    <label>
                      <input name="app_software" value="4" type="radio" >
                      <span></span>
                    </label>
                  </p>
                </td>
                <td class="custom-width">
                  <p>
                    <label>
                      <input name="app_software" value="5" type="radio" >
                      <span></span>
                    </label>
                  </p>
                </td>
              </tr>

              <tr>
                <td class="custom-width">General / Research</td>
                <td class="custom-width">
                  <p>
                    <label>
                      <input name="general_research" value="1" type="radio" >
                      <span></span>
                    </label>
                  </p>
                </td>
                <td class="custom-width">
                  <p>
                    <label>
                      <input name="general_research" value="2" type="radio"  >
                      <span></span>
                    </label>
                  </p>
                </td>
                <td class="custom-width">
                  <p>
                    <label>
                      <input name="general_research" value="3" type="radio">
                      <span></span>
                    </label>
                  </p>
                </td>
                <td class="custom-width">
                  <p>
                    <label>
                      <input name="general_research" value="4" type="radio" >
                      <span></span>
                    </label>
                  </p>
                </td>
                <td class="custom-width">
                  <p>
                    <label>
                      <input name="general_research" value="5" type="radio" >
                      <span></span>
                    </label>
                  </p>
                </td>
              </tr>


              @endforelse
            </tbody>
          </table>



        </div>
        <!-- @include('common.wizbtns') -->
        <div class="step-actions">
          <div class="row" >
            <div class="col m3 s12  mb-3  mt-1">
              <button id="saveSkillsSubjectMatterExpertiseBtn" class="waves-effect waves-dark btn btn-lg btn-primary"
              type="submit">Save</button>
            </div>
            <div class="col m9 s12  mb-2  mt-1" id="messageSkillsSubjectMatterExpertise">
            </div>
          </div>
        </div>
      </div> 
    </form>
  </span>
</div>

</li>

<li class="">
  <div class="collapsible-header" tabindex="0"><i class="material-icons">radio_button_checked</i>Software And Tools</div>
  <div class="collapsible-body" style="">
   <span>
    {{ Form::open(array('url' => route('dashboard.translator.updateProfile'), 'files'=>'true', 'method'=>'post', 'enctype'=>'multipart/form-data', 'id' => 'skills-software-and-tools', 'name' => 'skills-software-and-tools')) }}
    @csrf
    <input type="hidden" name="form_type" value="skills-software-and-tools">
    <input type="hidden" name="ucode" value="{{ Request::segment(4) ?? Auth::user()->ucode }}">
    <div class="step-content">

      <div class="row">



        <div class="input-field col m3 s12">
          <div class="step-title waves-effect">CAT Tools </div>
          <p>
            <label>
              <input class="form-check-input" type="checkbox" name="cat_tool_MemoQ" id="cat_tool_MemoQ" value="{{ $profile->cat_tool_MemoQ ? 1 : 0 }}"   {{ $profile->cat_tool_MemoQ ? 'checked' : '' }}>
              <span>MemoQ</span>
            </label>
          </p>
          <p>
            <label>
              <input class="form-check-input" type="checkbox" name="cat_tool_sds_trados" id="cat_tool_sds_trados" value="{{ $profile->cat_tool_sds_trados ? 1 : 0 }}"   {{ $profile->cat_tool_sds_trados ? 'checked' : '' }}>
              <span>SDS Trados</span>
            </label>
          </p>
          <p>
            <label>
              <input class="form-check-input" type="checkbox" name="cat_tool_other" id="cat_tool_other" value="{{ $profile->cat_tool_other ? 1 : 0 }}"   {{ $profile->cat_tool_other ? 'checked' : '' }}>
              <span>Other</span>
            </label>
          </p>
        </div>

        <div class="input-field col m3 s12">
          <div class="step-title waves-effect">MS Office Tools </div>
          <p>
            <label>
              <input class="form-check-input" type="checkbox" name="ms_office_tool_powerPoint" id="ms_office_tool_powerPoint" value="{{ $profile->ms_office_tool_powerPoint ? 1 : 0 }}"   {{ $profile->ms_office_tool_powerPoint ? 'checked' : '' }}>
              <span>MS PowerPoint</span>
            </label>
          </p>
          <p>
            <label>
              <input class="form-check-input" type="checkbox" name="ms_office_tool_Word" id="ms_office_tool_Word" value="{{ $profile->ms_office_tool_Word ? 1 : 0 }}"   {{ $profile->ms_office_tool_Word ? 'checked' : '' }}>
              <span>MS Word</span>
            </label>
          </p>
          <p>
            <label>
              <input class="form-check-input" type="checkbox" name="ms_office_tool_Excel" id="ms_office_tool_Excel" value="{{ $profile->ms_office_tool_Excel ? 1 : 0 }}"   {{ $profile->ms_office_tool_Excel ? 'checked' : '' }}>
              <span>MS Excel</span>
            </label>
          </p>
          <p>
            <label>
              <input class="form-check-input" type="checkbox" name="ms_office_tool_Visio" id="ms_office_tool_Visio" value="{{ $profile->ms_office_tool_Visio ? 1 : 0 }}"   {{ $profile->ms_office_tool_Visio ? 'checked' : '' }}>
              <span>MS Visio</span>
            </label>
          </p>

        </div>



      </div>
      <!-- @include('common.wizbtns') -->
      <div class="step-actions">
        <div class="row" >
          <div class="col m3 s12  mb-3  mt-1">
            <button id="saveSkillsSoftwareAndToolsBtn" class="waves-effect waves-dark btn btn-lg btn-primary"
            type="submit">Save</button>
          </div>
          <div class="col m9 s12  mb-2  mt-1" id="messageSkillsSoftwareAndTools">
          </div>
        </div>
      </div>
    </div> 
  </form>
</span>
</div>

</li>



<li class="">
  <div class="collapsible-header" tabindex="0"><i class="material-icons">radio_button_checked</i>Identification Information</div>
  <div class="collapsible-body" style="">
   <span>
    {{ Form::open(array('url' => route('dashboard.translator.updateProfile'), 'files'=>'true', 'method'=>'post', 'enctype'=>'multipart/form-data', 'id' => 'pd-identification-information', 'name' => 'pd-identification-information')) }}
    @csrf
    <input type="hidden" name="form_type" value="pd-identification-information">
    <input type="hidden" name="ucode" value="{{ $ucode }}">
    <input type="hidden" name="user_id" value="{{ $user_id }}">
    <!--  <div class="step-title waves-effect">Personal Information</div> -->
    <br/>
    <div class="step-content">
      <div class="row">

        <div class="input-field col m3 s12">
          <select name="photo_id_type">
            <option value="National ID" {{ $profile->photo_id_type == 'National ID' ?? 'selected' }}>National ID</option>
            <option value="Passport" {{ $profile->photo_id_type == 'Passport' ?? 'selected' }}>Passport</option>
            <option value="Driver`s Licence" {{ $profile->photo_id_type == 'Driver`s Licence' ?? 'selected' }}>Driver's Licence</option>
            <option value="Other" {{ $profile->photo_id_type == 'Other' ?? 'selected' }}>Other</option>
          </select>
          <label>Photo ID Type</label>
          <span class="error"><p id="photo_id_type_error"></p></span>
        </div>

        <div class="file-field input-field col m8 s12">
          <div class="btn">
            <span>ADD</span>
            <input type="file" id="photo_id" name="photo_id" accept="image/jpeg" value="{{ $profile->photo_id ?? '' }}" class="validate">
          </div>
          <div class="file-path-wrapper">
            <input class="file-path validate" type="text" value="{{ $profile->bank_statement ?? '' }}">
            <input id="photo_id_hidden"  type="hidden" value="{{ $profile->photo_id ? 1 : 0 }}">
            <span class="error"><p id="photo_id_error"></p></span>
          </div>
        </div>
        <div class="file-field input-field col m1 s12">
          @if ($profile->photo_id)
          <img src="{{ asset('/storage/'.$profile->photo_id) }}" width="50" height="50">
          @endif
        </div>





        <div class="input-field col m3 s12">
          <select name="address_proof_type">
            <option value="Passport" {{ $profile->address_proof_type == 'Passport' ?? 'selected' }}>Passport</option>
            <option value="Rent Agreement" {{ $profile->address_proof_type == 'Rent Agreement' ?? 'selected' }}>Rent Agreement</option>
            <option value="Bank Document" {{ $profile->address_proof_type == 'Bank Document' ?? 'selected' }}>Bank Document</option>
            <option value="Other" {{ $profile->address_proof_type == 'Other' ?? 'selected' }}>Other</option>
          </select>
          <label>Address Proof Type</label>
          <span class="error"><p id="address_proof_type_error"></p></span>
        </div>


        <div class="file-field input-field col m8 s12">
          <div class="btn">
            <span>ADD</span>
            <input type="file" id="address_proof" name="address_proof" accept="image/jpeg" value="{{ $profile->address_proof ?? '' }}" class="validate">
          </div>
          <div class="file-path-wrapper">
            <input class="file-path validate" type="text" value="{{ $profile->address_proof ?? '' }}">
            <input id="address_proof_hidden"  type="hidden" value="{{ $profile->address_proof ? 1 : 0 }}">
            <span class="error"><p id="address_proof_error"></p></span>
          </div>
        </div>
        <div class="file-field input-field col m1 s12">
          @if ($profile->address_proof)
          <img src="{{ asset('/storage/'.$profile->address_proof) }}" width="50" height="50">
          @endif
        </div>


      </div>
      <!-- @include('common.wizbtns') -->
      <div class="step-actions">
        <div class="row" >



          <div class="col m3 s12  mb-3  mt-1">
            <button id="savePdIdentificationInformationBtn" class="waves-effect waves-dark btn btn-lg btn-primary"
            type="submit">Save</button>
          </div>
          <div class="col m9 s12  mb-2  mt-1" id="messagePdIdentificationInformation">

          </div>
        </div>
      </div>
    </div>
  </form>
</span>
</div>
</li>

<li class="">
  <div class="collapsible-header" tabindex="0"><i class="material-icons">radio_button_checked</i>Work History</div>
  <div class="collapsible-body" style="">
   <span>
    {{ Form::open(array('url' => route('dashboard.translator.updateProfile'), 'files'=>'true', 'method'=>'post', 'enctype'=>'multipart/form-data', 'id' => 'pd-work-history', 'name' => 'pd-work-history')) }}
    @csrf
    <input type="hidden" name="form_type" value="pd-work-history">
    <input type="hidden" name="ucode" value="{{ Request::segment(4) ?? Auth::user()->ucode }}">
    <div class="step-content">

      <div class="row">

        <div class="input-field col m3 s12">
          <select id="work_experience" name="work_experience">
            <option value="I am a Fresher" {{ $profile->work_experience == 'I am a Fresher' ? 'selected' : '' }}>I am a Fresher</option>
            <option value="Less than 1 Year" {{ $profile->work_experience == 'Less than 1 Year' ? 'selected' : '' }}>Less than 1 Year</option>
            <option value="Between 1-2 Years" {{ $profile->work_experience == 'Between 1-2 Years' ? 'selected' : '' }}>Between 1-2 Years</option>
            <option value="Between 2-3 Years" {{ $profile->work_experience == 'Between 2-3 Years' ? 'selected' : '' }}>Between 2-3 Years</option>
            <option value="Between 3-4 Years" {{ $profile->work_experience == 'Between 3-4 Years' ? 'selected' : '' }}>Between 3-4 Years</option>
            <option value="More than 4 Years" {{ $profile->work_experience == 'More than 4 Years' ? 'selected' : '' }}>More than 4 Years</option>
          </select>
          <label>Number of years(Work Experience)</label>
          <span class="error"><p id="work_experience_error"></p></span> 
        </div>

        <div class="input-field col m3 s12">
          <div class="step-title waves-effect">Type of experience </div>
          <p>
            <label>
              <input class="form-check-input" type="checkbox" name="translation" id="translation" value="{{ $profile->translation ? 1 : 0 }}"   {{ $profile->translation ? 'checked' : '' }}>
              <span>Translation</span>
            </label>
          </p>
          <p>
            <label>
              <input class="form-check-input" type="checkbox" name="proofreading" id="proofreading" value="{{ $profile->proofreading ? 1 : 0 }}"   {{ $profile->proofreading ? 'checked' : '' }}>
              <span>Proofreading</span>
            </label>
          </p>
          <p>
            <label>
              <input class="form-check-input" type="checkbox" name="quality_assurance" id="quality_assurance" value="{{ $profile->quality_assurance ? 1 : 0 }}"   {{ $profile->quality_assurance ? 'checked' : '' }}>
              <span>Quality Aassurance</span>
            </label>
          </p>
        </div>

        <div class="file-field input-field col m11 s12">
          <div class="btn">
            <span>Proof of Experience</span>
            <input type="file" id="proof_of_experience" name="proof_of_experience" accept="image/jpeg" value="{{ $profile->proof_of_experience ?? '' }}" class="validate">
          </div>
          <div class="file-path-wrapper">
            <input class="file-path validate" type="text" value="{{ $profile->proof_of_experience ?? '' }}">
            <input id="proof_of_experience_hidden"  type="hidden" value="{{ $profile->proof_of_experience ? 1 : 0 }}">
            <span class="error"><p id="proof_of_experience_error"></p></span>
          </div>
        </div>
        <div class="file-field input-field col m1 s12">
          @if ($profile->proof_of_experience)
          <img src="{{ asset('/storage/'.$profile->proof_of_experience) }}" width="50" height="50">
          @endif
        </div>






      </div>
      <!-- @include('common.wizbtns') -->
      <div class="step-actions">
        <div class="row" >
          <div class="col m3 s12  mb-3  mt-1">
            <button id="savePdWorkHistoryBtn" class="waves-effect waves-dark btn btn-lg btn-primary"
            type="submit">Save</button>
          </div>
          <div class="col m9 s12  mb-2  mt-1" id="messagePdWorkHistory">
          </div>
        </div>
      </div>
    </div> 
  </form>
</span>
</div>
</li>

<li class="">
  <div class="collapsible-header" tabindex="0"><i class="material-icons">radio_button_checked</i>Work Schedule</div>
  <div class="collapsible-body" style="">
   <span>
     {{ Form::open(array('url' => route('dashboard.translator.updateProfile'), 'files'=>'true', 'method'=>'post', 'enctype'=>'multipart/form-data', 'id' => 'pd-work-schedule', 'name' => 'pd-work-schedule')) }}
     @csrf
     <input type="hidden" name="form_type" value="pd-work-schedule">
     <input type="hidden" name="ucode" value="{{ Request::segment(4) ??Auth::user()->ucode }}">
     <div class="step-content">

      <div class="row">



        <table class="subscription-table responsive-table highlight mb-5">
          <thead>
            <tr>
              <th class="custome-width" align="center"><strong>Available Days</strong></th>
              <th class="custome-width" align="center">08:00 AM - 12:00 PM</th>
              <th class="custome-width" align="center">12:00 PM - 04:00 PM</th>
              <th class="custome-width" align="center">04:00 PM - 08:00 PM</th>
              <th class="custome-width" align="center">08:00 PM - 12:00 AM</th>
              <th class="custome-width" align="center">12:00 AM - 04:00 AM</th>
              <th class="custome-width" align="center">04:00 AM - 08:00 AM</th>
            </tr>
          </thead>
          @php $arrSunday = json_decode($profile->sunday); @endphp
          @php $arrMonday = json_decode($profile->monday); @endphp
          @php $arrTuesday = json_decode($profile->tuesday); @endphp
          @php $arrWednesday = json_decode($profile->wednesday); @endphp
          @php $arrThursday = json_decode($profile->thursday); @endphp
          @php $arrFriday = json_decode($profile->friday); @endphp
          @php $arrSaturday = json_decode($profile->saturday); @endphp
          <tbody id="tableHeading">
            <tr>
              <td class="custom-width">Sunday</td>
              @if($profile->sunday)
              @foreach($arrSunday as $key => $val)
              <td class="custom-width">
                <p>
                  <label>
                    <input name="{{ $key }}" value="{{ substr($key, -1) }}" {{ $val == 1 ? 'checked' : '' }}  type="checkbox">
                    <span></span>
                  </label>
                </p>
              </td>
              @endforeach
              @else 
              <td class="custom-width">
                <p>
                  <label>
                    <input name="sunday_check_1" value="1"  type="checkbox">
                    <span></span>

                  </label>
                </p>
              </td>
              <td class="custom-width">
                <p>
                  <label>
                    <input name="sunday_check_2" value="2"  type="checkbox"  >
                    <span></span>
                  </label>
                </p>
              </td>
              <td class="custom-width">
                <p>
                  <label>
                    <input name="sunday_check_3" value="3"  type="checkbox">
                    <span></span>
                  </label>
                </p>
              </td>
              <td class="custom-width">
                <p>
                  <label>
                    <input name="sunday_check_4" value="4" type="checkbox" >
                    <span></span>
                  </label>
                </p>
              </td>
              <td class="custom-width">
                <p>
                  <label>
                    <input name="sunday_check_5" value="5"  type="checkbox" >
                    <span></span>
                  </label>
                </p>
              </td>
              <td class="custom-width">
                <p>
                  <label>
                    <input name="sunday_check_6" value="6"  type="checkbox" >
                    <span></span>
                  </label>
                </p>
              </td>
              @endif
            </tr>



            <tr>
              <td class="custom-width">Monday</td>
              @if($profile->monday)
              @foreach($arrMonday as $key => $val)
              <td class="custom-width">
                <p>
                  <label>
                    <input name="{{ $key }}" value="{{ substr($key, -1) }}" {{ $val == 1 ? 'checked' : '' }}  type="checkbox">
                    <span></span>
                  </label>
                </p>
              </td>
              @endforeach
              @else 
              <td class="custom-width">
                <p>
                  <label>
                    <input name="monday_check_1" value="1"  type="checkbox">
                    <span></span>

                  </label>
                </p>
              </td>
              <td class="custom-width">
                <p>
                  <label>
                    <input name="monday_check_2" value="2"  type="checkbox"  >
                    <span></span>
                  </label>
                </p>
              </td>
              <td class="custom-width">
                <p>
                  <label>
                    <input name="monday_check_3" value="3"  type="checkbox">
                    <span></span>
                  </label>
                </p>
              </td>
              <td class="custom-width">
                <p>
                  <label>
                    <input name="monday_check_4" value="4" type="checkbox" >
                    <span></span>
                  </label>
                </p>
              </td>
              <td class="custom-width">
                <p>
                  <label>
                    <input name="monday_check_5" value="5"  type="checkbox" >
                    <span></span>
                  </label>
                </p>
              </td>
              <td class="custom-width">
                <p>
                  <label>
                    <input name="monday_check_6" value="6"  type="checkbox" >
                    <span></span>
                  </label>
                </p>
              </td>
              @endif
            </tr>




            <tr>
              <td class="custom-width">Tuesday</td>
              @if($profile->tuesday)
              @foreach($arrTuesday as $key => $val)
              <td class="custom-width">
                <p>
                  <label>
                    <input name="{{ $key }}" value="{{ substr($key, -1) }}" {{ $val == 1 ? 'checked' : '' }}  type="checkbox">
                    <span></span>
                  </label>
                </p>
              </td>
              @endforeach
              @else 
              <td class="custom-width">
                <p>
                  <label>
                    <input name="tuesday_check_1" value="1"  type="checkbox">
                    <span></span>

                  </label>
                </p>
              </td>
              <td class="custom-width">
                <p>
                  <label>
                    <input name="tuesday_check_2" value="2"  type="checkbox"  >
                    <span></span>
                  </label>
                </p>
              </td>
              <td class="custom-width">
                <p>
                  <label>
                    <input name="tuesday_check_3" value="3"  type="checkbox">
                    <span></span>
                  </label>
                </p>
              </td>
              <td class="custom-width">
                <p>
                  <label>
                    <input name="tuesday_check_4" value="4" type="checkbox" >
                    <span></span>
                  </label>
                </p>
              </td>
              <td class="custom-width">
                <p>
                  <label>
                    <input name="tuesday_check_5" value="5"  type="checkbox" >
                    <span></span>
                  </label>
                </p>
              </td>
              <td class="custom-width">
                <p>
                  <label>
                    <input name="tuesday_check_6" value="6"  type="checkbox" >
                    <span></span>
                  </label>
                </p>
              </td>
              @endif
            </tr>




            <tr>
              <td class="custom-width">Wednesday</td>
              @if($profile->wednesday)
              @foreach($arrWednesday as $key => $val)
              <td class="custom-width">
                <p>
                  <label>
                    <input name="{{ $key }}" value="{{ substr($key, -1) }}" {{ $val == 1 ? 'checked' : '' }}  type="checkbox">
                    <span></span>
                  </label>
                </p>
              </td>
              @endforeach
              @else 
              <td class="custom-width">
                <p>
                  <label>
                    <input name="wednesday_check_1" value="1"  type="checkbox">
                    <span></span>

                  </label>
                </p>
              </td>
              <td class="custom-width">
                <p>
                  <label>
                    <input name="wednesday_check_2" value="2"  type="checkbox"  >
                    <span></span>
                  </label>
                </p>
              </td>
              <td class="custom-width">
                <p>
                  <label>
                    <input name="wednesday_check_3" value="3"  type="checkbox">
                    <span></span>
                  </label>
                </p>
              </td>
              <td class="custom-width">
                <p>
                  <label>
                    <input name="wednesday_check_4" value="4" type="checkbox" >
                    <span></span>
                  </label>
                </p>
              </td>
              <td class="custom-width">
                <p>
                  <label>
                    <input name="wednesday_check_5" value="5"  type="checkbox" >
                    <span></span>
                  </label>
                </p>
              </td>
              <td class="custom-width">
                <p>
                  <label>
                    <input name="wednesday_check_6" value="6"  type="checkbox" >
                    <span></span>
                  </label>
                </p>
              </td>
              @endif
            </tr>



            <tr>
              <td class="custom-width">Thursday</td>
              @if($profile->thursday)
              @foreach($arrThursday as $key => $val)
              <td class="custom-width">
                <p>
                  <label>
                    <input name="{{ $key }}" value="{{ substr($key, -1) }}" {{ $val == 1 ? 'checked' : '' }}  type="checkbox">
                    <span></span>
                  </label>
                </p>
              </td>
              @endforeach
              @else 
              <td class="custom-width">
                <p>
                  <label>
                    <input name="thursday_check_1" value="1"  type="checkbox">
                    <span></span>

                  </label>
                </p>
              </td>
              <td class="custom-width">
                <p>
                  <label>
                    <input name="thursday_check_2" value="2"  type="checkbox"  >
                    <span></span>
                  </label>
                </p>
              </td>
              <td class="custom-width">
                <p>
                  <label>
                    <input name="thursday_check_3" value="3"  type="checkbox">
                    <span></span>
                  </label>
                </p>
              </td>
              <td class="custom-width">
                <p>
                  <label>
                    <input name="thursday_check_4" value="4" type="checkbox" >
                    <span></span>
                  </label>
                </p>
              </td>
              <td class="custom-width">
                <p>
                  <label>
                    <input name="thursday_check_5" value="5"  type="checkbox" >
                    <span></span>
                  </label>
                </p>
              </td>
              <td class="custom-width">
                <p>
                  <label>
                    <input name="thursday_check_6" value="6"  type="checkbox" >
                    <span></span>
                  </label>
                </p>
              </td>
              @endif
            </tr>



            <tr>
              <td class="custom-width">Friday</td>
              @if($profile->friday)
              @foreach($arrFriday as $key => $val)
              <td class="custom-width">
                <p>
                  <label>
                    <input name="{{ $key }}" value="{{ substr($key, -1) }}" {{ $val == 1 ? 'checked' : '' }}  type="checkbox">
                    <span></span>
                  </label>
                </p>
              </td>
              @endforeach
              @else 
              <td class="custom-width">
                <p>
                  <label>
                    <input name="friday_check_1" value="1"  type="checkbox">
                    <span></span>

                  </label>
                </p>
              </td>
              <td class="custom-width">
                <p>
                  <label>
                    <input name="friday_check_2" value="2"  type="checkbox"  >
                    <span></span>
                  </label>
                </p>
              </td>
              <td class="custom-width">
                <p>
                  <label>
                    <input name="friday_check_3" value="3"  type="checkbox">
                    <span></span>
                  </label>
                </p>
              </td>
              <td class="custom-width">
                <p>
                  <label>
                    <input name="friday_check_4" value="4" type="checkbox" >
                    <span></span>
                  </label>
                </p>
              </td>
              <td class="custom-width">
                <p>
                  <label>
                    <input name="friday_check_5" value="5"  type="checkbox" >
                    <span></span>
                  </label>
                </p>
              </td>
              <td class="custom-width">
                <p>
                  <label>
                    <input name="friday_check_6" value="6"  type="checkbox" >
                    <span></span>
                  </label>
                </p>
              </td>
              @endif
            </tr>


            <tr>
              <td class="custom-width">Saturday</td>
              @if($profile->saturday)
              @foreach($arrSaturday as $key => $val)
              <td class="custom-width">
                <p>
                  <label>
                    <input name="{{ $key }}" value="{{ substr($key, -1) }}" {{ $val == 1 ? 'checked' : '' }}  type="checkbox">
                    <span></span>
                  </label>
                </p>
              </td>
              @endforeach
              @else 
              <td class="custom-width">
                <p>
                  <label>
                    <input name="saturday_check_1" value="1"  type="checkbox">
                    <span></span>

                  </label>
                </p>
              </td>
              <td class="custom-width">
                <p>
                  <label>
                    <input name="saturday_check_2" value="2"  type="checkbox"  >
                    <span></span>
                  </label>
                </p>
              </td>
              <td class="custom-width">
                <p>
                  <label>
                    <input name="saturday_check_3" value="3"  type="checkbox">
                    <span></span>
                  </label>
                </p>
              </td>
              <td class="custom-width">
                <p>
                  <label>
                    <input name="saturday_check_4" value="4" type="checkbox" >
                    <span></span>
                  </label>
                </p>
              </td>
              <td class="custom-width">
                <p>
                  <label>
                    <input name="saturday_check_5" value="5"  type="checkbox" >
                    <span></span>
                  </label>
                </p>
              </td>
              <td class="custom-width">
                <p>
                  <label>
                    <input name="saturday_check_6" value="6"  type="checkbox" >
                    <span></span>
                  </label>
                </p>
              </td>
              @endif
            </tr>
          </tbody>
        </table>


      </div>
      <!-- @include('common.wizbtns') -->
      <div class="step-actions">
        <div class="row" >
          <div class="col m3 s12  mb-3  mt-1">
            <button id="savePdWorkScheduleBtn" class="waves-effect waves-dark btn btn-lg btn-primary"
            type="submit">Save</button>
          </div>
          <div class="col m9 s12  mb-2  mt-1" id="messagePdWorkSchedule">
          </div>
        </div>
      </div>
    </div> 
  </form>
</span>
</div>
</li>

<li class="">
  <div class="collapsible-header" tabindex="0"><i class="material-icons">radio_button_checked</i>Payment Method</div>
  <div class="collapsible-body" style="">
   <span>
    {{ Form::open(array('url' => route('dashboard.translator.updateProfile'), 'files'=>'true', 'method'=>'post', 'enctype'=>'multipart/form-data', 'id' => 'payment-method', 'name' => 'payment-method')) }}
    @csrf
    <input type="hidden" name="form_type" value="payment-method">
    <input type="hidden" name="ucode" value="{{ Request::segment(4)?: Auth::user()->ucode }}">
    <input type="hidden" name="bank_statement_path" value="{{ $profile->bank_statement ?? '' }}">
    <input type="hidden" name="cancelled_cheque_path" value="{{ $profile->cancelled_cheque ?? '' }}">
    <div class="step-content">

      <div class="row">

        <div class="input-field col m4 s12">
          <select name="bank_country" id="bank_country">
            <option value="{{ $profile->bank_country ?? '' }}" readonly selected>{{ $profile->bank_country ? $profile->bank_country : 'Bank Country' }}</option>

            @foreach($countries as $country)
            <option value="{{ $country->name ?? $country->name }}">{{ $country->name ?? $country->name }}</option>
            @endforeach
          </select>
          <label>Bank Country</label>
          <span class="error"><p id="bank_country_error"></p></span>
        </div>

        <div class="input-field col m4 s12">
          <label for="bank_name">Bank Name<span class="red-text">*</span></label>
          <input type="text" id="bank_name" name="bank_name" value="{{ $profile->bank_name ?? '' }}">
          <span class="error"><p id="bank_name_error"></p></span>
        </div>

        <div class="input-field col m4 s12">
          <label for="bank_branch_address">Bank Branch Address<span class="red-text">*</span></label>
          <input type="text" id="bank_branch_address" name="bank_branch_address" value="{{ $profile->bank_branch_address ?? '' }}">
          <span class="error"><p id="bank_branch_address_error"></p></span>
        </div>

        <div class="input-field col m4 s12">
          <label for="beneficiary_name">Beneficiary Name<span class="red-text">*</span></label>
          <input type="text" id="beneficiary_name" name="beneficiary_name" value="{{ $profile->beneficiary_name ?? '' }}">
          <span class="error"><p id="beneficiary_name_error"></p></span>
        </div>

        <div class="input-field col m4 s12">
          <label for="beneficiary_account_number">Beneficiary Account Number / IBAN<span class="red-text">*</span></label>
          <input type="text" id="beneficiary_account_number" name="beneficiary_account_number" value="{{ $profile->beneficiary_account_number ?? '' }}">
          <span class="error"><p id="beneficiary_account_number_error"></p></span>
        </div>

        <div class="input-field col m4 s12">
          <label for="ifsc_code">SWIFT/IFSC Code<span class="red-text">*</span></label>
          <input type="text" id="ifsc_code" name="ifsc_code" value="{{ $profile->ifsc_code ?? '' }}">
          <span class="error"><p id="ifsc_code_error"></p></span>
        </div>

        <div class="file-field input-field col m11 s12">
          <div class="btn">
            <span>Upload Bank Statement</span>
            <input type="file" id="bank_statement" name="bank_statement" accept="image/jpeg" value="{{ $profile->bank_statement ?? '' }}" class="validate">
          </div>
          <div class="file-path-wrapper">
            <input class="file-path validate" type="text" value="{{ $profile->bank_statement ?? '' }}">
            <input id="bank_statement_hidden"  type="hidden" value="{{ $profile->bank_statement ? 1 : 0 }}">
            <span class="error"><p id="bank_statement_error"></p></span>
          </div>
        </div>
        <div class="file-field input-field col m1 s12">
          @if ($profile->bank_statement)
          <img src="{{ asset('/storage/'.$profile->bank_statement) }}" width="50" height="50">
          @endif
        </div>

        <div class="file-field input-field col m11 s12">
          <div class="btn">
            <span>Upload Cancelled Cheque</span>
            <input type="file" id="cancelled_cheque" name="cancelled_cheque" accept="image/jpeg" value="{{ $profile->cancelled_cheque ?? '' }}" class="validate">
          </div>
          <div class="file-path-wrapper">
            <input class="file-path validate" type="text" value="{{ $profile->cancelled_cheque ?? '' }}">
            <input id="cancelled_cheque_hidden"  type="hidden" value="{{ $profile->cancelled_cheque ? 1 : 0 }}">
            <span class="error"><p id="cancelled_cheque_error"></p></span>
          </div>
        </div>
        <div class="file-field input-field col m1 s12">
          @if ($profile->cancelled_cheque)
          <img src="{{ asset('/storage/'.$profile->cancelled_cheque) }}" width="50" height="50">
          @endif
        </div>






        <div class="input-field col m12 s12">
          <label for="paypal_email">Alternate Payment Method (Paypal Email ID)</label>
          <input type="text" id="paypal_email" name="paypal_email" value="{{ $profile->paypal_email ?? '' }}">
          <span class="error"><p id="paypal_email_error"></p></span>
        </div>


      </div>
      <!-- @include('common.wizbtns') -->
      <div class="step-actions">
        <div class="row" >
          <div class="col m3 s12  mb-3  mt-1">
            <button id="savePaymentMethodBtn" class="waves-effect waves-dark btn btn-lg btn-primary"
            type="submit">Save</button>
          </div>
          <div class="col m9 s12  mb-2  mt-1" id="messagePaymentMethod">
          </div>
        </div>
      </div>
    </div> 
  </form>
</span>
</div>
</li>

</ul>



</div>
</div>
</p>
</div>
</form>

</div>
</div>
@endsection
@push('css')
<style>
  .red{
    color:red;
  }
  .custom-width{
    width:15%;
  }
</style>
@endpush

@push('js')
<script type="text/javascript">
  $(() => {
    var token = document.head.querySelector('meta[name="csrf-token"]');
    const config = {
      headers: { 
        'X-CSRF-TOKEN': token.content,
        'content-type': 'multipart/form-data'
      }
    }

    document.getElementById('personalInformation').addEventListener('submit', function(e){
      e.preventDefault();
      /* Form Validation Start Here... */
      var names = ['first_name', 'last_name', 'gender', 'nationality', 'country', 'country_phonecode_mobile', 'mobile', 'country_phonecode_whatsapp', 'whatsapp_mobile'];
      var errorCount = 0;
      names.forEach((el) => {
        var val = document.forms["personalInformation"][el].value;
        if (val == null || val == "" || val == 0) {
          document.getElementById(el + '_error').style = 'color:red';
          document.getElementById(el + '_error').focus();
          document.getElementById(el + '_error').textContent = 'Required!!!';
          ++errorCount;
        }else{
          document.getElementById(el + '_error').textContent = '';
        }
      });
      if (errorCount) return false;
      /* Form Validation End Here... */
      const form = document.querySelector('#personalInformation');
      var formData = new FormData(form);
      axios.post(bURL+'dashboard/translator/update-profile', formData, config)
      .then(function (res) {
        console.log(res.data.message);
        if(res.data.message === true){
          var msg = `<div class="card-alert card  lighten-5">
          <div class="card-content green-text">
          <p>Success! Profile data saved successfuly</p>
          </div>
          </div>`;      
        }else{
          var msg = `<div class="card-alert card  lighten-5">
          <div class="card-content red-text">
          <p>Error! Have problem regarding save profile data</p>
          </div>
          </div>`;
        }
        $('#messagePersonalInformation').html(msg);
        $('#messagePersonalInformation').empty().show().html(msg).delay(3000).fadeOut(300);
      })
      .catch(function (err) {
        console.log(err);            
      });          
    });


    document.getElementById('skills-language-proficiency').addEventListener('submit', function(e){
      e.preventDefault();
      /* Form Validation Start Here... */
      // var names = ['first_name', 'last_name', 'gender', 'nationality', 'country', 'country_phonecode_mobile', 'mobile', 'country_phonecode_whatsapp', 'whatsapp_mobile'];
      // var errorCount = 0;
      // names.forEach((el) => {
      //   var val = document.forms["skills"][el].value;
      //   if (val == null || val == "" || val == 0) {
      //     document.getElementById(el + '_error').style = 'color:red';
      //     document.getElementById(el + '_error').focus();
      //     document.getElementById(el + '_error').textContent = 'Required!!!';
      //     ++errorCount;
      //   }else{
      //     document.getElementById(el + '_error').textContent = '';
      //   }
      // });
      // if (errorCount) return false;
      /* Form Validation End Here... */
      const form = document.querySelector('#skills-language-proficiency');
      var formData = new FormData(form);
      axios.post(bURL+'dashboard/translator/update-profile', formData, config)
      .then(function (res) {
        console.log(res.data.message);
        if(res.data.message === true){
          var msg = `<div class="card-alert card  lighten-5">
          <div class="card-content green-text">
          <p>Success! Language proficiency data saved successfuly</p>
          </div>
          </div>`;      
        }else{
          var msg = `<div class="card-alert card  lighten-5">
          <div class="card-content red-text">
          <p>Error! Have problem regarding save language proficiency</p>
          </div>
          </div>`;
        }
        $('#messageSkillsLanguageProficiency').html(msg);
        $('#messageSkillsLanguageProficiency').empty().show().html(msg).delay(3000).fadeOut(300);
      })
      .catch(function (err) {
        console.log(err);            
      });          
    });


    document.getElementById('skills-subject-matter-expertise').addEventListener('submit', function(e){
      e.preventDefault();
      /* Form Validation Start Here... */
      // var names = ['first_name', 'last_name', 'gender', 'nationality', 'country', 'country_phonecode_mobile', 'mobile', 'country_phonecode_whatsapp', 'whatsapp_mobile'];
      // var errorCount = 0;
      // names.forEach((el) => {
      //   var val = document.forms["skills"][el].value;
      //   if (val == null || val == "" || val == 0) {
      //     document.getElementById(el + '_error').style = 'color:red';
      //     document.getElementById(el + '_error').focus();
      //     document.getElementById(el + '_error').textContent = 'Required!!!';
      //     ++errorCount;
      //   }else{
      //     document.getElementById(el + '_error').textContent = '';
      //   }
      // });
      // if (errorCount) return false;
      /* Form Validation End Here... */
      const form = document.querySelector('#skills-subject-matter-expertise');
      var formData = new FormData(form);
      axios.post(bURL+'dashboard/translator/update-profile', formData, config)
      .then(function (res) {
        console.log(res.data);
        if(res.data.message === true){
          var msg = `<div class="card-alert card  lighten-5">
          <div class="card-content green-text">
          <p>Success! Subject matter expertise data saved successfuly</p>
          </div>
          </div>`;      
        }else{
          var msg = `<div class="card-alert card  lighten-5">
          <div class="card-content red-text">
          <p>Error! Have problem regarding save subject matter expertise</p>
          </div>
          </div>`;
        }
        $('#messageSkillsSubjectMatterExpertise').html(msg);
        $('#messageSkillsSubjectMatterExpertise').empty().show().html(msg).delay(3000).fadeOut(300);
      })
      .catch(function (err) {
        console.log(err);            
      });          
    });


    document.getElementById('skills-software-and-tools').addEventListener('submit', function(e){
      e.preventDefault();
      /* Form Validation Start Here... */
      // var names = ['first_name', 'last_name', 'gender', 'nationality', 'country', 'country_phonecode_mobile', 'mobile', 'country_phonecode_whatsapp', 'whatsapp_mobile'];
      // var errorCount = 0;
      // names.forEach((el) => {
      //   var val = document.forms["skills"][el].value;
      //   if (val == null || val == "" || val == 0) {
      //     document.getElementById(el + '_error').style = 'color:red';
      //     document.getElementById(el + '_error').focus();
      //     document.getElementById(el + '_error').textContent = 'Required!!!';
      //     ++errorCount;
      //   }else{
      //     document.getElementById(el + '_error').textContent = '';
      //   }
      // });
      // if (errorCount) return false;
      /* Form Validation End Here... */
      const form = document.querySelector('#skills-software-and-tools');
      var formData = new FormData(form);
      axios.post(bURL+'dashboard/translator/update-profile', formData, config)
      .then(function (res) {
        console.log(res.data.message);
        if(res.data.message === true){
          var msg = `<div class="card-alert card  lighten-5">
          <div class="card-content green-text">
          <p>Success! Software and tools saved successfuly</p>
          </div>
          </div>`;      
        }else{
          var msg = `<div class="card-alert card  lighten-5">
          <div class="card-content red-text">
          <p>Error! Have problem regarding save software and tools</p>
          </div>
          </div>`;
        }
        $('#messageSkillsSoftwareAndTools').html(msg);
        $('#messageSkillsSoftwareAndTools').empty().show().html(msg).delay(3000).fadeOut(300);
      })
      .catch(function (err) {
        console.log(err);            
      });          
    });

    document.getElementById('pd-identification-information').addEventListener('submit', function(e){
      e.preventDefault();
      /* Form Validation Start Here... */
      var names = ['photo_id_type', 'photo_id', 'address_proof_type', 'address_proof'];

      if($('#photo_id_hidden').val() == 1 && $('#address_proof_hidden').val() == 1){
        var names = ['photo_id_type', 'address_proof_type'];
      }else if($('#photo_id_hidden').val() == 1 && $('#address_proof_hidden').val() == 0){
        var names = ['photo_id_type', 'address_proof_type', 'address_proof'];
      }else if($('#photo_id_hidden').val() == 0 && $('#address_proof_hidden').val() == 1){
        var names = ['photo_id_type', 'photo_id', 'address_proof_type'];
      }else{
        var names = ['photo_id_type', 'photo_id', 'address_proof_type', 'address_proof'];
      }

      var errorCount = 0;
      names.forEach((el) => {
        var val = document.forms["pd-identification-information"][el].value;
        if (val == null || val == "") {
          document.getElementById(el + '_error').style = 'color:red';
          document.getElementById(el + '_error').focus();
          document.getElementById(el + '_error').textContent = 'Required!!!';
          ++errorCount;
        }else{
          document.getElementById(el + '_error').textContent = '';
        }
      });
      if (errorCount) return false;
      /* Form Validation End Here... */
      const form = document.querySelector('#pd-identification-information');
      var formData = new FormData(form);
      axios.post(bURL+'dashboard/translator/update-profile', formData, config)
      .then(function (res) {
        console.log(res.data.message);
        if(res.data.message === true){
          var msg = `<div class="card-alert card  lighten-5">
          <div class="card-content green-text">
          <p>Success! Language proficiency data saved successfuly</p>
          </div>
          </div>`;      
        }else{
          var msg = `<div class="card-alert card  lighten-5">
          <div class="card-content red-text">
          <p>Error! Have problem regarding save language proficiency</p>
          </div>
          </div>`;
        }
        $('#messagePdIdentificationInformation').html(msg);
        $('#messagePdIdentificationInformation').empty().show().html(msg).delay(3000).fadeOut(300);
      })
      .catch(function (err) {
        console.log(err);            
      });          
    });


    document.getElementById('pd-work-history').addEventListener('submit', function(e){
      e.preventDefault();
      /* Form Validation Start Here... */
      var names = ['work_experience'];
      var errorCount = 0;
      names.forEach((el) => {
        var val = document.forms["pd-work-history"][el].value;
        if (val == null || val == "" || val == 0) {
          document.getElementById(el + '_error').style = 'color:red';
          document.getElementById(el + '_error').focus();
          document.getElementById(el + '_error').textContent = 'Required!!!';
          ++errorCount;
        }else{
          document.getElementById(el + '_error').textContent = '';
        }
      });
      if (errorCount) return false;
      /* Form Validation End Here... */
      const form = document.querySelector('#pd-work-history');
      var formData = new FormData(form);
      axios.post(bURL+'dashboard/translator/update-profile', formData, config)
      .then(function (res) {
        console.log(res.data);
        if(res.data.message === true){
          var msg = `<div class="card-alert card  lighten-5">
          <div class="card-content green-text">
          <p>Success! Work history data saved successfuly</p>
          </div>
          </div>`;      
        }else{
          var msg = `<div class="card-alert card  lighten-5">
          <div class="card-content red-text">
          <p>Error! Have problem regarding save work history</p>
          </div>
          </div>`;
        }
        $('#messagePdWorkHistory').html(msg);
        $('#messagePdWorkHistory').empty().show().html(msg).delay(3000).fadeOut(300);
      })
      .catch(function (err) {
        console.log(err);            
      });          
    });


    document.getElementById('pd-work-schedule').addEventListener('submit', function(e){
      e.preventDefault();
      /* Form Validation Start Here... */
      // var names = ['first_name', 'last_name', 'gender', 'nationality', 'country', 'country_phonecode_mobile', 'mobile', 'country_phonecode_whatsapp', 'whatsapp_mobile'];
      // var errorCount = 0;
      // names.forEach((el) => {
      //   var val = document.forms["skills"][el].value;
      //   if (val == null || val == "" || val == 0) {
      //     document.getElementById(el + '_error').style = 'color:red';
      //     document.getElementById(el + '_error').focus();
      //     document.getElementById(el + '_error').textContent = 'Required!!!';
      //     ++errorCount;
      //   }else{
      //     document.getElementById(el + '_error').textContent = '';
      //   }
      // });
      // if (errorCount) return false;
      /* Form Validation End Here... */
      const form = document.querySelector('#pd-work-schedule');
      var formData = new FormData(form);
      axios.post(bURL+'dashboard/translator/update-profile', formData, config)
      .then(function (res) {
        console.log(res.data.message);
        if(res.data.message === true){
          var msg = `<div class="card-alert card  lighten-5">
          <div class="card-content green-text">
          <p>Success! Your availability saved successfuly</p>
          </div>
          </div>`;      
        }else{
          var msg = `<div class="card-alert card  lighten-5">
          <div class="card-content red-text">
          <p>Error! Have problem regarding save your availability</p>
          </div>
          </div>`;
        }
        $('#messagePdWorkSchedule').html(msg);
        $('#messagePdWorkSchedule').empty().show().html(msg).delay(3000).fadeOut(300);
      })
      .catch(function (err) {
        console.log(err);            
      });          
    });

    document.getElementById('payment-method').addEventListener('submit', function(e){
      e.preventDefault();
      /* Form Validation Start Here... */
      if($('#bank_statement_hidden').val() == 1 && $('#cancelled_cheque_hidden').val() == 1){
        var names = ['bank_country', 'bank_name', 'bank_branch_address', 'beneficiary_name', 'beneficiary_account_number', 'ifsc_code'];
      }else if($('#bank_statement_hidden').val() == 1 && $('#cancelled_cheque_hidden').val() == 0){
        var names = ['bank_country', 'bank_name', 'bank_branch_address', 'beneficiary_name', 'beneficiary_account_number', 'ifsc_code', 'cancelled_cheque',];
      }else if($('#bank_statement_hidden').val() == 0 && $('#cancelled_cheque_hidden').val() == 1){
        var names = ['bank_country', 'bank_name', 'bank_branch_address', 'beneficiary_name', 'beneficiary_account_number', 'ifsc_code', 'bank_statement'];
      }else{
        var names = ['bank_country', 'bank_name', 'bank_branch_address', 'beneficiary_name', 'beneficiary_account_number', 'ifsc_code', 'bank_statement', 'cancelled_cheque'];
      }
      var errorCount = 0;
      names.forEach((el) => {
        var val = document.forms["payment-method"][el].value;
        if (val == null || val == "") {
          document.getElementById(el + '_error').style = 'color:red';
          document.getElementById(el + '_error').focus();
          document.getElementById(el + '_error').textContent = 'Required!!!';
          ++errorCount;
        }else{
          document.getElementById(el + '_error').textContent = '';
        }
      });
      if (errorCount) return true;
      /* Form Validation End Here... */
      const form = document.querySelector('#payment-method');
      var formData = new FormData(form);
      axios.post(bURL+'dashboard/translator/update-profile', formData, config)
      .then(function (res) {
        console.log(res.data.message);
        if(res.data.message === true){
          var msg = `<div class="card-alert card  lighten-5">
          <div class="card-content green-text">
          <p>Success! Profile data saved successfuly</p>
          </div>
          </div>`;      
        }else{
          var msg = `<div class="card-alert card  lighten-5">
          <div class="card-content red-text">
          <p>Error! Have problem regarding save profile data</p>
          </div>
          </div>`;
        }
        $('#messagePaymentMethod').html(msg);
        $('#messagePaymentMethod').empty().show().html(msg).delay(3000).fadeOut(300);
      })
      .catch(function (err) {
        console.log(err);            
      });          
    });

    $('#languages').on('change', () => {
      var myStr = $('#languages').val();
      var language_count = $('#language_count').val();
        // localStorage.setItem('language_count', language_count);
        // language_count = localStorage.getItem('language_count')
        language_count++;   
        $('#language_count').val(language_count);
        
        var lang_val = language_count - 1;
        //alert(lang_val);
        
        $('#tableHeading').append(`
          <tr data-language="`+myStr+`">
          <td class="custom-width">`+myStr+`</td>
          <td class="custom-width">
          <p>
          <label>
          <input name="lang_`+lang_val+`" value="`+myStr+`_0" type="radio">
          <span></span>
          </label>
          </p>
          </td>
          <td class="custom-width">
          <p>
          <label>
          <input name="lang_`+lang_val+`" value="`+myStr+`_1" type="radio" >
          <span></span>
          </label>
          </p>
          </td>
          <td class="custom-width">
          <p>
          <label>
          <input name="lang_`+lang_val+`" value="`+myStr+`_2" type="radio" >
          <span></span>
          </label>
          </p>
          </td>
          <td class="custom-width">
          <p>
          <label>
          <input name="lang_`+lang_val+`" value="`+myStr+`_3" type="radio" >
          <span></span>
          </label>
          </p>
          </td>
          <td class="custom-width">
          <p>
          <label>
          <input name="lang_`+lang_val+`" value="`+myStr+`_4" type="radio" >
          <span></span>
          </label>
          </p>
          </td>
          <td class="custom-width">
          <p>
          <label>
          <input name="lang_`+lang_val+`" value="`+myStr+`_5" type="radio" >
          <span></span>
          </label>
          </p>
          </td>
          </tr>
          `);
      });

  });
</script>
@endpush      