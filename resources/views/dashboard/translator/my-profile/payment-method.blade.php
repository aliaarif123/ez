@extends('layouts.user')
@section('content')
@php 
$ucode = Request::segment(5) ?? Auth::user()->ucode;
$user_id = Request::segment(6) ?? Auth::id();
$profile = App\Profile::where('ucode', $ucode)->first();
$banking_information_flag = ' &nbsp; <span id="banking_information_flag" style="color:red"> ( Information
to be filled )</span>';
$alternate_payment_flag = ' &nbsp; <span id="alternate_payment_flag" style="color:red"> ( Information
to be filled )</span>';

@endphp
<div id="popout" class="row">
	<div class="col s12">
		<p>Manage your payment information</p>
	</div>
	<div class="col s12">
		<ul class="collapsible popout">
			<li id="li1">
				<div class="collapsible-header" tabindex="0"><i class="material-icons">radio_button_checked</i>Banking information {!! $banking_information_flag !!}</div>
				<div class="collapsible-body" id="li1child">
					<span>
						{{ Form::open(array('url' => route('dashboard.translator.updateProfile'), 'files'=>'true', 'method'=>'post', 'enctype'=>'multipart/form-data', 'id' => 'pm-banking-information', 'name' => 'pm-banking-information')) }}
						@csrf
						<input type="hidden" name="form_type" value="pm-banking-information" >
						<input type="hidden" name="ucode" value="{{ $ucode }}">
						<input type="hidden" name="user_id" value="{{ $user_id }}">
						<br/>
						<div class="step-content">
							<div class="row">

								<div class="input-field col m4 s12">
									<label for="bank_name">Bank Name<span class="red-text">*</span></label>
									<input type="text" id="bank_name" name="bank_name" value="{{ $profile->bank_name ?? '' }}" @if(Auth::user()->profile->saveBtnStatus == 0) readonly @endif>
									<span class="error"><p id="bank_name_error"></p></span>
								</div>



								<div class="input-field col m4 s12">
									<label for="beneficiary_name">Beneficiary Name<span class="red-text">*</span></label>
									<input type="text" id="beneficiary_name" name="beneficiary_name" value="{{ $profile->beneficiary_name ?? '' }}" @if(Auth::user()->profile->saveBtnStatus == 0) readonly @endif>
									<span class="error"><p id="beneficiary_name_error"></p></span>
								</div>

								<div class="input-field col m4 s12">
									<label for="beneficiary_account_number">Beneficiary Account Number / IBAN<span class="red-text">*</span></label>
									<input type="text" id="beneficiary_account_number" name="beneficiary_account_number" value="{{ $profile->beneficiary_account_number ?? '' }}" @if(Auth::user()->profile->saveBtnStatus == 0) readonly @endif>
									<span class="error"><p id="beneficiary_account_number_error"></p></span>
								</div>
							</div>

							<div class="row">
								<div class="input-field col m4 s12">
									<label for="ifsc_code">SWIFT/IFSC Code<span class="red-text">*</span></label>
									<input type="text" id="ifsc_code" name="ifsc_code" value="{{ $profile->ifsc_code ?? '' }}" @if(Auth::user()->profile->saveBtnStatus == 0) readonly @endif>
									<span class="error"><p id="ifsc_code_error"></p></span>
								</div>

								<div class="input-field col m8 s12">
									<label for="bank_branch_address">Bank Branch Address<span class="red-text">*</span></label>
									<input type="text" id="bank_branch_address" name="bank_branch_address" value="{{ $profile->bank_branch_address ?? '' }}" @if(Auth::user()->profile->saveBtnStatus == 0) readonly @endif>
									<span class="error"><p id="bank_branch_address_error"></p></span>
								</div>

							</div>

							<div class="row">

								<div class="input-field col m4 s12">
									<select id="proof_of_bank_type" name="proof_of_bank_type" @if(Auth::user()->profile->saveBtnStatus == 0) readonly @endif>
										<option value="Cancelled cheque" {{ $profile->proof_of_bank_type == 'Cancelled cheque' ?? 'selected' }}>Cancelled cheque</option>
										<option value="Bank Letter" {{ $profile->proof_of_bank_type == 'Bank Letter' ?? 'selected' }}>Bank Letter</option>
										<option value="Bank statement
										(Latest)" {{ $profile->proof_of_bank_type == 'Bank statement
										(Latest)' ?? 'selected' }}>Bank statement
									(Latest)</option>
								</select>
								<label>Proof of Bank Type</label>
								<span class="error"><p id="proof_of_bank_type_error"></p></span>
							</div>

							<div class="file-field input-field col m7 s12">
								<div class="btn">
									<span>ADD</span>
									<input type="file" id="proof_of_bank" name="proof_of_bank" accept="image/jpeg" value="{{ $profile->proof_of_bank ?? '' }}" class="validate" @if(Auth::user()->profile->saveBtnStatus == 0) readonly @endif>
								</div>


								<div class="file-path-wrapper">
									<input class="file-path validate" type="text" value="{{ $profile->proof_of_bank ?? '' }}" @if(Auth::user()->profile->saveBtnStatus == 0) readonly @endif>
									<input id="proof_of_bank_hidden"  type="hidden" value="{{ $profile->proof_of_bank ? 1 : 0 }}" @if(Auth::user()->profile->saveBtnStatus == 0) readonly @endif>
									<span class="error"><p id="proof_of_bank_error"></p></span>
								</div>
							</div>
							<div class="file-field input-field col m1 s12">
								@if ($profile->proof_of_bank)
								<img src="{{ asset('/storage/'.$profile->proof_of_bank) }}" width="50" height="50">
								@endif
							</div>
							<!-- @include('common.wizbtns') -->
							<div class="step-actions">
								<div class="row" >
									<div class="col m3 s12  mb-3  mt-1">
										@if(Auth::user()->profile->saveBtnStatus == 1) 
										<button id="savePMBankingInformationBtn" class="waves-effect waves-dark btn btn-lg btn-primary"
										type="submit">Save</button>
										<button  class="waves-effect waves-dark btn btn-lg btn-primary customHidden" id="next1" type="button" >Next</button>
										@else
										<button  
										class="waves-effect waves-dark btn btn-lg btn-primary" type="button"
										onclick="window.location.href=`{{ route('dashboard.translator.getHelp') }}`"> Get Help </button>
										@endif
									</div>
									<div class="col m9 s12  mb-2  mt-1" id="messagePMBankingInformation"></div>
								</div>
							</div>
						</div>
					</div>
				</form>
			</span>
		</div>
	</li>

	<li id="li2">
		<div class="collapsible-header" tabindex="0"><i class="material-icons">radio_button_checked</i>Alternate Payment
		Method{!! $alternate_payment_flag !!}</div>
		<div class="collapsible-body" id="li2child">
			<span>
				{{ Form::open(array('url' => route('dashboard.translator.updateProfile'), 'files'=>'true', 'method'=>'post', 'enctype'=>'multipart/form-data', 'id' => 'pm-alternate-payment', 'name' => 'pm-alternate-payment')) }}
				@csrf
				<input type="hidden" name="form_type" value="pm-alternate-payment">
				<input type="hidden" name="ucode" value="{{ Request::segment(5) ?? Auth::user()->ucode }}">
				<div class="step-content">
					<div class="row">
						<div class="input-field col m12 s12">
							<label for="paypal_email">Alternate Payment Method (Paypal Email ID)</label>
							<input type="text" id="paypal_email" name="paypal_email" value="{{ $profile->paypal_email ?? '' }}" @if(Auth::user()->profile->saveBtnStatus == 0) readonly @endif>
							<span class="error"><p id="paypal_email_error"></p></span>
						</div>
					</div>
					<!-- @include('common.wizbtns') -->
					<div class="step-actions">
						<div class="row" >
							<div class="col m3 s12  mb-3  mt-1">

								@if(Auth::user()->profile->saveBtnStatus == 1) 
								<button id="savePMAlternatePaymentBtn" class="waves-effect waves-dark btn btn-lg btn-primary"
								type="submit">Save</button>
								@else
								<button  
								class="waves-effect waves-dark btn btn-lg btn-primary" type="button"
								onclick="window.location.href=`{{ route('dashboard.translator.getHelp') }}`"> Get Help </button>
								@endif
							</div>
							<div class="col m9 s12  mb-2  mt-1" id="messagePMAlternatePayment">
							</div>
						</div>
					</div>
				</div>
			</form>
		</span>
	</div>
</li>
</ul>
</div>
</div>
@endsection
@push('css')
<style>
	.red{
		color:red;
	}
	.custom-width{
		width:15%;
	}
</style>
@endpush

@push('js')
<script type="text/javascript">
	$(() => {
		var token = document.head.querySelector('meta[name="csrf-token"]');
		const config = {
			headers: { 
				'X-CSRF-TOKEN': token.content,
				'content-type': 'multipart/form-data'
			}
		}	
		//alert(token.content);
		$('#next1').on('click', function(){
			$('#li1').removeClass('active');
			$('#li1child').css({'display': 'none'});
			$('#li2').addClass('active');
			$('#li2child').css({'display': 'block'});
		});
		
		document.getElementById('pm-banking-information').addEventListener('submit', function(e){
			e.preventDefault();
			if($('#proof_of_bank_hidden').val() == 1){
				var names = ['bank_name', 'beneficiary_name', 'beneficiary_account_number', 'ifsc_code', 'bank_branch_address'];
			}else{
				var names = ['bank_name', 'beneficiary_name', 'beneficiary_account_number', 'ifsc_code', 'bank_branch_address', 'proof_of_bank_type', 'proof_of_bank'];
			}
			var errorCount = 0;
			names.forEach((el) => {
				var val = document.forms["pm-banking-information"][el].value;
				if (val == null || val == "") {
					document.getElementById(el + '_error').style = 'color:red';
					document.getElementById(el + '_error').focus();
					document.getElementById(el + '_error').textContent = 'Required!!!';
					++errorCount;
				}else{
					document.getElementById(el + '_error').textContent = '';
				}
			});
			if (errorCount) return true;		
			const form = document.querySelector('#pm-banking-information');
			var formData = new FormData(form);
			var config2 = config;
			axios.post(bURL+'dashboard/translator/update-profile', formData, config2)
			.then(function (res) {
				console.log(res.data);
				if(res.data.message === true){
					var msg = `<div class="card-alert card  lighten-5">
					<div class="card-content green-text">
					<p>Success! Subject matter expertise data saved successfuly</p>
					</div>
					</div>`;
					$filled =  ' &nbsp; <span id="banking_information_flag" style="color:green"> ( Information filled )</span>';     
				}else{
					var msg = `<div class="card-alert card  lighten-5">
					<div class="card-content red-text">
					<p>Error! Have problem regarding save subject matter expertise</p>
					</div>
					</div>`;
					$filled =  ' &nbsp; <span id="banking_information_flag" style="color:red"> ( Information to be filled )</span>'; 
				}
				$('#banking_information_flag').html($filled);
				$('#messagePMBankingInformation').html(msg);
				$('#messagePMBankingInformation').empty().show().html(msg).delay(3000).fadeOut(300);
			})
			.catch(function (err) {
				console.log(err);            
			});          
		});
		
		
		document.getElementById('pm-alternate-payment').addEventListener('submit', function(e){
			e.preventDefault();
			const form = document.querySelector('#pm-alternate-payment');
			var formData = new FormData(form);
			axios.post(bURL+'dashboard/translator/update-profile', formData, config)
			.then(function (res) {
				console.log(res.data);
				if(res.data.message === true){
					var msg = `<div class="card-alert card  lighten-5">
					<div class="card-content green-text">
					<p>Success! Subject matter expertise data saved successfuly</p>
					</div>
					</div>`;
					$filled =  ' &nbsp; <span id="alternate_payment_flag" style="color:green"> ( Information filled )</span>';     
				}else{
					var msg = `<div class="card-alert card  lighten-5">
					<div class="card-content red-text">
					<p>Error! Have problem regarding save subject matter expertise</p>
					</div>
					</div>`;
					$filled =  ' &nbsp; <span id="alternate_payment_flag" style="color:red"> ( Information to be filled )</span>'; 
				}
				$('#alternate_payment_flag').html($filled);
				$('#messagePMAlternatePayment').html(msg);
				$('#messagePMAlternatePayment').empty().show().html(msg).delay(3000).fadeOut(300);
			})
			.catch(function (err) {
				console.log(err);            
			});          
		});
		
		
	});
</script>
@endpush      