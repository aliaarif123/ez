@extends('layouts.user')
@section('content')
@php 
$ucode = Request::segment(5) ?? Auth::user()->ucode;
$user_id = Request::segment(6) ?? Auth::id();
@endphp

<!-- @if(Auth::user()->profile->saveBtnStatus == 1) readonly @endif -->

<div class="row ">
  <div class="col s12">
    <p>Manage your personal information</p>
  </div>
  <div class="col s12 m12 l12">
    {{ Form::open(array('url' => route('dashboard.translator.updateProfile'), 'files'=>'true', 'method'=>'post', 'enctype'=>'multipart/form-data', 'id' => 'personalInformation', 'name' => 'personalInformation')) }}
    @csrf
    <input type="hidden" name="form_type" value="personal_information">
    <input type="hidden" name="ucode" value="{{ $ucode }}">
    <input type="hidden" name="user_id" value="{{ $user_id }}">
    <br/>
    <div class="step-content">
      <div class="row">
        <div class="input-field col m3 s12">
          <label for="first_name">First Name<span class="red-text">*</span></label>
          <input type="text" id="first_name" name="first_name" value="{{ $profile->first_name }}" >
          <span class="error"><p id="first_name_error"></p></span>
        </div>
        <div class="input-field col m2 s12">
          <label for="middle_name">Middle Name</label>
          <input type="text" id="middle_name" name="middle_name" value="{{ $profile->middle_name }}" >
          <span class="error"><p id="middle_name_error"></p></span>
        </div>
        <div class="input-field col m2 s12">
          <label for="last_name">Last Name<span class="red-text">*</span></label>
          <input type="text" id="last_name" name="last_name" value="{{ $profile->last_name }}" >
          <span class="error"><p id="last_name_error"></p></span>
        </div>
        <div class="input-field col m3 s12">
          <label for="email">Email<span class="red-text">*</span></label>
          <input type="email"  name="email" id="email" value="{{ Request::segment(5) ? $profile->user->email : Auth::user()->email }}"  disabled>
          <span class="error"><p id="email_error"></p></span>
        </div>
        <div class="input-field col m2 s12">
          <select name="gender" id="gender" >
            <option value="Male" {{ $profile->gender == 'Male' ?? 'selected' }}>Male</option>
            <option value="Female" {{ $profile->gender == 'Female' ?? 'selected' }}>Female</option>
            <option value="Other" {{ $profile->gender == 'Prefer not to say' ?? 'selected' }}>Prefer not to say</option>
          </select>
          <label>Gender</label>
          <span class="error"><p id="gender_error"></p></span>
        </div>
      </div>
      <div class="row">
        <div class="input-field col m2 s12">
          <label for="nationality">Nationality<span class="red-text">*</span></label>
          <input type="text" id="nationality" name="nationality" value="{{ $profile->nationality }}" >
          <span class="error"><p id="nationality_error"></p></span>
        </div>
        <div class="input-field col m3 s12">
          <select name="country" id="country" >
           <option disabled selected></option>
           @foreach($countries as $country)
           <option value="{{ $country->name ?? '' }}" @if($profile->country == $country->name) selected @endif >{{ $country->name  ?? '' }}</option> == 
           @endforeach
         </select>
         <label  for="country">Country of Residence</label>
         <span class="error"><p id="country_error"></p></span>
       </div>
       <div class="input-field col m1 s12">
        <select name="country_phonecode_mobile" id="country_phonecode_mobile" >
          <option disabled selected></option>
          @foreach($countriesPhoneCodes as $country)
          <option value="{{ $country->phonecode }}" @if($profile->country_phonecode_mobile == $country->phonecode)  selected  @endif >{{ $country->phonecode }}</option>
          @endforeach
        </select>
        <label  for="country_phonecode_mobile">Phonecode</label>
        <span class="error"><p id="country_phonecode_mobile_error"></p></span>
      </div>
      <div class="input-field col m2 s12">
        <label for="mobile">Mobile<span class="red-text">*</span></label>
        <input type="text" id="mobile" value="{{ $profile->mobile ?? '' }}" name="mobile" >
        <span class="error"><p id="mobile_error"></p></span>
      </div>
      <div class="input-field col m2 s12">
        <select name="country_phonecode_whatsapp" id="country_phonecode_whatsapp" >
         <option disabled selected></option>
         @foreach($countriesPhoneCodes as $country)
         <option value="{{ $country->phonecode }}" 
          @if($profile->country_phonecode_whatsapp == $country->phonecode) selected @endif>
          {{ $country->phonecode }}
        </option>
        @endforeach
      </select>
      <label>Whatsapp Phonecode</label>
      <span class="error"><p id="country_phonecode_whatsapp_error"></p></span>
    </div>
    <div class="input-field col m2 s12">
      <label for="whatsapp_mobile">WhatsApp Mobile</label>
      <input type="text"  name="whatsapp_mobile" id="whatsapp_mobile" value="{{ $profile->whatsapp_mobile ?? '' }}" >
      <span class="error"><p id="whatsapp_mobile_error"></p></span>
    </div>
  </div>
  <div class="step-actions">
    <div class="row" >
      <div class="col s4  mb-3  mt-1" >
        <button class="waves-effect waves-dark btn btn-lg btn-primary" type="submit"
        id="savePersonalInformationBtn" >Save</button>

<!-- @if(Auth::user()->profile->saveBtnStatus == 1) 
<button class="waves-effect waves-dark btn btn-lg btn-primary" type="submit"
id="savePersonalInformationBtn" >Save</button>
@else
<button  
class="waves-effect waves-dark btn btn-lg btn-primary" type="button"
onclick="window.location.href=`{{ route('dashboard.translator.getHelp') }}`"> Get Help </button>
@endif -->
<button  class="waves-effect waves-dark btn btn-lg btn-primary customHidden" type="submit" onclick="window.location.href='/dashboard/translator/my-profile/skills'">Next</button>

</div>
<div class="col s8  mb-2  mt-1" id="messagePersonalInformation"></div>
</div>
</div>
</div>
</form>
</div>
</div>
@endsection
@push('css')
<style>
</style>
@endpush
@push('js')
<script type="text/javascript">
  $(() => {
    $('#userFinalStatus').html(`Status : {{ Auth::user()->profile->finalStatus }}`);


    var token = document.head.querySelector('meta[name="csrf-token"]');
    const config = {
      headers: { 
        'X-CSRF-TOKEN': token.content,
        'content-type': 'multipart/form-data'
      }
    }


    document.getElementById('personalInformation').addEventListener('submit', function(e){
      e.preventDefault();
      /* Form Validation Start Here... */
      var names = ['first_name', 'last_name', 'gender', 'nationality', 'country', 'mobile', 'whatsapp_mobile'];
      var errorCount = 0;
      names.forEach((el) => {
        var val = document.forms["personalInformation"][el].value;
        if (val == null || val == "" || val == 0) {
          document.getElementById(el + '_error').style = 'color:red';
          document.getElementById(el + '_error').focus();
          document.getElementById(el + '_error').textContent = 'Required!!!';
          ++errorCount;
        }else{
          document.getElementById(el + '_error').textContent = '';
        }
      });
      if (errorCount) return false;
      /* Form Validation End Here... */
      const form = document.querySelector('#personalInformation');
      var formData = new FormData(form);
      axios.post(bURL+'dashboard/translator/update-profile', formData, config)
      .then(function (res) {
        console.log(res.data.status);
        if(res.data.message === true){
          var msg = `<div class="card-alert card  lighten-5">
          <div class="card-content green-text">
          <p>Success! Profile data saved successfuly</p>
          </div>
          </div>`; 

          if(res.data.status == true){
            let status = `{{ Session::put('finalStatus', 'Test Pending') }}`
            localStorage.setItem('finalStatus', 'Test Pending');
            $('#userFinalStatus').html('Status : Test Pending');
          }


        }else{
          var msg = `<div class="card-alert card  lighten-5">
          <div class="card-content red-text">
          <p>Error! Have problem regarding save profile data</p>
          </div>
          </div>`;
          //console.log(res.data.status);

          //localStorage.setItem('status', 'Test Pending');
          //$('#userFinalStatus').html('Status : '+localStorage.getItem('status'));
          


        }
        $('#messagePersonalInformation').html(msg);

        $('#messagePersonalInformation').empty().show().html(msg).delay(3000).fadeOut(300);
      })
      .catch(function (err) {
        console.log(err);            
      });          
    });
  });
</script>
@endpush      