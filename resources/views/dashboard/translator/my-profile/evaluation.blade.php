@extends('layouts.user')

@section('content')


<div class="row">


  <div class="col s12 m12 l12">
    <div class="card subscriber-list-card animate fadeRight">
      <div class="card-content pb-1">
        <h4 class="card-title mb-0">My Test List <i class="material-icons float-right"><button type="button" id="confirm-box1"  class="waves-effect waves-light btn gradient-45deg-red-pink">Evaluation Criteria</button></i></h4>

      </div>
      <table class="subscription-table responsive-table highlight">
        <thead>
          <tr>
            <th>Title</th>
            <th>Source</th>
            <th>Target</th>
            <th>Duration</th>
            <th>Status</th>
            <th>Test Score</th>
            <th class="right-align">Action</th>
          </tr>
        </thead>
        <tbody>

          @foreach ($myAllPossibleTests as $key => $tests)

          @php
          $arr = explode('-', $key);
          $lpStr =  $arr[0] .' to '.$arr[1];
          $lpStrForEnableDisable = $arr[0] .'-'.$arr[1];

          $showFlag = DB::table('language_pairs')->where('ucode', Auth::user()->ucode)->where('flag', 'language-pair')->where('name', $lpStrForEnableDisable)->first();
          $testsShowFlag = $showFlag->showTest;
          @endphp
          <tr>
            <th align="center" colspan="7" style="padding-left: 20px;"><strong>{{ $lpStr }}</strong>
<!-- <label id="myLangs" style="position: relative; top: 8px;" >
<input style="position: relative; right: 20px;" class="form-check-input" data-aaa="{{ $lpStrForEnableDisable }}" type="checkbox" @if($testsShowFlag == 1) checked @endif>
<span  data-aaa="{{ $lpStrForEnableDisable }}" class="enDis"></span>
</label> -->

</th>
</tr> 


@foreach ($tests as $test)
<!-- <tr class="@if($testsShowFlag == 0) {{ $lpStrForEnableDisable }} cusHide  @else {{ $lpStrForEnableDisable }}  @endif"> -->
  <tr>
    <td>{{ $test->title }}</td>
    <td>{{ $test->source_language }}</td>
    <td>{{ $test->target_language }}</td>
    <td>{{ $test->test_duration }} (min.)</td>
    @php
    $test_attempt = DB::table('test_attempt')->where('lang_pair', $key)->where('ucode', $ucode)->where('test_id', $test->id)->first();

    $lang_pairs = DB::table('language_pairs')->where('flag', 'language-pair')->where('name', $key)->where('ucode', $ucode)->first();

    @endphp

    <td>
      @if($test_attempt && $test_attempt->test_ended_at != null)
      <span class="badge green lighten-5 blue-text text-accent-4">Complete</span>
      @else 
      <span class="badge red lighten-5 blue-text text-accent-4">To be Given</span> 
      @endif

    </td>

   <!--  <td>
      @if($lang_pairs && $lang_pairs->final_score != null)
      <span class="badge green lighten-5 blue-text text-accent-4">{{ $lang_pairs->final_score ?? 0 }}</span>
      @else
      <span class="badge green lighten-5 blue-text text-accent-4">0</span>
      @endif
    </td> -->


    <td>
      @if($test_attempt && $test_attempt->test_result != null)
      <span class="badge green lighten-5 blue-text text-accent-4">{{ json_decode( $test_attempt->test_result, true )['total_score'] ?? 0 }}</span>
      @else
      <span class="badge green lighten-5 blue-text text-accent-4">0</span>
      @endif
    </td>





    <td class="right-align">
      @if($test_attempt && $test_attempt->test_ended_at != null)
      <a   href="{{ route('dashboard.translator.showUserTestSingle', [$key, base64_encode($test->id) ]) }}">View Test</a>
      @else 
      <a class="test-link" onclick="return goToTestLink(`{{ route('dashboard.translator.showUserTestSingle', [$key, base64_encode($test->id)]) }}, {{ $test->test_duration }}`);"  href="javascript:;">Start Test</a>
      @endif

    </td>
  </tr>
  @endforeach

  @endforeach


</tbody>
</table>

</div>

@if(Auth::user()->profile->finalStatus == 'Contract Pending')
<button  class="waves-effect waves-dark btn btn-lg btn-primary customHidden left mb-4" type="button" onclick="window.location.href='/dashboard/translator/my-profile/professional-details'">Next</button>
@endif
</div>



<!-- Modal Structure -->
<div id="modal1" class="modal bottom-sheet modal-content" style=" height: 900px;">



  <div id="responsive-table " class="card card card-default scrollspy">
    <div class="card-content">
      <h4 class="card-title">Evaluation Criteria <i class="material-icons right-align right modal-close">clear</i></h4>
     <!--  <p class="mb-2">Add <code class=" language-markup">class="responsive-table"</code> to the table tag to make
        the table
        horizontally scrollable on smaller screen widths. 
        <i class="material-icons right-align right modal-close">clear</i>
      </p> -->
      <div class="row">

        <div class="col s12">
          <table class="black-text">
            <thead>
              <tr>
                <th width="10%" data-field="id">Criteria</th>
                <th width="55%" data-field="name">Explanation and deductible items</th>
                <th width="5%" data-field="price">Score</th>
                <th width="30%" data-field="total">For each mistake in any of the items under the criteria (designed for a text of 300-400 words)</th>
              </tr>
            </thead>
            <tbody>

              <tr>
                <td>Semantics</td>
                <td>The translator is able to convey the full meaning of SL into TL (no deletion, no addition, no change of meaning, no ambiguity.) Interpretation of TL is the same as that of SL.</td>
                <td>150</td>
                <td>Deduct 10 for each mistake.</td>
                
              </tr>

              <tr>
                <td>Terminology</td>
                <td>1) Correct usages and choices of words as to
                  convey the meaning correctly (ex: boy, man,
                  male)
                  2) No lexical ambiguity
                  3) Using correct jargons when necessary that fit
                the context and the audience</td>
                <td>50</td>
                <td>Deduct 5 for each mistake.</td>
                
              </tr>

              <tr>
                <td>Syntax</td>
                <td>- Correct and clear usage of:
                  1) Grammar (tenses, conditionality, modality.
                  etc.)
                  2) Spelling, especially for Arabic: (lam Shamsia
                  wal kamaria, ha’a wat aa marbota, tanween)
                  3) Punctuation
                  5) Sentence structure:
                  a) Making sure that there is no ambiguity in
                  sentence structure
                  b) Using correct sentence structure in Arabic
                  (like using verbal sentences whenever suitable)
                c) Correct conjunctions</td>
                <td>150</td>
                <td>Deduct 10 for each
                  mistake. (if mistakes
                  are within the same
                  category, consider it
                one)</td>
                
              </tr>

              <tr>
                <td>Stylistic quality</td>
                <td>1) Suitable expressions
                  2) Correct usage of translation methods (literal,
                  semantic..)
                  3) Correct usage of numbering and equivelances
                  in Arabic (Abjad Hawaz) and text/slide direction
                  4) No repetition
                  5) Translation fits culture and audience
                  (expert/inexpert). Marks will be deducted for
                  expressions, terms, or style not suitable for the
                audience or culture.</td>
                <td>100</td>
                <td>Deduct 5 for each
                  mistake. (if mistakes
                  are within the same
                  category, consider it
                one)</td>
                
              </tr>

              <tr>
                <td>Stylistic beauty and cultural style</td>
                <td>- Beauty of text such as:
                  1) Text coherence, and connection of ideas
                  2) Readability and clarity
                  3) The translation sounds authentic in TL.
                  4) Style of translation fits the context (media,
                legal. etc.)</td>
                <td>50</td>
                <td>Subjective evaluation</td>
                
              </tr>

              <tr>
                <td colspan="4">Total: 500</td>
              </tr>

            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>


</div>
</div>


<!-- <iframe  id="modal1" class="modal modal-conten" width="100%" height="100%" border="0" src="{{ asset('static_files/Evaluation_criteria.pdf') }}"></iframe> -->

<!-- Modal Structure -->
<div id="modal2" class="modal">
  <div class="modal-content">
    <h4 align="center"><strong>Instructions</strong></h4>
    <p align="justify"><strong>1. Duration of the test is 120 minutes or 2 hours from the moment of commencement. Once you start the test, the timer will start countdown and can not be reset.</strong></p>
    <!-- <p align="justify"><strong>2. Test duration: <span id="dynamicTestDuration">0</span> min.</strong></p> -->
    <p align="justify"><strong>2. Once the test has begun, you are required to take the complete test. In case you leave the test without submitting, you shall be required to take the whole test again.</strong></p>
    <p align="justify"><strong>3. A complete verification of personnel details shall be done by the ArabEasy team and only the individual who wishes to join ArabEasy shall take the test.</strong></p>
    <p align="justify"><strong>4. Use of internet and obtaining any form of help from outside sources such as friends, family or vendors is not permissible.</strong></p>

  </div>
  <div class="modal-footer">
    <a id="dynamicLink" href="javascript:;" class="modal-action waves-effect waves-green btn-flat">Start test now</a>
    <a  href="javascript:;" class="modal-action modal-close waves-effect waves-red btn-flat ">Take later</a>
  </div>
</div>









</div>
@endsection


@push('css')
<style>
  .cusHide{
    display:none;
  }
  .modal.bottom-sheet {
    max-height: 90%;
  }
</style>
@endpush
@push('js')
<script>
  //location.reload(true);

  function goToTestLink(link, test_duration){
    var str = link;
    var dynamicData = str.split(",");
    var dynamicLink = dynamicData[0];
    //var dynamicTestDuration = dynamicData[1];
    document.getElementById("dynamicLink").href=dynamicLink; 
    //document.getElementById("dynamicTestDuration").innerHTML=dynamicTestDuration; 
    $("#modal2").modal('open', {
      dismissible: false,
    });  
  }
  $(() => {
    //sessionStorage.setItem('status', 'Test Pending');
    //$('#userFinalStatus').html(`Status : {{ Auth::user()->profile->finalStatus }}`);
    $('#userFinalStatus').html(`Status : {{ $status ??  Session::get('finalStatus') }}`);

   //let userFinalStatus = `{{ Session::get('finalStatus') ?? 'Profile Incomplete' }}`;
   //let userFinalStatus = `{{ Auth::user()->profile->finalStatus }}`;
    //$('#userFinalStatus').text('Status : '+userFinalStatus);
    //document.getElementById('userFinalStatus').innerHTML = 'Status : '+userFinalStatus; 
    //$('#userFinalStatus').html('Status : '+userFinalStatus)

    //window.location.href = window.location.href

    $('#confirm-box1').on('click', () => {
      $("#modal1").modal('open', {
        dismissible: false,
      });
    });

    // $('.enDis').on('click', function(){
    //   var name = $(this).data('aaa');
    //   axios.get(bURL+'dashboard/translator/update-show-test-flag/'+name)
    //   .then(function (res) {
    //     $('.'+name).toggleClass('cusHide');
    //   // if(res.data.message === true){
    //     //   $('.'+aa).removeClass('cusHide');
    //     //   $('.'+aa).toggleClass('cusHide');
    //     // }  
    //   })
    //   .catch(function (err) { console.log(err);});
    // });
    
  });
</script>
@endpush
