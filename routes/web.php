<?php

Route::get('/test-server-ip', function(){
    dd($_SERVER['HTTP_HOST']);
});

Auth::routes(['verify' => true]);
Route::get('/user/verify/{token}', 'Auth\RegisterController@verify');

Route::get('/intro-model-display-state', function(){

    if(Auth::check()){
        Auth::user()->profile->update([
            'introModelDisplayState' => 0
        ]);
        return 1;    
    }else{
        return 0;
    }
    
});


Route::get('/', 'WelcomeController@index')->name('welcome');
Route::get('/permission-error', 'CommonController@permissionError')->name('permission');
Route::get('/not-found', 'CommonController@permissionError')->name('notFound');
Route::get('/dashboard', 'DashboardController@index')->name('dashboard');

Route::group(['middleware' => ['auth', 'verified'], 'as' => 'dashboard.', 'prefix' => 'dashboard'], function () {

    ///////////////////////////////////////////////
    //////////////////////////////////////////////
    /////////////////////////////////////////////

    /* Start Admin Routes */
    Route::get('admin', [
        'uses' => 'AdminController@index',
        'as' => 'admin.index',
        'middleware' => 'roles',
        'roles' => ['Admin']
    ]);

    Route::get('admin/manage-users', [
        'uses' => 'AdminController@manageUsers',
        'as' => 'admin.manageUsers',
        'middleware' => 'roles',
        'roles' => ['Admin']
    ]);

    Route::get('admin/manage-roles', [
        'uses' => 'AdminController@manageRoles',
        'as' => 'admin.manageRoles',
        'middleware' => 'roles',
        'roles' => ['Admin']
    ]);

    Route::post('admin/manage-roles', [
        'uses' => 'AdminController@manageRolesAction',
        'as' => 'admin.manageRolesAction',
        'middleware' => 'roles',
        'roles' => ['Admin']
    ]);


    Route::get('admin/manage-tests', [
        'uses' => 'AdminController@manageTests',
        'as' => 'admin.manageTests',
        'middleware' => 'roles',
        'roles' => ['Admin']
    ]);

    Route::get('admin/manage-translotors', [
        'uses' => 'AdminController@manageTranslotors',
        'as' => 'admin.manageTranslotors',
        'middleware' => 'roles',
        'roles' => ['Admin']
    ]);

    Route::get('admin/general-settings', [
        'uses' => 'AdminController@generalSettings',
        'as' => 'admin.generalSettings',
        'middleware' => 'roles',
        'roles' => ['Admin']
    ]);

    Route::get('admin/add-user', [
        'uses' => 'AdminController@addUser',
        'as' => 'admin.addUser',
        'middleware' => 'roles',
        'roles' => ['Admin']
    ]);

    Route::post('admin/addNewUser', [
        'uses' => 'AdminController@addNewUser',
        'as' => 'admin.addNewUser',
        'middleware' => 'roles',
        'roles' => ['Admin']
    ]);

    Route::post('admin/deleteUser', [
        'uses' => 'AdminController@deleteUser',
        'as' => 'admin.deleteUser',
        'middleware' => 'roles',
        'roles' => ['Admin']
    ]);

    Route::get('admin/viewUser/{id?}', [
        'uses' => 'AdminController@viewUser',
        'as' => 'admin.viewUser',
        'middleware' => 'roles',
        'roles' => ['Admin']
    ]);

    Route::get('admin/addTest/{id?}', [
        'uses' => 'AdminController@addTest',
        'as' => 'admin.addTest',
        'middleware' => 'roles',
        'roles' => ['Admin']
    ]);

    Route::post('admin/addNewTest{id?}', [
        'uses' => 'AdminController@addNewTest',
        'as' => 'admin.addNewTest',
        'middleware' => 'roles',
        'roles' => ['Admin']
    ]);

    Route::get('admin/editTest/{id?}', [
        'uses' => 'AdminController@editTest',
        'as' => 'admin.editTest',
        'middleware' => 'roles',
        'roles' => ['Admin']
    ]);

    Route::post('admin/updateTest/{id?}', [
        'uses' => 'AdminController@updateTest',
        'as' => 'admin.updateTest',
        'middleware' => 'roles',
        'roles' => ['Admin']
    ]);

    Route::post('admin/deleteTest', [
        'uses' => 'AdminController@deleteTest',
        'as' => 'admin.deleteTest',
        'middleware' => 'roles',
        'roles' => ['Admin']
    ]);

    Route::get('admin/evalTests/{id?}', [
        'uses' => 'AdminController@evalTests',
        'as' => 'admin.evalTests',
        'middleware' => 'roles',
        'roles' => ['Admin']
    ]);

    Route::get('admin/evaluateTests', [
        'uses' => 'AdminController@evaluateTests',
        'as' => 'admin.evaluateTests',
        'middleware' => 'roles',
        'roles' => ['Admin']
    ]);

    Route::get('admin/evaluateTest/{id?}', [
        'uses' => 'AdminController@evaluateTest',
        'as' => 'admin.evaluateTest',
        'middleware' => 'roles',
        'roles' => ['Admin']
    ]);

    Route::post('admin/submitEval/{id?}', [
        'uses' => 'AdminController@submitEval',
        'as' => 'admin.submitEval',
        'middleware' => 'roles',
        'roles' => ['Admin']
    ]);

    Route::get('admin/updateContract/{id?}', [
        'uses' => 'AdminController@updateContract',
        'as' => 'admin.updateContract',
        'middleware' => 'roles',
        'roles' => ['Admin']
    ]);

    Route::post('admin/updateContractLP', [
        'uses' => 'AdminController@updateContractLP',
        'as' => 'admin.updateContractLP',
        'middleware' => 'roles',
        'roles' => ['Admin']
    ]);

    Route::post('admin/sendStatusReport', [
        'uses' => 'AdminController@sendStatusReport',
        'as' => 'admin.sendStatusReport',
        'middleware' => 'roles',
        'roles' => ['Admin']
    ]);

    Route::post('admin/ModalWithLpDetails', [
        'uses' => 'AdminController@ModalWithLpDetails',
        'as' => 'admin.ModalWithLpDetails',
        'middleware' => 'roles',
        'roles' => ['Admin']
    ]);

    Route::post('admin/saveModalDetails', [
        'uses' => 'AdminController@saveModalDetails',
        'as' => 'admin.saveModalDetails',
        'middleware' => 'roles',
        'roles' => ['Admin']
    ]);

    Route::post('admin/emailFormat', [
        'uses' => 'AdminController@emailFormat',
        'as' => 'admin.emailFormat',
        'middleware' => 'roles',
        'roles' => ['Admin']
    ]);

    Route::get('admin/addNewClient', [
        'uses' => 'AdminController@addNewClient',
        'as' => 'admin.addNewClient',
        'middleware' => 'roles',
        'roles' => ['Admin']
    ]);

    Route::post('admin/addNewClientPost', [
        'uses' => 'AdminController@addNewClientPost',
        'as' => 'admin.addNewClientPost',
        'middleware' => 'roles',
        'roles' => ['Admin']
    ]);

    Route::get('admin/manageClients', [
        'uses' => 'AdminController@manageClients',
        'as' => 'admin.manageClients',
        'middleware' => 'roles',
        'roles' => ['Admin']
    ]);

    Route::get('admin/manageAssignments', [
        'uses' => 'AdminController@manageAssignments',
        'as' => 'admin.manageAssignments',
        'middleware' => 'roles',
        'roles' => ['Admin']
    ]);

    Route::get('admin/add-new-assignment', [
        'uses' => 'AdminController@addNewAssignment',
        'as' => 'admin.addNewAssignment',
        'middleware' => 'roles',
        'roles' => ['Admin']
    ]);

    Route::POST('admin/add-new-assignment', [
        'uses' => 'AdminController@addNewAssignmentPost',
        'as' => 'admin.addNewAssignmentPost',
        'middleware' => 'roles',
        'roles' => ['Admin']
    ]);

    Route::POST('admin/addNewAssignmentPost', [
        'uses' => 'AdminController@addNewAssignmentPost',
        'as' => 'admin.addNewAssignmentPost',
        'middleware' => 'roles',
        'roles' => ['Admin']
    ]);

    Route::get('admin/viewAssigment/{id?}', [
        'uses' => 'AdminController@viewAssigment',
        'as' => 'admin.viewAssigment',
        'middleware' => 'roles',
        'roles' => ['Admin']
    ]);

    Route::get('admin/manage-roles/', [
        'uses' => 'AdminController@manageRoles',
        'as' => 'admin.manageRoles',
        'middleware' => 'roles',
        'roles' => ['Admin', 'Quality Analyst']
    ]);

    Route::post('admin/manage-roles/', [
        'uses' => 'AdminController@manageRolesAction',
        'as' => 'admin.manageRolesAction',
        'middleware' => 'roles',
        'roles' => ['Admin']
    ]);


    Route::post('admin/upload-file/', [
        'uses' => 'AdminController@uploadFile',
        'as' => 'admin.uploadFile',
        'middleware' => 'roles',
        'roles' => ['Admin']
    ]);


    /* End Admin Routes */

                                                                                                                                                        ///////////////////////////////////////////////
                                                                                                                                                        //////////////////////////////////////////////
                                                                                                                                                        /////////////////////////////////////////////

    /* Start Translator Routes */
    Route::get('translator', [
        'uses' => 'TranslatorController@index',
        'as' => 'translator.index',
        'middleware' => 'roles',
        'roles' => ['Translator']
    ]);

    Route::get('translator/manage-profile/{ucode?}', [
        'uses' => 'TranslatorController@manageProfile',
        'as' => 'translator.manageProfile',
        'middleware' => 'roles',
        'roles' => ['Translator', 'Admin']
    ]);

    Route::get('translator/partials/educations/fields', [
        'uses' => 'TranslatorController@educationFields',
        'as' => 'translator.partials.educations.fields',
        'middleware' => 'roles',
        'roles' => ['Translator', 'Admin']
    ]);

    Route::post('translator/update-profile', [
        'uses' => 'TranslatorController@updateProfile',
        'as' => 'translator.updateProfile',
        'middleware' => 'roles',
        'roles' => ['Translator', 'Admin']
    ]);

    Route::get('translator/update-language-pairs/{languages?}', [
        'uses' => 'TranslatorController@updateLanguagePairs',
        'as' => 'translator.updateLanguagePairs',
        'middleware' => 'roles',
        'roles' => ['Translator', 'Admin']
    ]);

    Route::get('translator/my-profile/evaluation/{ucode?}', [
        'uses' => 'TranslatorController@evaluation',
        'as' => 'translator.myprofile.evaluation',
        'middleware' => 'roles',
        'roles' => ['Translator', 'Admin']
    ]);

    Route::get('translator/my-profile/public-profile/{ucode?}', [
        'uses' => 'TranslatorController@publicProfile',
        'as' => 'translator.myprofile.publicProfile',
        'middleware' => 'roles',
        'roles' => ['Translator', 'Admin']
    ]);

    Route::get('translator/test/{lang_pair}/{test_id?}/{ucode?}', [
        'uses' => 'TranslatorController@showUserTestSingle',
        'as' => 'translator.showUserTestSingle',
        'middleware' => 'roles',
        'roles' => ['Translator', 'Admin']
    ]);

    Route::get('translator/show-test-results/{ucode?}', [
        'uses' => 'TranslatorController@showTestResults',
        'as' => 'translator.showTestResults',
        'middleware' => 'roles',
        'roles' => ['Translator']
    ]);

    Route::post('translator/take-test', [
        'uses' => 'TranslatorController@attemptTest',
        'as' => 'translator.attemptTest',
        'middleware' => 'roles',
        'roles' => ['Translator', 'Admin']
    ]);

                                                                                                                                                                                            // Route::get('translator/myprofile/contracts/{ucode?}', [
                                                                                                                                                                                                //     'uses' => 'TranslatorController@contracts',
                                                                                                                                                                                                //     'as' => 'translator.contracts',
                                                                                                                                                                                                //     'middleware' => 'roles',
                                                                                                                                                                                                //     'roles' => ['Translator']
                                                                                                                                                                                                // ]);



    Route::get('translator/update-show-test-flag/{name}', 'TranslatorController@changeTestShowFlag');


    Route::post('translator/ModalWithLpDetails', [
        'uses' => 'TranslatorController@ModalWithLpDetails',
        'as' => 'translator.ModalWithLpDetails',
        'middleware' => 'roles',
        'roles' => ['Translator']
    ]);

    Route::get('translator/my-account/{ucode?}', [
        'uses' => 'TranslatorController@myAccount',
        'as' => 'translator.myAccount',
        'middleware' => 'roles',
        'roles' => ['Translator', 'Admin']
    ]);

    Route::post('translator/acceptContract', [
        'uses' => 'TranslatorController@acceptContract',
        'as' => 'translator.acceptContract',
        'middleware' => 'roles',
        'roles' => ['Translator']
    ]);


    Route::get('translator/my-profile/personal-information/{ucode?}', [
        'uses' => 'TranslatorController@personalInformation',
        'as' => 'translator.myprofile.personalInformation',
        'middleware' => 'roles',
        'roles' => ['Translator', 'Admin']
    ]);

    Route::get('translator/my-profile/skills/{ucode?}', [
        'uses' => 'TranslatorController@skills',
        'as' => 'translator.myprofile.skills',
        'middleware' => 'roles',
        'roles' => ['Translator', 'Admin']
    ]);

    Route::get('translator/my-profile/professional-details/{ucode?}', [
        'uses' => 'TranslatorController@professionalDetails',
        'as' => 'translator.myprofile.professionalDetails',
        'middleware' => 'roles',
        'roles' => ['Translator', 'Admin']
    ]);

    Route::get('translator/my-profile/payment-method/{ucode?}', [
        'uses' => 'TranslatorController@paymentMethod',
        'as' => 'translator.myprofile.paymentMethod',
        'middleware' => 'roles',
        'roles' => ['Translator', 'Admin']
    ]);

    Route::get('translator/myaccount/contracts/{ucode?}', [
        'uses' => 'TranslatorController@contracts',
        'as' => 'translator.my-account.contracts',
        'middleware' => 'roles',
        'roles' => ['Translator', 'Admin']

    ]);


    Route::get('translator/my-assignments/active-assignments/{ucode?}', [
        'uses' => 'TranslatorController@activeAssignments',
        'as' => 'translator.myassignments.activeAssignments',
        'middleware' => 'roles',
        'roles' => ['Translator', 'Admin']
    ]);

    Route::get('translator/my-assignments/live-feed/{ucode?}', [
        'uses' => 'TranslatorController@liveFeed',
        'as' => 'translator.myassignments.liveFeed',
        'middleware' => 'roles',
        'roles' => ['Translator', 'Admin']
    ]);




    Route::get('translator/get-help', [
        'uses' => 'CommonController@getHelp',
        'as' => 'translator.getHelp',
        'middleware' => 'auth'
    ]);

    Route::post('translator/get-help', [
        'uses' => 'CommonController@getHelpProcess',
        'as' => 'translator.getHelpProcess',
        'middleware' => 'auth'
    ]);







                                                                                                                                                                                                                                                // Route::post('translator/test/', [
                                                                                                                                                                                                                                                    //     'uses' => 'TranslatorController@attemptTest',
                                                                                                                                                                                                                                                    //     'as' => 'translator.attemptTest',
                                                                                                                                                                                                                                                    //     'middleware' => 'roles',
                                                                                                                                                                                                                                                    //     'roles' => ['Translator']
                                                                                                                                                                                                                                                    // ]);

    /* End Translator Routes */

                                                                                                                                                                                                                                                    ///////////////////////////////////////////////
                                                                                                                                                                                                                                                    //////////////////////////////////////////////
                                                                                                                                                                                                                                                    /////////////////////////////////////////////

    /* Start QA Routes */
    Route::get('quality-analyst', [
        'uses' => 'QaController@index',
        'as' => 'qa.index',
        'middleware' => 'roles',
        'roles' => ['Quality Analyst']
    ]);

    Route::get('quality-analyst/manage-profile', [
        'uses' => 'QaController@manageProfile',
        'as' => 'qa.manageProfile',
        'middleware' => 'roles',
        'roles' => ['Quality Analyst']
    ]);

    Route::get('quality-analyst/manage-test', [
        'uses' => 'QaController@manageTest',
        'as' => 'qa.manageTest',
        'middleware' => 'roles',
        'roles' => ['Quality Analyst']
    ]);




    /* End QA Routes */

                                                                                                                                                                                                                                                                ///////////////////////////////////////////////
                                                                                                                                                                                                                                                                //////////////////////////////////////////////
                                                                                                                                                                                                                                                                /////////////////////////////////////////////


    /* Start Proof Readers Routes */
    Route::get('proof-reader', [
        'uses' => 'PrController@index',
        'as' => 'pr.index',
        'middleware' => 'roles',
        'roles' => ['Proof Reader']
    ]);

    Route::get('proof-reader/manage-users', [
        'uses' => 'PrController@manageProfile',
        'as' => 'pr.manageProfile',
        'middleware' => 'roles',
        'roles' => ['Proof Reader']
    ]);

    Route::get('proof-reader/manage-test', [
        'uses' => 'PrController@manageTest',
        'as' => 'pr.manageTest',
        'middleware' => 'roles',
        'roles' => ['Proof Reader']
    ]); 
    /* End Proof Readers Routes */

                                                                                                                                                                                                                                                                            ///////////////////////////////////////////////
                                                                                                                                                                                                                                                                            //////////////////////////////////////////////
                                                                                                                                                                                                                                                                            /////////////////////////////////////////////

    /* Start Operation User Routes */
    Route::get('operation-user', [
        'uses' => 'OuController@index',
        'as' => 'ou.index',
        'middleware' => 'roles',
        'roles' => ['Operation User']
    ]);

    Route::get('operation-user/manage-users', [
        'uses' => 'OuController@manageProfile',
        'as' => 'ou.manageProfile',
        'middleware' => 'roles',
        'roles' => ['Operation User']
    ]);

    Route::get('operation-user/manage-test', [
        'uses' => 'OuController@manageTest',
        'as' => 'ou.manageTest',
        'middleware' => 'roles',
        'roles' => ['Operation User']
    ]); 
    /* End Operation User Routes */

                                                                                                                                                                                                                                                                                        ///////////////////////////////////////////////
                                                                                                                                                                                                                                                                                        //////////////////////////////////////////////
                                                                                                                                                                                                                                                                                        /////////////////////////////////////////////


    /* Start Client Routes */
    Route::get('client', [
        'uses' => 'ClientController@index',
        'as' => 'client.index',
        'middleware' => 'roles',
        'roles' => ['Client']
    ]);

    Route::get('client/manage-profile', [
        'uses' => 'ClientController@manageProfile',
        'as' => 'client.manageProfile',
        'middleware' => 'roles',
        'roles' => ['Client']
    ]);

    Route::get('client/manage-test', [
        'uses' => 'ClientController@manageTest',
        'as' => 'client.manageTest',
        'middleware' => 'roles',
        'roles' => ['Client']
    ]); 
    /* End Client Routes */

                                                                                                                                                                                                                                                                                                    ///////////////////////////////////////////////
                                                                                                                                                                                                                                                                                                    //////////////////////////////////////////////
                                                                                                                                                                                                                                                                                                    /////////////////////////////////////////////



});



Route::get('dashboard/translator/generate-article', [
    'uses' => 'CommonController@getGenerateArticle',
    'as' => 'translator.article',
    'middleware' => 'roles',
    'roles' => ['Translator']
]);

Route::get('dashboard/admin/assign-roles', [
    'uses' => 'CommonController@listRoles',
    'as' => 'admin',
    'middleware' => 'roles',
    'roles' => ['Admin']
]);

Route::post('dashboard/admin/assign-roles', [
    'uses' => 'CommonController@postAdminAssignRoles',
    'as' => 'admin.assign',
    'middleware' => 'roles',
    'roles' => ['Admin']
]);


Route::get('webcam/start', [
    'uses' => 'WelcomeController@webcamStart',
    'as' => 'webcam.start',
    'middleware' => 'web'
]);

Route::get('webcam/save', [
    'uses' => 'WelcomeController@photoSave',
    'as' => 'webcam.save',
    'middleware' => 'web'
]);




Route::get('logout', 'AuthController@getLogout');














